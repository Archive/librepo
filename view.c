/* -*-Mode: C;-*-
 * handle.c,v 1.11 1998/10/03 00:17:50 jmacd Exp
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "repo.h"
#include "repopriv.h"

typedef struct _ViewHandle ViewHandle;

struct _ViewHandle {
  RepoFileHandle  handle;
  FileSegment    *seg;
  SegmentView    *view;
};

static gboolean     view_handle_close (FileHandle* fh);
static void         view_handle_abort (FileHandle* fh);
static const gchar* view_handle_name  (FileHandle *fh);
static void         view_handle_free  (FileHandle* fh);

static gboolean     view_file_page_in (RepoFileHandle *handle,
				       void           *pgaddr,
				       gssize          page_no,
				       gsize           in_len);

static gboolean     view_file_page_out (RepoFileHandle *handle,
					void           *pgaddr,
					gssize          page_no,
					gsize           out_len);

static HandleFuncTable view_table = {
  view_handle_close,
  view_handle_abort,
  view_handle_name,
  view_handle_free,
  mem_pool_map_page,
  mem_pool_unmap_page,
  NULL,
  NULL,
  NULL
};

static PageManager*
view_pager (void)
{
  static PageManager* pager = NULL;

  if (! pager)
    {
      if (! (pager = mem_pool_pager_new (SEGMENT_VIEW_PAGE_SIZE, view_file_page_in, view_file_page_out)))
	return NULL;
    }

  return pager;
}

FileHandle*
view_file_handle_new (FileSegment  *seg, SegmentView  *view, gint flags)
{
  ViewHandle* vfh;

  vfh = g_new0 (ViewHandle, 1);

  vfh->handle.handle.table = & view_table;
  vfh->handle.handle.fh_open_flags = flags;
  vfh->handle.handle.fh_open_mode = flags & (HV_Read | HV_Replace);
  vfh->handle.handle.fh_has_len = TRUE;

  vfh->handle.pager = view_pager ();

  vfh->seg = seg;
  vfh->view = view;

  file_position_from_abs (SEGMENT_VIEW_PAGE_SIZE, 0,                    & vfh->handle.handle.fh_cur_pos);
  file_position_from_abs (SEGMENT_VIEW_PAGE_SIZE, segment_length (seg), & vfh->handle.handle.fh_file_len);

  return & vfh->handle.handle;
}

gboolean
view_file_page_in (RepoFileHandle *handle,
		   void           *pgaddr,
		   gssize          page_no,
		   gsize           in_len)
{
  ViewHandle* vfh = (ViewHandle*) handle;

  return vfh->view->page_in (vfh->seg, vfh->view, pgaddr, page_no, in_len);
}

gboolean
view_file_page_out (RepoFileHandle *handle,
		    void       *pgaddr,
		    gssize      page_no,
		    gsize       out_len)
{
  abort ();
  return TRUE;
}

gboolean
view_handle_close (FileHandle* fh)
{
  return TRUE;
}

void
view_handle_abort (FileHandle* fh)
{
}

const gchar*
view_handle_name  (FileHandle *fh)
{
  ViewHandle* vfh = (ViewHandle*) fh;

  return segment_handle_to_string (vfh->seg);
}

void
view_handle_free  (FileHandle* fh)
{
  g_free (fh);
}

gboolean
segment_view_create (FileSegment* seg, guint len, SegmentView* view)
{
  if (! seg->repo->supports_segments)
    {
      repo_generate_pathpath_event (EC_RepoSegmentsNotSupported, seg->seg_file->driver_path, seg->seg_name);
      return FALSE;
    }

  if (segment_is_not_type (seg, SV_NotPresent))
    return FALSE;

  if (seg->seg_file->type == FV_Sequence)
    {
      File *file;

      if (! (file = file_sequence_next_internal (seg->seg_file)))
	return FALSE;

      seg = file_segment (file, seg->seg_name);
    }

  if (! seg->repo->driver->simp_driver.make_view (seg, len, view->name))
    return FALSE;

  if (! driver_set_segment_state (seg, NULL, SV_View))
    return FALSE;

  return TRUE;
}
