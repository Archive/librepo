/* -*-Mode: C;-*-
 * $Id$
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "repo.h"
#include "repopriv.h"

/* Constructors
 */

DirectoryHandle*
file_cont_open (File* cont)
{
  return file_cont_prefix (cont, "");
}

DirectoryHandle*
file_cont_prefix (File *cont, const char *prefix)
{
  DirectoryHandle *dh;

  g_return_val_if_fail (cont, NULL);

  if (file_is_not_type (cont, FV_Directory))
    return NULL;

  dh = cont->repo->driver->simp_driver.cont_open (cont->driver_file, prefix);

  g_assert (dh->type);

  if (dh)
    g_hash_table_insert (cont->repo->open_directory_handles, dh, dh);

  return dh;
}

DirectoryHandle*            file_cont_prefix_len (File         *cont,
						  const guint8 *prefix,
						  guint         len);

DirectoryHandle*            file_cont_lookup     (File           *index,
						  const char     *key);
DirectoryHandle*            file_cont_lookup_len (File           *index,
						  const guint8   *key,
						  guint           key_len);

DirectoryHandle*            file_cont_join      (File             *index,
						 DirectoryHandle **dh_array,
						 guint             dh_array_len);

ContainerType               file_cont_type     (File         *index);

const char*                 file_cont_type_to_string (ContainerType type);

Path*                       dh_next      (DirectoryHandle* dh);
gboolean                    dh_close     (DirectoryHandle* dh);
ContainerType               dh_type      (DirectoryHandle* dh);

#if 0

static DirectoryHandle*
file_index_iterate (File *index, IndexQueryFlag flags)
{
  DirectoryHandle *dh;

  g_return_val_if_fail (index, FALSE);

  g_assert ((flags & ~ (IQF_NoIndexKey)) == 0);

  if (file_is_not_type (index, FV_Index))
    return FALSE;

  dh = index->repo->driver->simp_driver.index_iterate (index->driver_file, flags);

  if (dh)
    {
      dh->type = file_index_type (index);
      dh->flags = flags;

      g_hash_table_insert (index->repo->open_directory_handles, dh, dh);
    }

  return dh;
}

DirectoryHandle*
file_opendir_prefix (File* file, const char* prefix)
{
  DirectoryHandle *dh;

  g_return_val_if_fail (file && prefix, NULL);

  if (file_is_not_type (file, FV_Directory))
    return NULL;

  /* @@@ Check for no separator character... */

  dh = file->repo->driver->simp_driver.opendir (file->driver_file, prefix);

  if (dh)
    {
      dh->type = DHT_Directory;

      g_hash_table_insert (file->repo->open_directory_handles, dh, dh);
    }

  return dh;
}

DirectoryHandle*
file_index_query (File           *index,
		  const guint8   *key,
		  guint           key_len,
		  IndexQueryFlag  flags)
{
  DirectoryHandle *dh;
  guint open_mode = flags & (IQF_Duplicates | IQF_Range);

  g_assert ((flags & ~ (IQF_Range | IQF_Duplicates | IQF_NoIndexKey)) ==0);

  switch (open_mode)
    {
    case IQF_Duplicates:
    case IQF_Range:
      break;
    default:
      abort ();
    }

  g_return_val_if_fail (index && (key || key_len == 0), FALSE);

  if (file_is_not_type (index, FV_Index))
    return FALSE;

  dh = index->repo->driver->simp_driver.index_query (index->driver_file, key, key_len, flags);

  if (dh)
    {
      dh->flags = flags;
      dh->open_mode = open_mode;
      dh->type = file_index_type (index);

      g_hash_table_insert (index->repo->open_directory_handles, dh, dh);
    }

  return dh;
}

/* Operations
 */

gboolean
dh_index_key (DirectoryHandle* dh, const guint8** key, guint* key_len)
{
  g_return_val_if_fail (dh && key && key_len, FALSE);

  if (! dh->index_key || (dh->flags & IQF_NoIndexKey))
    {
      repo_generate_void_event (EC_RepoIndexKeyNotAvailable);
      return FALSE;
    }

  return dh->index_key (dh, key, key_len);
}

Path*
dh_next (DirectoryHandle* dh)
{
  Path *r;

  g_return_val_if_fail (dh, NULL);

  if (dh->failure || dh->done)
    return NULL;

  if (! dh->next (dh, & r))
    {
      dh->failure = TRUE;
      return NULL;
    }

  if (! r)
    dh->done = TRUE;

  return r;
}

gboolean
dh_close (DirectoryHandle* dh)
{
  gboolean failure;

  g_return_val_if_fail (dh, NULL);

  failure = dh->failure;

  g_assert (g_hash_table_lookup (dh->repo->open_directory_handles, dh) == dh);

  g_hash_table_remove (dh->repo->open_directory_handles, dh);

  if (! dh->close (dh))
    return FALSE;

  return ! failure;
}

DHType
dh_type (DirectoryHandle* dh)
{
  return dh->type;
}

/* External functions.
 */

gboolean
file_index_lookup (File         *index,
		   const guint8 *key,
		   guint         key_len,
		   Path        **result)
{
  DirectoryHandle* dh;

  g_return_val_if_fail (index && key && key_len > 0 && result, FALSE);

  if (file_is_not_type (index, FV_Index))
    return FALSE;

  if (! (dh = file_index_query (index, key, key_len, IQF_Duplicates)))
    return FALSE;

  (* result) = dh_next (dh);

  if (! dh_close (dh))
    return FALSE;

  return TRUE;
}
#endif
