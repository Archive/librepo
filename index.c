/* -*-Mode: C;-*-
 * $Id: index.c 1.8 Mon, 03 May 1999 19:48:53 -0700 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "repo.h"
#include "repopriv.h"

typedef gboolean  (ProcessFile) (DriverFile* index, DriverFile* file, const guint8* key, guint key_len);

static gboolean file_index_link_exists (File* index, File* dir, gboolean* exists);
static gboolean file_index_process_file (File* index, File* file, gboolean insert, ProcessFile *proc);
static void unpack_ilinks (Ilinks* ils);

static gboolean file_index_trigger (gint32       code,
				    const char  *code_name,
				    File        *target_dir,
				    File        *target_file,
				    Path        *target_seg_name,
				    FileHandle  *target_handle,
				    const char  *trigger_argument,
				    gint32       trigger_id,
				    gint32       trigger_flags,
				    gint32       trigger_parent_id,
				    gint32       trigger_event_type);

static RepoFileIlinksProperty FP_Ilinks;

gboolean
index_library_init (void)
{
  if (! edsio_new_file_ilinks_property ("Ilinks", PF_Persistent, & FP_Ilinks))
    return FALSE;

  if (! trigger_type_register_internal (INDEX_TRIGGER_TYPE, "Index Trigger", file_index_trigger))
    return FALSE;

  return TRUE;
}

gboolean
file_index_trigger (gint32       code,
		    const char  *code_name,
		    File        *target_dir,
		    File        *target_file,
		    Path        *target_seg_name,
		    FileHandle  *target_handle,
		    const char  *trigger_argument,
		    gint32       trigger_id,
		    gint32       trigger_flags,
		    gint32       trigger_parent_id,
		    gint32       trigger_event_type)
{
  Path* index_path;
  File* index;

  if (! (index_path = path_canonicalize (target_dir->repo, trigger_argument)))
    return FALSE;

  index = file_initialize (target_dir->repo, index_path);

  if (file_is_not_type (index, FV_Index))
    return FALSE;

  switch (trigger_event_type)
    {
    case TE_SegmentDelete:

      if (! file_index_process_file (index, target_file, FALSE, index->repo->driver->simp_driver.index_delete_file))
	return FALSE;

      break;

    case TE_SegmentCreate:

      if (! file_index_process_file (index, target_file, TRUE, index->repo->driver->simp_driver.index_insert_file))
	return FALSE;

      break;

    case TE_ContainerDelete: /* Called during rmdir (assume no entries) */
    case TE_TriggerDelete:   /* Called during file_index_unlink */
      {

	Ilinks* ils;
	const char* dstr;
	DirectoryHandle* dh;
	Path *p;
	int i;
	gboolean found = FALSE;

	/* Update the index's ilinks
	 */

	g_assert (file_test_ilinks (index, FP_Ilinks));

	if (! file_get_ilinks (index, FP_Ilinks, & ils))
	  return FALSE;

	unpack_ilinks (ils);

	dstr = path_to_string (target_dir->repo, file_dest_path (target_dir));

	for (i = 0; i < ils->dpaths_len; i += 1)
	  {
	    if (strcmp (dstr, ils->dpaths[i]) == 0)
	      {
		if (i < (ils->dpaths_len-1))
		  ils->dpaths_write->pdata[i] = ils->dpaths_write->pdata[ils->dpaths_len-1];

		found = TRUE;
		g_ptr_array_set_size (ils->dpaths_write, ils->dpaths_len-1);
		break;
	      }
	  }

	g_assert (found);

	ils->dpaths = (const char**) ils->dpaths_write->pdata;
	ils->dpaths_len = ils->dpaths_write->len;

	if (! file_set_ilinks (index, FP_Ilinks, ils))
	  return FALSE;

	/* Delete current files
	 */

	if (trigger_event_type == TE_TriggerDelete)
	  {
	    if (! (dh = file_cont_open (target_dir)))
	      return FALSE;

	    while ((p = dh_next (dh)))
	      {
		File* f = file_initialize_link (index->repo, p);

		switch (file_type (f))
		  {
		  case FV_Regular:
		  case FV_NotPresent:
		    if (! file_index_process_file (index, f, FALSE, index->repo->driver->simp_driver.index_delete_file))
		      return FALSE;
		    break;

		  case FV_IrregularCase:
		    break;

		  case FV_Invalid:
		    abort ();
		    break;
		  }
	      }

	    if (! dh_close (dh))
	      return FALSE;
	  }
      }
      break;
    case TE_Create: /* Called during file_index_link */
      {

	Ilinks* ils;
	const char* dstr;
	DirectoryHandle* dh;
	Path *p;

	/* Update the index's ilinks
	 */

	if (! file_test_ilinks (index, FP_Ilinks))
	  {
	    ils = g_new0 (Ilinks, 1);
	  }
	else
	  {
	    if (! file_get_ilinks (index, FP_Ilinks, & ils))
	      return FALSE;
	  }

	unpack_ilinks (ils);

	dstr = path_to_string (target_dir->repo, file_dest_path (target_dir));

	g_ptr_array_add (ils->dpaths_write, g_strdup (dstr));

	ils->dpaths = (const char**) ils->dpaths_write->pdata;
	ils->dpaths_len = ils->dpaths_write->len;

	if (! file_set_ilinks (index, FP_Ilinks, ils))
	  return FALSE;

	/* Insert current files
	 */
	if (! (dh = file_cont_open (target_dir)))
	  return FALSE;

	while ((p = dh_next (dh)))
	  {
	    File* f = file_initialize_link (index->repo, p);

	    switch (file_type (f))
	      {
	      case FV_Regular:
	      case FV_NotPresent:
		if (! file_index_process_file (index, f, TRUE, index->repo->driver->simp_driver.index_insert_file))
		  return FALSE;
		break;

	      case FV_IrregularCase:
		break;

	      case FV_Invalid:
		abort ();
		break;
	      }
	  }

	if (! dh_close (dh))
	  return FALSE;
      }
      break;
    default:
      abort ();
    }

  return TRUE;
}

gboolean
file_index_process_file (File* index, File* file, gboolean insert, ProcessFile *proc)
{
  Path* sn;
  FileSegment *seg;

  if (! (sn = file_index_seg_name (index)))
    return FALSE;

  seg = file_segment (file, sn);

  switch (seg->seg_type)
    {
    case SV_View:
    case SV_Regular:
      {
	guint8* key;
	gssize  key_len;
	FileHandle *fh;

	/* @@@ This can be optimized... since the immediate segment is available on insert. */

	if ((key_len = segment_length (seg)) < 0)
	  return FALSE;

	if (key_len > INDEX_KEY_MAX_SIZE)
	  {
	    if (insert)
	      repo_generate_segmentpath_event (EC_RepoIndexedSegmentTooLong, seg, index->driver_file->driver_path);
	    return TRUE;
	  }

	key = g_malloc (key_len);

	if (! (fh = segment_open (seg, HV_Read)))
	  return FALSE;

	if (handle_read (fh, key, key_len) != key_len)
	  return FALSE;

	if (! handle_close (fh))
	  return FALSE;

	if (! proc (index->driver_file, file->driver_file, key, key_len))
	  return FALSE;

	g_free (key);
      }
      break;

    case SV_NotPresent:
      break;

    case SV_Invalid:
      abort ();
      }

  return TRUE;
}

ContainerType
file_index_type (File *index)
{
  if (file_is_not_type (index, FV_Index))
    return CT_Invalid;

  return index->driver_file->driver_indextype (index->driver_file);
}

gboolean
file_index_create (File         *file_stat,
		   Path         *seg_name,
		   ContainerType     type)
{
  File* file;

  g_return_val_if_fail (file_stat && seg_name, FALSE);

  if (seg_name == path_root ())
    {
      repo_generate_void_event (EC_RepoCannotIndexDefaultSegment);
      return FALSE;
    }

  if (file_is_temp (file_stat))
    return FALSE;

  file = driver_lstat (file_stat);

  if (file_is_type_noerr (file, FV_Sequence))
    {
      if (! (file = file_sequence_next_internal (file->driver_file)))
	return FALSE;
    }

  if (file_may_not_access (file, AT_Create))
    return NULL;

  if (! (* file->repo->driver->simp_driver.make_index) (file->driver_file, seg_name, type))
    return FALSE;

  if (! driver_set_file_state (file->driver_file, FV_Index))
    return FALSE;

  return TRUE;
}

Path*
file_index_seg_name (File *index)
{
  g_return_val_if_fail (index, NULL);

  if (file_is_not_type (index, FV_Index))
    return FALSE;

  return index->driver_file->driver_segname (index->driver_file);
}

gboolean
file_index_link_exists (File* index, File* dir, gboolean* exists)
{
  Ilinks* ils;
  const char* dstr;
  int i;

  (* exists) = FALSE;

  if (! file_test_ilinks (index, FP_Ilinks))
    return TRUE;

  if (! file_get_ilinks (index, FP_Ilinks, & ils))
    return FALSE;

  dstr = path_to_string (dir->repo, file_dest_path (dir));

  for (i = 0; i < ils->dpaths_len; i += 1)
    {
      if (strcmp (dstr, ils->dpaths[i]) == 0)
	{
	  (* exists) = TRUE;
	  break;
	}
    }

  return TRUE;
}

void
unpack_ilinks (Ilinks* ils)
{
  int i;

  if (! ils->dpaths_write)
    ils->dpaths_write = g_ptr_array_new ();

  for (i = 0; i < ils->dpaths_len; i += 1)
    g_ptr_array_add (ils->dpaths_write, (void*) ils->dpaths[i]);
}

gboolean
file_index_link (File         *index,
		 File         *dir,
		 gboolean      recurse)
{
  gboolean exists;
  const char* istr;
  Path *sn;

  g_return_val_if_fail (index && dir, FALSE);

  if (file_is_not_type (index, FV_Index))
    return FALSE;

  if (file_is_not_type (dir, FV_Directory))
    return FALSE;

  if (! file_index_link_exists (index, dir, & exists))
    return FALSE;

  if (exists)
    {
      repo_generate_filefile_event (EC_RepoIndexAlreadyLinked, index, dir);
      return TRUE;
    }

  /* Add a trigger to update the index.  The trigger's create event
   * updates the index's ilinks and does initial population.
   */
  istr = path_to_string (index->repo, file_dest_path (index));

  if (! (sn = file_index_seg_name (index)))
    return FALSE;

  if (! trigger_add (dir,
		     INDEX_TRIGGER_TYPE,
		     sn,
		     TE_SegmentCreate | TE_SegmentDelete | TE_ContainerDelete | TE_TriggerDelete | TE_Create,
		     recurse ? TF_Recursive : 0,
		     istr))
    return FALSE;

  return TRUE;
}

gboolean
file_index_unlink (File         *index,
		   File         *dir,
		   gboolean      recurse)
{
  gboolean exists;
  Triggers *ts;
  const char *istr;
  int i;

  g_assert (! recurse);

  g_return_val_if_fail (index && dir, FALSE);

  if (file_is_not_type (index, FV_Index))
    return FALSE;

  if (file_is_not_type (dir, FV_Directory))
    return FALSE;

  if (! file_index_link_exists (index, dir, & exists))
    return FALSE;

  if (! exists)
    {
      repo_generate_filefile_event (EC_RepoIndexNotLinked, index, dir);
      return TRUE;
    }

  if (! file_triggers (dir, & ts))
    return FALSE;

  istr = path_to_string (index->repo, file_dest_path (index));

  for (i = 0; i < ts->val_len; i += 1)
    {
      Trigger *t = ts->val[i];

      if (t->code == INDEX_TRIGGER_TYPE &&
	  strcmp (istr, t->argument) == 0)
	{
	  if (! trigger_delete (dir->repo,
				dir,
				t->id))
	    return FALSE;

	  break;
	}
    }

  return TRUE;
}

gboolean
file_index_delete_triggers (File *index)
{
  Ilinks *ils;
  int i, j;
  const char *istr = path_to_string (index->repo, index->driver_file->driver_path);

  if (! file_test_ilinks (index, FP_Ilinks))
    return TRUE;

  if (! file_get_ilinks (index, FP_Ilinks, & ils))
    return FALSE;

  for (i = 0; i < ils->dpaths_len; i += 1)
    {
      Path* p;
      File* d;
      Triggers *ts;
      gboolean found = FALSE;

      if (! (p = path_canonicalize (index->repo, ils->dpaths[i])))
	return FALSE;

      d = file_initialize (index->repo, p);

      g_assert (d->driver_file->type == FV_Directory);

      if (! file_triggers (d, & ts))
	return FALSE;

      for (j = 0; j < ts->val_len; j += 1)
	{
	  Trigger *t = ts->val[j];

	  if (t->code == INDEX_TRIGGER_TYPE &&
	      strcmp (t->argument, istr) == 0)
	    {
	      if (! trigger_delete_internal (index->repo,
					     d,
					     FALSE,
					     t->id,
					     t->parent_id))
		return FALSE;

	      found = TRUE;
	      break;
	    }
	}

      g_assert (found);
    }

  if (! file_unset_ilinks (index, FP_Ilinks))
    return FALSE;

  return TRUE;
}

const char*
file_cont_type_to_string (ContainerType type)
{
  switch (type)
    {
    case IT_Hash:
      return "Hash index";
    case IT_Btree:
      return "Btree index";
    case IT_Recno:
      return "Recno index";
    default:
      abort ();
    }
}
