/* -*-Mode: C;-*-
 * $Id: trigger.c 1.5 Fri, 30 Apr 1999 01:29:46 -0700 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "repo.h"
#include "repopriv.h"

typedef struct _TriggerType TriggerType;

struct _TriggerType {
  gint32            code;
  const char       *code_name;
  DirectoryTrigger *trigger_func;
};

static GHashTable* all_trigger_types;

static RepoFileTriggersProperty FP_Triggers;

static TriggerType* trigger_type_lookup (File* dir, gint32 code);
static gboolean trigger_container_delete (File *dir);

static gint32 trigger_add_internal (File             *dir,
				    gint32           code,
				    Path             *seg_name_mask,
				    gint32           trigger_event_mask,
				    gint32           trigger_flags,
				    gint32            parent_id,
				    const char       *argument);

gboolean
trigger_library_init (void)
{
  if (! edsio_new_file_triggers_property ("Triggers", PF_Persistent, & FP_Triggers))
    return FALSE;

  return TRUE;
}

gboolean
trigger_type_register (gint32           code,
		       const char       *code_name,
		       DirectoryTrigger *trigger_func)
{
  if (! (code % 2))
    {
      repo_generate_void_event (EC_RepoInvalidTriggerCode);
      return FALSE;
    }

  return trigger_type_register_internal (code, code_name, trigger_func);
}

gboolean
trigger_type_register_internal (gint32           code,
				const char       *code_name,
				DirectoryTrigger *trigger_func)
{
  TriggerType* tt;

  if (! all_trigger_types)
    all_trigger_types = g_hash_table_new (g_int_hash, g_int_equal);

  if ((tt = g_hash_table_lookup (all_trigger_types, & code)))
    {
      if (strcmp (tt->code_name, code_name) == 0 &&
	  tt->trigger_func == trigger_func)
	return TRUE;

      repo_generate_string_event (EC_RepoTriggerTypeAlreadyRegistered, code_name);
      return FALSE;
    }

  tt = g_new0 (TriggerType, 1);

  tt->code = code;
  tt->code_name = code_name;
  tt->trigger_func = trigger_func;

  g_hash_table_insert (all_trigger_types, & tt->code, tt);

  return TRUE;
}

gint32
trigger_add (File             *dir,
	     gint32           code,
	     Path             *seg_name_mask,
	     gint32           trigger_event_mask,
	     gint32           trigger_flags,
	     const char       *argument)
{
  return trigger_add_internal (dir, code, seg_name_mask, trigger_event_mask, trigger_flags, 0, argument);
}

static gboolean
unpack_triggers (Triggers *ts)
{
  if (! ts->val_write)
    {
      int i;

      ts->val_write = g_ptr_array_new ();

      for (i = 0; i < ts->val_len; i += 1)
	{
	  Trigger* t = ts->val[i];

	  g_ptr_array_add (ts->val_write, t);

	  if (t->seg_name_mask_str[0] &&
	      ! (t->seg_name_mask = path_canonicalize_simple (path_root (), '/', t->seg_name_mask_str)))
	    return FALSE;
	}
    }

  return TRUE;
}

TriggerType*
trigger_type_lookup (File* dir, gint32 code)
{
  TriggerType *tt;

  if (! all_trigger_types ||
      ! (tt = g_hash_table_lookup (all_trigger_types, & code)))
    {
      repo_generate_fileint_event (EC_RepoNoSuchTriggerCode, dir, code);
      return NULL;
    }

  return tt;
}

gint32
trigger_add_internal (File             *dir,
		      gint32            code,
		      Path             *seg_name_mask,
		      gint32            trigger_event_mask,
		      gint32            trigger_flags,
		      gint32            parent_id,
		      const char       *argument)
{
  TriggerType* tt;
  Triggers* ts;
  Trigger* t;

  if (! (tt = trigger_type_lookup (dir, code)))
    return -1;

  if (! file_test_triggers (dir, FP_Triggers))
    {
      ts = g_new0 (Triggers, 1);

      ts->point = 1;
    }
  else
    {
      if (! file_get_triggers (dir, FP_Triggers, & ts))
	return -1;
    }

  if (! unpack_triggers (ts))
    return FALSE;

  t = g_new0 (Trigger, 1);

  g_ptr_array_add (ts->val_write, t);

  ts->val = (Trigger**) ts->val_write->pdata;
  ts->val_len = ts->val_write->len;

  t->id = ts->point++;
  t->parent_id = parent_id;
  t->seg_name_mask = seg_name_mask;
  t->seg_name_mask_str = seg_name_mask ? g_strdup (pts (seg_name_mask)) : "";
  t->code = code;
  t->flags = trigger_flags;
  t->event_mask = trigger_event_mask;
  t->argument = g_strdup (argument);

  if (! file_set_triggers (dir, FP_Triggers, ts))
    return -1;

  if ((t->event_mask & TE_Create) &&
      ! tt->trigger_func (tt->code,
			  tt->code_name,
			  dir,
			  NULL,
			  NULL,
			  NULL,
			  t->argument,
			  t->flags,
			  t->id,
			  t->parent_id,
			  TE_Create))
    return -1;

  if (t->flags & TF_Recursive)
    {
      DirectoryHandle* dh;
      Path *p;

      if (! (dh = file_opendir (dir)))
	return -1;

      while ((p = dh_next (dh)) != NULL)
	{
	  File* f = file_initialize (dh->repo, p);

	  if (f->driver_file->type == FV_Directory)
	    {
	      if (! trigger_add_internal (f,
					  code,
					  seg_name_mask,
					  trigger_event_mask,
					  trigger_flags,
					  t->id,
					  argument))
		return -1;
	    }
	}

      if (! dh_close (dh))
	return -1;
    }

  return t->id;
}

gboolean
trigger_delete (Repository       *repo,
		File             *dir,
		gint32            trigger_id)
{
  return trigger_delete_internal (repo, dir, TRUE, trigger_id, 0);
}

gboolean
trigger_delete_internal (Repository       *repo,
			 File             *dir,
			 gboolean          deliver,
			 gint32            trigger_id,
			 gint32            parent_id)
{
  Triggers* ts;
  int i;

  if (! file_test_triggers (dir, FP_Triggers))
    {
      repo_generate_intfile_event (EC_RepoNoSuchTriggerId, trigger_id, dir);
      return FALSE;
    }
  else
    {
      if (! file_get_triggers (dir, FP_Triggers, & ts))
	return FALSE;
    }

  if (! unpack_triggers (ts))
    return FALSE;

  for (i = 0; i < ts->val_len; i += 1)
    {
      Trigger* t = ts->val[i];

      if ((trigger_id && (t->id == trigger_id)) ||
	  (parent_id  && (t->parent_id == parent_id)))
	{
	  TriggerType *tt;

	  if (! (tt = trigger_type_lookup (dir, t->code)))
	    return FALSE;

	  g_ptr_array_remove_index_fast (ts->val_write, i);

	  ts->val = (Trigger**) ts->val_write->pdata;
	  ts->val_len = ts->val_write->len;

	  /* could unset it if len=0, but it will call ContainerDelete accidently? */
	  if (! file_set_triggers (dir, FP_Triggers, ts))
	    return FALSE;

	  if (deliver &&
	      (t->event_mask & TE_TriggerDelete) &&
	      ! tt->trigger_func (tt->code,
				  tt->code_name,
				  dir,
				  NULL,
				  NULL,
				  NULL,
				  t->argument,
				  t->flags,
				  t->id,
				  t->parent_id,
				  TE_TriggerDelete))
	    return FALSE;

	  if (t->flags & TF_Recursive)
	    {
	      DirectoryHandle* dh;
	      Path *p;

	      if (! (dh = file_opendir (dir)))
		return FALSE;

	      while ((p = dh_next (dh)) != NULL)
		{
		  File* f = file_initialize (dh->repo, p);

		  if (f->driver_file->type == FV_Directory)
		    {
		      if (! trigger_delete_internal (f->repo,
						     f,
						     deliver,
						     0,
						     t->id))
			return FALSE;
		    }
		}

	      if (! dh_close (dh))
		return FALSE;
	    }

	  return TRUE;
	}
    }

  repo_generate_intfile_event (EC_RepoNoSuchTriggerId, trigger_id, dir);
  return FALSE;
}

gboolean
file_triggers (File* file, Triggers** ts)
{
  g_assert (file_test_triggers (file, FP_Triggers));

  if (! file_get_triggers (file, FP_Triggers, ts))
    return FALSE;

  if (! unpack_triggers (*ts))
    return FALSE;

  return TRUE;
}

gboolean
trigger_container_delete (File *dir)
{
  TriggerType *tt;
  Triggers* ts;
  int i;

  if (! file_test_triggers (dir, FP_Triggers))
    return TRUE;

  if (! file_get_triggers (dir, FP_Triggers, & ts))
    return FALSE;

  for (i = 0; i < ts->val_len; i += 1)
    {
      Trigger *t = ts->val[i];

      if (t->event_mask & TE_ContainerDelete)
	{
	  if (! (tt = trigger_type_lookup (dir, t->code)))
	    return FALSE;

	  if (! tt->trigger_func (tt->code,
				  tt->code_name,
				  dir,
				  NULL,
				  NULL,
				  NULL,
				  t->argument,
				  t->flags,
				  t->id,
				  t->parent_id,
				  TE_ContainerDelete))
	    return FALSE;
	}
    }

  if (! file_unset_triggers (dir, FP_Triggers))
    return FALSE;

  return TRUE;
}

gboolean
trigger_execute (File* dir, File *target, Path* target_segment_name, FileHandle* target_handle, FileSegment* target_handle_seg, gint32 event_type)
{
  Triggers* ts;
  int i;

  /* First if a dir is being deleted, delete its triggers. */
  if (event_type == TE_DirectoryDelete)
    {
      if (! trigger_container_delete (target))
	return FALSE;
    }

  /* Second, if an index is being deleted, delete its triggers. */
  if (event_type == TE_IndexDelete)
    {
      if (! file_index_delete_triggers (target))
	return FALSE;
    }

  /* Third, delete auto segments. */
  if (event_type == TE_SegmentDelete)
    {
      if (! auto_property_delete_trigger (dir, target, target_segment_name, event_type))
	return FALSE;
    }

  /* Fourth, verify digest assertions on open_read. */
  if (event_type == TE_OpenRead)
    {
      if (! auto_digest_verify (target_handle_seg, target_handle))
	return FALSE;

      /* @@@ I'm not delivering this, because it causes nasty loops. */
      return TRUE;
    }

  /* Now execute this dir's triggers
   */

  if (! dir->repo->supports_segments)
    return TRUE;

  if (! file_test_triggers (dir, FP_Triggers))
    return TRUE;

  if (! file_get_triggers (dir, FP_Triggers, & ts))
    return FALSE;

  if (! unpack_triggers (ts))
    return FALSE;

  for (i = 0; i < ts->val_len; i += 1)
    {
      Trigger* trigger = ts->val[i];
      TriggerType* tt;

      /* Recursive propagation
       */
      if (event_type == TE_DirectoryCreate && (trigger->flags & TF_Recursive))
	{
	  if (! trigger_add_internal (target,
				      trigger->code,
				      trigger->seg_name_mask,
				      trigger->event_mask,
				      trigger->flags,
				      trigger->id,
				      trigger->argument))
	    return FALSE;
	}

      if (! (trigger->event_mask & event_type))
	continue;

      if (target_segment_name && trigger->seg_name_mask && trigger->seg_name_mask != target_segment_name)
	continue;

      if (trigger->flags & TF_ArgumentPrefixMask &&
	  strncmp (trigger->argument, path_basename (target->driver_file->driver_path), strlen (trigger->argument)) != 0)
	continue;

      if (! (tt = trigger_type_lookup (dir, trigger->code)))
	return FALSE;

      if (! tt->trigger_func (tt->code,
			      tt->code_name,
			      dir,
			      target,
			      target_segment_name,
			      target_handle,
			      trigger->argument,
			      trigger->flags,
			      trigger->id,
			      trigger->parent_id,
			      event_type))
	{
	  repo_generate_filestring_event (EC_RepoTriggerFailed, dir, tt->code_name);
	  return FALSE;
	}
    }

  return TRUE;
}
