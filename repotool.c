/* -*-Mode: C;-*-
 * $Id: repotool.c 1.1 Mon, 03 May 1999 19:48:53 -0700 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "repo.h"
#include <stdio.h>

int
main (int argc, char** argv)
{
  Repository *repo;
  int ret = 0;

  if (argc != 2)
    {
      fprintf (stderr, "usage: %s repository\n", argv[0]);
      exit (1);
    }

  repository_system_init ();

  if (repository_exists (argv[1]))
    {
      if (! (repo = repository_initialize (argv[1])))
	exit (1);
    }
  else
    {
      if (! (repo = repository_create (argv[1])))
	exit (1);
    }

  repository_tool (repo);

  if (ret == 0)
    {
      if (repository_commit (repo))
	{
	  printf ("commit succeeded\n");
	}
      else
	{
	  fprintf (stderr, "commit failed\n");
	  ret = 1;
	}
    }

  repository_close (repo);

  repository_system_close ();

  return ret;
}
