/* -*-Mode: C;-*-
 * $Id: repodump.c 1.10 Fri, 09 Apr 1999 23:16:38 -0700 jmacd $
 *
 * Copyright (C) 1997, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "repo.h"
#include "repopriv.h"
#include <edsiostdio.h>

int
main (int argc, char** argv)
{
  Repository *repo;
  int res = 0;

  if (argc != 2)
    {
      g_print ("usage: %s repository\n", argv[0]);
      exit (1);
    }

  if (! repository_system_init ())
    exit (1);

  if (! (repo = repository_initialize (argv[1])))
    {
      res = 1;
      goto done;
    }

  if (! repo_list (repo))
    res = 1;

 done:

  repository_close (repo);

  repository_system_close ();

  return res;
}
