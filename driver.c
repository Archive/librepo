/* -*-Mode: C;-*-
 * $Id: driver.c 1.85 Sun, 02 May 1999 04:53:40 -0700 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "repo.h"
#include "repopriv.h"

#define LFD_Path 0
#define LFD_File 1

static FileSegment* driver_temp_segment_stat   (DriverFile* file, Path* name);
static gboolean     driver_temp_segment_unlink (FileSegment* seg);
static guint        driver_temp_segment_length (FileSegment* seg);

static gboolean driver_file_init_internal_rec (Repository *repo,
					       File       *file,
					       Path       *path,
					       Path       *orig_path,
					       gboolean    follow);

gboolean
driver_library_init ()
{
  return TRUE;
}

/* Driver functions.
 */

static FileSegment*
invalid_segment_stat (DriverFile* file, Path* name)
{
  abort ();
}

static gboolean
invalid_segment_unlink (FileSegment* seg)
{
  abort ();
}

gboolean
driver_init (Repository* repo)
{
  repo->link_file_deps = g_relation_new (2);

  g_relation_index (repo->link_file_deps, LFD_Path, g_direct_hash, g_direct_equal);
  g_relation_index (repo->link_file_deps, LFD_File, g_direct_hash, g_direct_equal);

  repo->invalid_driver_file = g_new0 (DriverFile, 1);

  repo->invalid_driver_file->repo = repo;
  repo->invalid_driver_file->type = FV_Invalid;
  repo->invalid_driver_file->driver_path = NULL; /* @@@ this can't be a path yet... (recursion in path_repository ()) */
  repo->invalid_driver_file->segment_unlink = invalid_segment_unlink;
  repo->invalid_driver_file->segment_stat   = invalid_segment_stat;

  return TRUE;
}

static gboolean
driver_commit (Repository* repo)
{
  return (* repo->driver->simp_driver.commit) (repo);
}

static gboolean
driver_rollback (Repository* repo)
{
  return (* repo->driver->simp_driver.rollback) (repo);
}

static gboolean
driver_close_dir_func (gpointer	key, gpointer value, gpointer user_data)
{
  DirectoryHandle *dh = key;

  dh_close (dh);

  return TRUE;
}

static gboolean
driver_close (Repository* repo)
{
  g_hash_table_foreach_remove (repo->open_directory_handles, driver_close_dir_func, NULL);

  return (* repo->driver->simp_driver.close) (repo);
}

/* File initialization
 */

DriverFile*
driver_lstat_driver_path (Repository* repo, DriverFile* parent, Path* dpath)
{
  DriverFile* df;

  df = g_hash_table_lookup (repo->driver_files, dpath);

  if (df == NULL)
    {
      if (! (df = (* repo->driver->simp_driver.lstat) (repo, parent, dpath)))
	return NULL;

      g_hash_table_insert (repo->driver_files, dpath, df);
    }

  return df;
}

static gboolean
driver_file_init_internal_rec (Repository *repo,
			       File       *file,
			       Path       *path,
			       Path       *orig_path,
			       gboolean    follow)
{
  DriverFile* df;
  DriverFile* parent = NULL;

  /* Expand symlinks in the directory prefix. */
  if (path != path_root ())
    {
      Path* dir_path = path_dirname (path);
      File* dir_file = file_initialize (repo, dir_path);

      if (file_is_not_type (dir_file, FV_Directory))
	return FALSE;

      path = path_append (dir_file->driver_file->driver_path, path_basename (path));
      parent = dir_file->driver_file;
    }

  /* The above step canonicalized the name.
   */
  if (! (df = driver_lstat_driver_path (repo, parent, path)))
    return FALSE;

  if (follow && df->type == FV_SymbolicLink)
    {
      Path* link_path;
      const char* link;

      if (! (link = (* df->driver_readlink) (df)))
	return FALSE;

      if (! (link_path = (* repo->driver->simp_driver.canonicalize_long) (repo,
									  path_dirname (path),
									  link)))
	return FALSE;

      if (g_relation_exists (repo->link_file_deps, path, file))
	{
	  repo_generate_path_event (EC_RepoCyclicLinks, orig_path);
	  return FALSE;
	}

      g_relation_insert (repo->link_file_deps, path, file);

      return driver_file_init_internal_rec (repo, file, link_path, orig_path, follow);
    }

  file->driver_file = df;

  g_relation_insert (repo->link_file_deps, path, file);

  return TRUE;
}

static File*
driver_file_init_internal (Repository       *repo,
			   Path             *path,
			   GHashTable       *table,
			   gboolean          follow)
{
  File* file = g_hash_table_lookup (table, path);

  if (file != NULL)
    return file;

  file = g_new0 (File, 1);

  file->repo = repo;
  file->access_path = path;
  file->is_stat = follow;

  if (! driver_file_init_internal_rec (repo, file, path, path, follow))
    {
      g_free (file);
      return NULL;
    }

  g_ptr_array_add (repo->all_files, file);

  g_hash_table_insert (table, path, file);

  return file;
}

static File*
driver_file_init (Repository *repo, Path *path)
{
  return driver_file_init_internal (repo, path, repo->stat_files, TRUE);
}

static File*
driver_link_init (Repository *repo, Path *path)
{
  return driver_file_init_internal (repo, path, repo->lstat_files, FALSE);
}

static File*
driver_temp_init (Repository *repo)
{
  File* file = g_new0 (File, 1);
  DriverFile *dfile = g_new0 (DriverFile, 1);
  Path* tpath = path_append_format (path_absolute ("tmp"), "temp.%d", repo->temp_count++);

  file->repo = repo;
  file->access_path = tpath;
  file->is_stat = TRUE;
  file->driver_file = dfile;

  dfile->repo = repo;
  dfile->type = FV_NotPresent;
  dfile->driver_path = tpath;
  dfile->is_temp = TRUE;

  dfile->segment_stat   = driver_temp_segment_stat;
  dfile->segment_unlink = driver_temp_segment_unlink;
  dfile->segment_length = driver_temp_segment_length;
  /* @@@ Missing 5 fields. */

  return file;
}

/* State changes.
 */

File*
df2f (DriverFile *df)
{
  return file_initialize_link (df->repo, df->driver_path);
}

File*
df2pf (DriverFile *df)
{
  return file_initialize (df->repo, path_dirname (df->driver_path));
}

File*
driver_lstat (File* file_stat)
{
  if (! file_stat->is_stat)
    return file_stat;

  return file_initialize_link (file_stat->repo, file_stat->access_path);
}

gboolean
driver_set_segment_state (FileSegment *seg, FileHandle* seg_handle, RepoSegmentTypePropertyValue nstate)
{
  RepoSegmentTypePropertyValue ostate = seg->seg_type;
  gint32 trigger_event_code;
  gboolean update_before = TRUE;

  if (ostate == nstate)
    return TRUE;

  /* Sanity checks */
  switch (nstate)
    {
    case SV_Regular:
    case SV_View:
      g_assert (ostate == SV_NotPresent);

      if (seg->seg_is_default)
	g_assert (seg->seg_file->type == FV_NotPresent);

      trigger_event_code = TE_SegmentCreate;
      break;

    case SV_Invalid:
      abort ();
      break;

    case SV_NotPresent:
      g_assert (ostate & (SV_Regular | SV_View));

      update_before = FALSE;

      if (seg->seg_is_default)
	g_assert (seg->seg_file->type == FV_Regular);

      trigger_event_code = TE_SegmentDelete;
      break;
    }

  /* Triggers */

  if (! update_before)
    {
      if (seg->repo->supports_segments && ! trigger_execute (df2pf (seg->seg_file),
							     df2f (seg->seg_file),
							     seg->seg_name,
							     seg_handle,
							     NULL,
							     trigger_event_code))
	return FALSE;
    }

  /* Update seg type */

  seg->seg_type = nstate;

  /* Update file type */

  if (seg->seg_is_default)
    {
      switch (nstate)
	{
	case SV_Regular:
	case SV_View:
	  seg->seg_file->type = FV_Regular;
	  break;

	case SV_NotPresent:
	  seg->seg_file->type = FV_NotPresent;
	  break;

	default:
	  break;
	}
    }

  if (update_before)
    {
      if (seg->repo->supports_segments && ! trigger_execute (df2pf (seg->seg_file),
							     df2f (seg->seg_file),
							     seg->seg_name,
							     seg_handle,
							     NULL,
							     trigger_event_code))
	return FALSE;
    }

  return TRUE;
}

gboolean
driver_set_file_state (DriverFile *file, RepoFileTypePropertyValue nstate)
{
  FileSegment *ds;
  RepoFileTypePropertyValue ostate = file->type;
  gint32 trigger_event_code;
  gboolean update_before = TRUE;

  if (ostate == nstate)
    return TRUE;

  ds = file_segment_internal (file, path_root ());

  /* Sanity checks */
  switch (nstate)
    {
    case FV_Regular:
    case FV_Invalid:
      abort ();
      break;

    case FV_NotPresent:
      break;

    case FV_IrregularCase:
      g_assert (ostate == FV_NotPresent);
      g_assert (ds->seg_type & (SV_NotPresent | SV_Invalid));
      break;
    }

  /* Triggers */
  switch (nstate)
    {
    case FV_NotPresent:
      g_assert (ds->seg_type == SV_Invalid);
      update_before = FALSE;
      switch (ostate)
	{
	case FV_SymbolicLink:
	  trigger_event_code = TE_SymlinkDelete;
	  break;
	case FV_Directory:
	  trigger_event_code = TE_DirectoryDelete;
	  break;
	case FV_Index:
	  trigger_event_code = TE_IndexDelete;
	  break;
	case FV_Sequence:
	  trigger_event_code = TE_SequenceDelete;
	  break;
	case FV_IrregularComplementCase:
	  abort ();
	  break;
	}
      break;
    case FV_SymbolicLink:
      trigger_event_code = TE_SymlinkCreate;
      break;
    case FV_Directory:
      trigger_event_code = TE_DirectoryCreate;
      break;
    case FV_Index:
      trigger_event_code = TE_IndexCreate;
      break;
    case FV_Sequence:
      trigger_event_code = TE_SequenceCreate;
      break;
    case FV_Regular:
    case FV_Invalid:
      abort ();
      break;
    }

  if (! update_before)
    {
      if (file->repo->supports_segments &&
	  ! trigger_execute (df2pf (file),
			     df2f (file),
			     NULL,
			     NULL,
			     NULL,
			     trigger_event_code))
	return FALSE;
    }

  file->type = nstate;

  /* Symlink redirects */
  if ((ostate | nstate) & FV_SymbolicLink)
    {
      GTuples* altered_files = g_relation_select (file->repo->link_file_deps, file->driver_path, LFD_Path);
      gint i;

      for (i = 0; i < altered_files->len; i += 1)
	{
	  File* altered_file = (File*) g_tuples_index (altered_files, i, LFD_File);

	  g_relation_delete (file->repo->link_file_deps, altered_file, LFD_File);

	  if (! driver_file_init_internal_rec (file->repo,
					       altered_file,
					       altered_file->access_path,
					       altered_file->access_path,
					       altered_file->is_stat))
	    altered_file->driver_file = file->repo->invalid_driver_file;
	}
    }

  /* Update segment type */
  switch (nstate)
    {
    case FV_Regular:
    case FV_Invalid:
      abort ();
      break;

    case FV_NotPresent:
    case FV_Sequence:
      ds->seg_type = SV_NotPresent;
      break;

    case FV_SymbolicLink:
    case FV_Directory:
    case FV_Index:
      ds->seg_type = SV_Invalid;
      break;
    }

  if (update_before)
    {
      if (file->repo->supports_segments &&
	  ! trigger_execute (df2pf (file),
			     df2f (file),
			     NULL,
			     NULL,
			     NULL,
			     trigger_event_code))
	return FALSE;
    }

  return TRUE;
}

static gboolean
driver_mkdir (File* file_stat)
{
  File* file;

  if (file_is_temp (file_stat))
    return FALSE;

  file = driver_lstat (file_stat);

  if (file_is_type_noerr (file, FV_Sequence))
    {
      if (! (file = file_sequence_next_internal (file->driver_file)))
	return FALSE;
    }

  if (file_may_not_access (file, AT_Create))
    return FALSE;

  if (! (* file->repo->driver->simp_driver.make_dir) (file->driver_file))
    return FALSE;

  if (! driver_set_file_state (file->driver_file, FV_Directory))
    return FALSE;

  return TRUE;
}

static gboolean
driver_symlink (File* file_stat, const char* path)
{
  File* file;

  if (file_is_temp (file_stat))
    return FALSE;

  file = driver_lstat (file_stat);

  if (file_is_type_noerr (file, FV_Sequence))
    {
      if (! (file = file_sequence_next_internal (file->driver_file)))
	return FALSE;
    }

  if (file_may_not_access (file, AT_Create))
    return FALSE;

  if (! (* file->repo->driver->simp_driver.make_symlink) (file->driver_file, path))
    return FALSE;

  if (! driver_set_file_state (file->driver_file, FV_SymbolicLink))
    return FALSE;

  return TRUE;
}

const char*
file_link (File* file)
{
  if (file_is_not_type (file, FV_SymbolicLink))
    return NULL;

  return file->driver_file->driver_readlink (file->driver_file);
}

gboolean
file_properties (File* file, guint8** arg, guint32* arg_len)
{
  return file->repo->driver->simp_driver.file_properties (file->driver_file, arg, arg_len);
}

static gboolean
driver_erase (File* file_stat, gboolean recurse)
{
  File* file = driver_lstat (file_stat);
  DriverFile *df = file->driver_file;

  /* @@@ You need to make changes here and set a policy regarding
   * deletion of segments.
   */

  if (file_is_type (file, FV_Invalid))
    return FALSE;

  switch (df->type)
    {
    case FV_Invalid:
      abort ();
      break;

    case FV_NotPresent:
    case FV_Regular:
      {
	FileSegment *segs;

	g_assert (file->repo->driver->simp_driver.segments_read);

	if (! df->segs_read && ! file->repo->driver->simp_driver.segments_read (df))
	  return FALSE;

	df->segs_read = TRUE;

	for (segs = df->segment_list; segs; segs = segs->next)
	  {
	    if (segs->seg_type & (SV_View | SV_Regular))
	      {
		if (! driver_set_segment_state (segs, NULL, SV_NotPresent))
		  return FALSE;

		if (! df->segment_unlink (segs))
		  return FALSE;
	      }
	  }

	g_assert (df->type == FV_NotPresent);
      }

      break;
    case FV_SymbolicLink:

      if (! driver_set_file_state (df, FV_NotPresent))
	return FALSE;

      if (! file->repo->driver->simp_driver.delete_symlink (df))
	return FALSE;

      break;
    case FV_Directory:

      {
	DirectoryHandle *dh;
	Path *p;

	if (! (dh = file_opendir (file)))
	  return FALSE;

	while ((p = dh_next (dh)))
	  {
	    File *f = file_initialize_link (df->repo, p);

	    switch (f->driver_file->type)
	      {
	      case FV_Invalid:
		abort ();

	      case FV_Regular:
	      case FV_NotPresent:
	      case FV_IrregularCase:

		if (recurse)
		  {
		    if (! file_erase (f, TRUE))
		      return FALSE;
		  }
		else
		  {
		    repo_generate_file_event (EC_RepoDirectoryNotEmpty, file);
		    return FALSE;
		  }

		break;
	      }
	  }

	if (! dh_close (dh))
	  return FALSE;

	if (! driver_set_file_state (df, FV_NotPresent))
	  return FALSE;

	if (! df->repo->driver->simp_driver.delete_dir (df))
	  return FALSE;
      }

      break;
    case FV_Index:

      if (! driver_set_file_state (df, FV_NotPresent))
	return FALSE;

      if (! file->repo->driver->simp_driver.delete_index (df))
	return FALSE;

      break;
    case FV_Sequence:

      if (! driver_set_file_state (df, FV_NotPresent))
	return FALSE;

      if (! file->repo->driver->simp_driver.delete_sequence (df))
	return FALSE;

      break;
    }

  return TRUE;
}

static gboolean
driver_erase_segment (FileSegment* seg, gboolean force_all)
{
  if (segment_is_not_type (seg, SV_Regular | SV_View))
    return FALSE;

  if (! driver_set_segment_state (seg, NULL, SV_NotPresent))
    return FALSE;

  if (! seg->seg_file->segment_unlink (seg))
    return FALSE;

  return TRUE;
}

/* Temp files
 */

typedef struct _TempFileSegment TempFileSegment;

struct _TempFileSegment {
  FileSegment seg;

  PageManager *pager;

  guint temp_len;
};

static gboolean
driver_temp_handle_replace (FileHandle  *fh,
			    void        *data,
			    gssize       len,
			    PageManager *pager)
{
  TempFileSegment* seg = data;

  if (seg->pager)
    /* @@@ the pager should really have a reference count. */
    mem_pool_pager_free (seg->pager);

  seg->pager = pager;
  seg->temp_len = len;

  seg->seg.seg_type = SV_Regular;

  if (seg->seg.seg_is_default)
    seg->seg.seg_file->type = FV_Regular;

  return TRUE;
}

static FileHandle*
driver_open_temp (FileSegment* rseg, gint flags)
{
  TempFileSegment* seg = (TempFileSegment*) rseg;

  gint open_mode = flags & (HV_Read | HV_Replace);

  switch (open_mode)
    {
    case HV_Read:
      return temp_file_handle_new (seg->pager, seg->temp_len, seg, NULL, segment_handle_to_string, flags);
    default:
      return temp_file_handle_new (NULL, 0, seg, driver_temp_handle_replace, segment_handle_to_string, flags);
    }
}

gboolean
driver_temp_segment_unlink (FileSegment* rseg)
{
  TempFileSegment* seg = (TempFileSegment*) rseg;

  if (seg->pager)
    mem_pool_pager_free (seg->pager);

  seg->pager = NULL;
  seg->temp_len = 0;

  return TRUE;
}

guint
driver_temp_segment_length (FileSegment* rseg)
{
  TempFileSegment* seg = (TempFileSegment*) rseg;

  return seg->temp_len;
}

FileSegment*
driver_temp_segment_stat (DriverFile* file, Path* name)
{
  TempFileSegment *seg = g_new0 (TempFileSegment, 1);

  seg->seg.repo = file->repo;
  seg->seg.seg_file = file;
  seg->seg.seg_name = name;
  seg->seg.seg_is_default = (name == path_root ());
  seg->seg.seg_type = SV_NotPresent;

  return & seg->seg;
}

/* open/close */

static FileHandle*
driver_open_file (FileSegment* seg, gint flags)
{
  FileHandle* fh = NULL;
  gint open_mode = flags & (HV_Read | HV_Replace);

  g_return_val_if_fail (seg, NULL);

  if ((open_mode != HV_Replace) && (open_mode != HV_Read))
    {
      repo_generate_void_event (EC_RepoInvalidArgument);
      return NULL;
    }

  if (segment_is_not_type (seg, SV_NotPresent | SV_Regular | SV_View))
    return NULL;

  switch (open_mode)
    {
    case HV_Read:
      if (segment_is_not_type (seg, SV_Regular | SV_View))
	return NULL;
      break;
    default:
      flags |= HV_NoSeek;
    }

  g_assert (seg->seg_file);

  if (seg->seg_type == SV_View)
    {
      SegmentView* view;

      if (open_mode != HV_Read)
	{
	  repo_generate_segment_event (EC_RepoReadOnlyView, seg);
	  return NULL;
	}

      if (! (view = segment_view_lookup (segment_view_name (seg))))
	return NULL;

      fh = view_file_handle_new (seg, view, flags);
    }
  else if (seg->seg_file->is_temp)
    {
      fh = driver_open_temp (seg, flags);
    }
  else
    {
      switch (open_mode)
	{
	case HV_Read:

	  if (segment_may_not_access (seg, AT_Read))
	    return NULL;

	  fh = (* seg->repo->driver->simp_driver.open_read) (seg, flags);

	  break;
	case HV_Replace:

	  if (seg->seg_file->type == FV_Sequence)
	    {
	      g_assert (seg->repo->driver->simp_driver.sequence_open);

	      fh = seg->repo->driver->simp_driver.sequence_open (seg);
	    }
	  else
	    {
	      if (segment_may_not_access (seg, AT_Replace))
		return NULL;

	      fh = (* seg->repo->driver->simp_driver.open_replace) (seg, flags);
	    }
	}
    }

  if (fh && ! trigger_execute (df2pf (seg->seg_file), NULL, NULL, fh, seg, open_mode == HV_Replace ? TE_OpenReplace : TE_OpenRead))
    return NULL;

  return fh;
}

/* Driver initializations
 */

#define DRIVER(field) driver->field = driver_ ## field;

RepositoryDriver*
driver_new (gint                repo_sizeof,
	    SimpleDriver       *simp_driver,
	    const char         *driver_name)
{
  RepositoryDriver* driver = g_new0 (RepositoryDriver, 1);

  driver->repo_sizeof = repo_sizeof;
  driver->driver_name = driver_name;

  memcpy (&driver->simp_driver, simp_driver, sizeof (*simp_driver));

  DRIVER (commit);
  DRIVER (rollback);
  DRIVER (close);
  DRIVER (file_init);
  DRIVER (link_init);
  DRIVER (temp_init);
  DRIVER (erase);
  DRIVER (erase_segment);
  DRIVER (mkdir);
  DRIVER (symlink);
  DRIVER (open_file);

  repository_register_driver (driver);

  return driver;
}
