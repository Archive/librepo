/* -*-Mode: C;-*-
 * $Id: path.c 1.40 Sun, 02 May 1999 04:53:40 -0700 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "repo.h"
#include "repopriv.h"

struct _Path {
  Path       *container;
  const char *name;
  GHashTable *elements;
};

Path*
path_map (Path* path, const char* (*func) (const char*))
{
  if (path == path_root ())
    return path;

  return path_append (path_map (path_dirname (path), func), func (path_basename (path)));
}

Path*
path_prepend_basenames (Path* path, const char* prefix)
{
  GString* str;

  if (path == path_root ())
    return path;

  str = temporary_string (path_basename (path));

  g_string_prepend (str, prefix);

  return path_append (path_prepend_basenames (path_dirname (path), prefix), str->str);
}

Path*
path_append_basenames  (Path* path, const char* suffix)
{
  GString* str;

  if (path == path_root ())
    return path;

  str = temporary_string (path_basename (path));

  g_string_append (str, suffix);

  return path_append (path_append_basenames (path_dirname (path), suffix), str->str);
}

gboolean
path_will_handle (const char* url, RepositoryDriver* driver)
{
  return FALSE;
}

static Path*
path_alloc (Path* container, const char* name)
{
  static GMemChunk *path_chunk = NULL;
  static GStringChunk* string_chunk = NULL;
  Path* npath;

  if (path_chunk == NULL)
    {
      path_chunk = g_mem_chunk_new ("Path mem chunk", sizeof(Path), 64*sizeof(Path), G_ALLOC_ONLY);
      string_chunk = g_string_chunk_new (64);
    }

  npath = g_chunk_new (Path, path_chunk);

  npath->elements = NULL;
  npath->container = container;
  npath->name = g_string_chunk_insert_const (string_chunk, name);

  return npath;
}

Path* path_intern (const char* name, int segs)
{
  Path* path = path_root();

  for (; segs > 0; segs -= 1)
    {
      path = path_append (path, name);
      name += strlen (name) + 1;
    }

  return path;
}

Path*
path_canonicalize (Repository *repo,
		   const char *url)
{
  g_return_val_if_fail (repo && url, NULL);

  return (* repo->driver->simp_driver.canonicalize) (repo, url);
}

Path*
path_canonicalize_long (Repository *repo,
			Path* rel,
			const char *url)
{
  g_return_val_if_fail (repo && url, NULL);

  return (* repo->driver->simp_driver.canonicalize_long) (repo, rel, url);
}

Path*
path_canonicalize_simple (Path       *relative,
			  char        sep,
			  const char *native_filename)
{
  return path_canonicalize_internal (_fs_repo,
				     relative,
				     sep,
				     TRUE,
				     native_filename);
}

Path*
path_append (Path       *path,
	     const char *simp_name)
{
  Path* npath;

  g_return_val_if_fail (path && simp_name && simp_name[0] != 0, NULL);

  if (path->elements != NULL)
    {
      gpointer lu = g_hash_table_lookup (path->elements, simp_name);

      if (lu)
	return (Path*) lu;
    }
  else
    {
      path->elements = g_hash_table_new (g_str_hash, g_str_equal);
    }

  npath = path_alloc (path, simp_name);

  g_hash_table_insert (path->elements, (void*) npath->name, npath);

  return npath;
}

Path*
path_append_n (Path       *path,
	       const char *simp_name,
	       int         simp_name_len)
{
  char* buf = malloc (simp_name_len+1);
  Path* p;

  buf[simp_name_len] = 0;

  g_assert (path && simp_name && simp_name_len > 0);

  strncpy (buf, simp_name, simp_name_len);

  p = path_append (path, buf);

  g_free (buf);

  return p;
}

Path*
path_append_path (Path *path,
		  Path *apath)
{
  if (path == path_root ())
    return apath;

  if (apath == path_root ())
    return path;

  return path_append (path_append_path (path, path_dirname (apath)), path_basename (apath));
}

Path*
path_dirname (Path *path)
{
  g_return_val_if_fail (path, NULL);

  if (path != path_root ())
    return path->container;

  return path_root ();
}

const char*
path_basename (Path *path)
{
  g_return_val_if_fail (path, NULL);

  return path->name;
}

const char*
path_to_string (Repository *repo,
		Path       *path)
{
  g_return_val_if_fail (repo, NULL);

  return (* repo->driver->simp_driver.path_to_string) (repo, path);
}

const char*
pts (Path* p)
{
  if (! p)
    return "(null)";

  return path_to_string_simple (p, '/');
}

Path*
path_root (void)
{
  static Path* root;

  if (root == NULL)
    {
      root = path_alloc (NULL, "<root>");
      root->container = root;
    }

  return root;
}

static void
path_to_string_internal_rec (Path* path, GString* str, char sep)
{
  if (path != path_root())
    {
      path_to_string_internal_rec (path_dirname(path), str, sep);
      g_string_sprintfa (str, "%c%s", sep, path_basename (path));
    }
}

const char*
path_to_string_simple (Path* path, char sep)
{
  GString* str;

  g_assert (path != NULL);

  str = temporary_string ("");

  if (path == path_root())
    g_string_append_c (str, sep);
  else
    path_to_string_internal_rec (path, str, sep);

  return str->str;
}

Path*
path_canonicalize_internal (Repository* repo, Path* relative, char sep, gboolean simple, const char* url0)
{
  const char* url = url0;
  gint c;
  char buf[256];
  gint buf_i;

  buf[0] = 0;
  buf_i = 0;

  if (url[0] == sep)
    relative = path_root ();

  while ((c = *url) != 0)
    {
      if (c == sep)
	{
	  url += 1;

	  if (buf_i == 0)
	    {
	    }
	  else if (strcmp (buf, ".") == 0)
	    {
	      if (simple)
		goto notsimple;
	    }
	  else if (strcmp (buf, "..") == 0)
	    {
	      if (simple)
		goto notsimple;

	      relative = path_dirname (relative);
	    }
	  else
	    relative = path_append (relative, buf);

	  buf[0] = 0;
	    buf_i = 0;
	}
      else
	{
	  if (buf_i == 255)
	    {
	      repo_generate_string_event (EC_RepoPathComponentTooLong, url0);
	      return NULL;
	    }

	  buf[buf_i++] = *url++;
	  buf[buf_i] = 0;
	}
    }

  if (buf_i > 0)
    {
      if (strcmp (buf, ".") == 0)
	{
	  if (simple)
	    goto notsimple;
	}
      else if (strcmp (buf, "..") == 0)
	{
	  if (simple)
	    goto notsimple;

	relative = path_dirname (relative);
	}
      else
	relative = path_append (relative, buf);
    }
  else if (simple && relative != path_root ())
    {
      repo_generate_string_event (EC_RepoNoTrailingSeparator, url0);
      return NULL;
    }

  return relative;

 notsimple:

  repo_generate_string_event (EC_RepoNotASimplePath, url0);
  return NULL;
}

Path*
path_append_format   (Path       *path,
		      const char *fmt,
		      ...)
{
  va_list args;
  gchar* copy;
  Path* path2;

  va_start (args, fmt);

  copy = g_strdup_vprintf (fmt, args);

  path2 = path_append (path, copy);

  g_free (copy);

  va_end (args);

  return path2;
}

gboolean
path_contains_equiv (Path       *one,
		     Path       *two)
{
  while (two != path_root ())
    {
      if (one == two)
	return TRUE;

      two = path_dirname (two);
    }

  return FALSE;
}

gboolean
path_contains (Path *one,
	       Path *two)
{
  do
    {
      two = path_dirname (two);

      if (one == two)
	return TRUE;
    }
  while (two != path_root ());

  return FALSE;
}

Path*
path_absolute (const char* p)
{
  return path_append (path_root (), p);
}

Path*
path_absolute2 (const char* p, const char* q)
{
  return path_append2 (path_root (), p, q);
}

Path*
path_absolute3 (const char* p, const char* q, const char* r)
{
  return path_append3 (path_root (), p, q, r);
}

Path*
path_absolute4 (const char* p, const char* q, const char* r, const char* s)
{
  return path_append4 (path_root (), p, q, r, s);
}

Path*
path_append2 (Path* path, const char* s1, const char* s2)
{
  return path_append (path_append (path, s1), s2);
}

Path*
path_append3 (Path* path, const char* s1, const char* s2, const char* s3)
{
  return path_append (path_append (path_append (path, s1), s2), s3);
}

Path*
path_append4 (Path* path, const char* s1, const char* s2, const char* s3, const char* s4)
{
  return path_append (path_append (path_append (path_append (path, s1), s2), s3), s4);
}

Path*
path_suffix (Path  *path,   /* path_suffix ("/D1/D2/D3", 2) -> "/D3" */
	     guint  levels)
{
  if (levels == 0)
    return path_root ();

  return path_append (path_suffix (path_dirname (path), levels - 1), path_basename (path));
}

guint
path_length (Path *path)
{
  if (path_root () == path)
    return 0;

  return 1 + path_length (path_dirname (path));
}
