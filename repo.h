/* -*-Mode: C;-*-
 * $Id: repo.h 1.91 Mon, 03 May 1999 19:48:53 -0700 jmacd $
 *
 * Copyright (C) 1997, 1998, 1999, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#ifndef _REPO_H_
#define _REPO_H_

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <stdlib.h>
#include <glib.h>
#include <edsio.h>

/**********************************************************************/
/*			        Types                                 */
/**********************************************************************/

typedef struct _Repository              Repository;
typedef struct _Path                    Path;
typedef struct _File                    File;
typedef struct _FileSegment             FileSegment;
typedef struct _DirectoryHandle         DirectoryHandle;
typedef struct _SegmentView             SegmentView;
typedef struct _SerialAliasedIdentifier AliasedIdentifier;
typedef struct _DriverFile              DriverFile;

#include "repo_edsio.h"

enum _RepoFileTypePropertyValue {
  FV_NotPresent   = 1<<1,

  /* Non-containers
   */
  FV_Regular      = 1<<2,
  FV_SymbolicLink = 1<<3,

  /* Containers
   */
  FV_Directory    = 1<<4,
  FV_Index        = 1<<5,
  FV_Sequence     = 1<<6,
  FV_Log          = 1<<7,

  FV_Invalid      = 1<<10
};

#define FV_Irregular (FV_Directory | FV_Index | FV_SymbolicLink | FV_Sequence | FV_Log)

#define FV_IrregularCase FV_SymbolicLink: case FV_Directory: case FV_Index: case FV_Sequence: case FV_Log
#define FV_IrregularComplementCase FV_NotPresent: case FV_Regular: case FV_Invalid

enum _RepoSegmentTypePropertyValue {
  SV_NotPresent   = 1<<16,
  SV_Regular      = 1<<17,
  SV_View         = 1<<18,
  SV_Invalid      = 1<<19
};

#define SV_Readable (SV_Regular | SV_View)

enum _FileAccessType {
  AT_Read      = 1 << 0, /* can read */
  AT_Replace   = 1 << 1, /* can create, may or may not exist */
  AT_Modify    = 1 << 2, /* can create in directory */
  AT_Unlink    = 1 << 3, /* can remove, does exist */
  AT_Create    = 1 << 4  /* can create, does not exist */
};

enum _TriggerEventCode {
  /* Trigger constructor and destructor events
   */
  TE_IndexCreate     = (1 <<  0),
  TE_IndexDelete     = (1 <<  1),
  TE_SegmentCreate   = (1 <<  2),
  TE_SegmentDelete   = (1 <<  3),
  TE_DirectoryCreate = (1 <<  4),
  TE_DirectoryDelete = (1 <<  5),
  TE_SymlinkCreate   = (1 <<  6),
  TE_SymlinkDelete   = (1 <<  7),
  TE_SequenceCreate  = (1 <<  8),
  TE_SequenceDelete  = (1 <<  9),
  TE_LogCreate       = (1 << 10),
  TE_LogDelete       = (1 << 11),

  /* Trigger constructor and destructor events
   */
  TE_ContainerDelete = (1 << 16), /* The trigger's own container is disappearing. */
  TE_TriggerDelete   = (1 << 17), /* The trigger is being deleted w/ a call to trigger_delete. */
  TE_Create          = (1 << 18), /* The trigger is being created. */

  /* Trigger handle creation events
   */
  TE_OpenReplace     = (1 << 19), /* A new file handle has been created to replace a segment. */
  TE_OpenRead        = (1 << 20)  /* A new file handle has been created to read a segment. */
};

enum _TriggerFlag {
  TF_Recursive          = (1 << 0),
  TF_ArgumentPrefixMask = (1 << 1)
};

#define TE_TriggerAll ((gint32)(-1))
#define TE_TriggerCreateDelete (TE_IndexCreate | TE_IndexDelete | TE_SegmentCreate | TE_SegmentDelete | TE_DirectoryCreate | TE_DirectoryDelete | TE_SymlinkCreate | TE_SymlinkDelete | TE_SequenceCreate | TE_SequenceDelete)

enum _RepositoryListFlags {

  /* Segment related flags */
  RLF_HideSystemSegments = (1 << 0), /* Hide the "Auto", "Triggers", and "Ilinks" segments */
  RLF_HideAutoSegments   = (1 << 1), /* Hide any segment that was automatically produced by the
				      * system.  For example: "MD5". */
  RLF_HideSegments       = (1 << 2), /* Hide all non-default segments. */

  /* Recursion related flags */
  RLF_NotRecursive       = (1 << 3), /* ls -d */
  RLF_Recursive          = (1 << 4), /* ls -R */

  /* File type specific - Regular */
  RLF_ShowSegmentHead    = (1 << 5), /* shows first 8 bytes */

  /* File type specific - Regular containing EDSIO type */
  RLF_TestSerial         = (1 << 6), /* checks for EDSIO type and if present, list */
  RLF_ShowSerial         = (1 << 7), /* checks for EDSIO type and if present, print */

  /* File type specific - Index */
  RLF_ListIndices        = (1 << 8)  /* lists the index (key -> path) mapping */
};

enum _ContainerType {
  CT_HIndex    = (1 << 1),
  CT_BIndex    = (1 << 2),
  CT_Directory = (1 << 3),
  CT_Sequence  = (1 << 4),
  CT_Log       = (1 << 5),
  CT_Joined    = (1 << 6),
  CT_Invalid   = (1 << 7)
};

typedef enum _RepoFileTypePropertyValue    RepoFileTypePropertyValue;
typedef enum _RepoSegmentTypePropertyValue RepoSegmentTypePropertyValue;
typedef enum _FileAccessType               FileAccessType;
typedef enum _TriggerEventCode             TriggerEventCode;
typedef enum _TriggerFlag                  TriggerFlag;
typedef enum _RepositoryListFlags          RepositoryListFlags;
typedef enum _ContainerType                ContainerType;

/**********************************************************************/
/*			Repository Operations                         */
/**********************************************************************/

/*
 * Initializes the system.
 */
gboolean repository_system_init (void);

/*
 * Closes the system.
 */
gboolean repository_system_close (void);

/*
 * Initializes an existing repository located by NATIVE_LOCATION_STRING,
 * which is assumed to be some sort of resource locator.
 */
Repository* repository_initialize (const char* native_location_string);

/*
 * Creates a repository located by NATIVE_LOCATION_STRING.
 */
Repository* repository_create     (const char* native_location_string);

/*
 * Returns TRUE if a repository located by NATIVE_LOCATION_STRING
 * can be found.
 */
gboolean    repository_exists     (const char* native_location_string);

/*
 * Closes the repository.  Does not commit pending changes.
 */
gboolean    repository_close      (Repository* repo);

/**********************************************************************/
/*		       Repository Transactions                        */
/**********************************************************************/

gboolean    repository_commit     (Repository* repo);
gboolean    repository_rollback   (Repository* repo);

/**********************************************************************/
/*		           Repository State                           */
/**********************************************************************/

gboolean    repository_chdir      (Repository *repo,
				   Path       *path);

Path*       repository_getcwd     (Repository *repo);

/**********************************************************************/
/*		       Repository General Utils                       */
/**********************************************************************/

gboolean    repository_list       (Repository *repo,
				   gint        flags);

gboolean    repository_list_file  (File       *file,
				   gint        flags);

void        repository_tool       (Repository *repo);

/**********************************************************************/
/*			   Path Operations                            */
/**********************************************************************/

/* Paths are repository-independant hierarchical names.  You may
 * exchange them between repositories.  Canonicalize is repository-
 * specific in order to translate repository-specific names into
 * paths. */

Path* path_canonicalize        (Repository *repo,
				const char *native_filename);
Path* path_canonicalize_long   (Repository *repo,
				Path       *relative,
				const char *native_filename);
Path* path_canonicalize_simple (Path       *relative,
				char        sep,
				const char *native_filename);

Path* path_append          (Path       *path,
			    const char *simp_name);
Path* path_append_n        (Path       *path,
			    const char *simp_name,
			    int         simp_name_len);
Path* path_append_format   (Path       *path,
			    const char *fmt,
			    ...);
Path* path_append_path     (Path       *path,
			    Path       *apath);
Path* path_append_strings  (Path       *path,
			    const char *str1,
			    ...);
Path*       path_dirname   (Path       *path);
const char* path_basename  (Path       *path);
const char* path_to_string (Repository *repo,
			    Path       *path);

/* Strips (length - levels) off the path.
 *
 *   path_suffix ("/D1/D2/D3", 1) -> "/D3"
 */
Path* path_suffix          (Path       *path,
			    guint       levels);

Path* path_prepend_basenames (Path* path, const char* prefix);
Path* path_append_basenames  (Path* path, const char* suffix);
Path* path_map               (Path* path, const char* (*func) (const char*));

guint path_length          (Path       *path);

const char* path_to_string_simple (Path       *path,
				   char        sep);
Path* path_root            (void);

Path* path_absolute        (const char* p);
Path* path_absolute2       (const char* p, const char* q);
Path* path_absolute3       (const char* p, const char* q, const char* r);
Path* path_absolute4       (const char* p, const char* q, const char* r, const char* s);

Path* path_append2         (Path* path, const char* s1, const char* s2);
Path* path_append3         (Path* path, const char* s1, const char* s2, const char* s3);
Path* path_append4         (Path* path, const char* s1, const char* s2, const char* s3, const char* s4);

/* Returns true if one strictly contains two:
 *
 *   (/A/B/C, /A/B/C/D/E) = TRUE
 *   (/A/B/C, /A/B/C/D) = TRUE
 *   (/A/B/C, /A/B/C) = FALSE
 */
gboolean path_contains     (Path       *one,
			    Path       *two);

/* Returns true if one contains two, or is equivalent:
 *
 *   (/A/B/C, /A/B/C/D/E) = TRUE
 *   (/A/B/C, /A/B/C/D) = TRUE
 *   (/A/B/C, /A/B/C) = TRUE
 */
gboolean path_contains_equiv (Path       *one,
			      Path       *two);

/**********************************************************************/
/*			   File Operations                            */
/**********************************************************************/

/*
 * Returns a handle associated with PATH in REPO.  It does not otherwise
 * assert or require any properties or existance of that file.
 */
File* file_initialize         (Repository *repo,
			       Path       *path);

/* Initializes the file named entry_name within dir. */
File* file_initialize_entry   (File       *dir,
			       const char *entry_name);

/* Initializes file's parent. */
File* file_initialize_parent  (File       *file);

/* Initializes the file named entry_name within file's parent. */
File* file_initialize_sibling (File       *file,
			       const char *entry_name);

/* Returns a handle associated with PATH in REPO without following
 * symbolic links.
 */
File* file_initialize_link (Repository *repo,
			    Path       *path);

/* Returns a temporary (plain) file.  The resources used by this file
 * will be released when all references are released.  The repository
 * is implicitely the temp_repository.  [@@@ The temp stuff needs a
 * serious overhaul...]
 */
File* file_initialize_temp (Repository *repo);

/* These functions perform tests of a files type, and whether a file
 * is temporary.  Function names containing _not are the logical
 * negation of those without the suffix. Function names with the
 * _noerr suffix do not deliver errors, those without the suffix
 * deliver an error if their result non-zero.
 *
 * file_is_type (FILE, TYPE) returns the bitwise-or of the file's
 *     actual type and TYPE
 *
 * file_is_temp (FILE) returns TRUE if the file is temporary
 *
 * It is important to note that this error convention is contrary to
 * my normal calling convention, because I found myself using them
 * most often to generate errors by inclusion or exclusion, so I
 * normally write:
 *
 *   if (file_is_not_type (...)) SILENT FAILURE CONDITION;
 *   if (file_is_type (...)) SILENT FAILURE CONDITION;
 *
 * whereas most of my functions generate errors on a FALSE return value.
 */

#define file_is_type(f,t) file_is_type_int(f,t,__FILE__,__LINE__)
#define file_is_not_type(f,t) file_is_not_type_int(f,t,__FILE__,__LINE__)

gboolean file_is_type_int          (File* file, gint type, const char* errfile, int errline);
gboolean file_is_not_type_int      (File* file, gint type, const char* errfile, int errline);
gboolean file_is_type_noerr        (File* file, gint type);
gboolean file_is_not_type_noerr    (File* file, gint type);

gboolean file_is_temp              (File* file);
gboolean file_is_not_temp          (File* file);
gboolean file_is_temp_noerr        (File* file);
gboolean file_is_temp_not_noerr    (File* file);

#define file_may_not_access(f,t) file_may_not_access_int(f,t,__FILE__,__LINE__)

gboolean file_may_not_access_int   (File* file, gint type, const char* errfile, int errline);
gboolean file_may_not_access_noerr (File* file, gint type);

/* This attempts to create a symlink.  It creates a link at
 * file_get_path (file, FP_Path) pointing to NPATH.  Requires that
 * path_dirname (file_get_path (file, FP_Path)) be writable and
 * that no file exists at file_get_path (file, FP_Path).  The
 * exact encoded path used for npath is de-canonicalized by
 * computing the least relative path between the two paths.
 * if this is not good enough... */
gboolean file_symlink (File *file, const char* link);

/* This behaves link unlink() and/or rmdir() in UNIX.  It does not
 * follow links.  It may operate recursively on directories.  It
 * requires that each file being deleted is contained in a writable
 * directory.  This erases ALL segments, see segment_erase for erasing
 * individual segments. */
gboolean file_erase (File *file, gboolean recurse);

/* Tests whether the file content was not modified since stamp was
 * taken (the FP_ContentStamp property). */
gboolean      file_unmodified_since   (File* file, const guint8* data, guint len);

/* Restores the properties previously retrieved with the FP_Properties
 * structure. */
gboolean      file_properties_restore (File* file, const guint8* data, guint len);

RepoFileTypePropertyValue   file_type              (File* file);
gssize                      file_length            (File* file); /* -1 on failure, w/ an error */

Path*                       file_access_path (File* file);
Path*                       file_dest_path   (File* file);
const char*                 file_link        (File* file);
gboolean                    file_properties  (File* file,  /* FALSE on failure, w/ an error */
					      guint8** arg,
					      guint32* arg_len);
gboolean                    file_copy        (File *from, File* to);

/**********************************************************************/
/*		       File/Segment Operations                        */
/**********************************************************************/

/* Opens this file to read or replace.  This opens the default
 * segment.
 */
FileHandle* file_open (File *file, gint flags);

/* The following operations are the same for file segments as the
 * corresponding operations above.  Each above operation uses the
 * repository's default file segment which has the root path as its
 * name.
 */
FileSegment*  file_default_segment  (File* file);
FileSegment*  file_segment          (File* file, Path* name);

/* To iterate over all of a file's possibly existing segments
 */
FileSegment*  file_segment_first    (File* file);
FileSegment*  file_segment_next     (FileSegment *seg);

gboolean      segment_is_default    (FileSegment* seg);
Path*         segment_name          (FileSegment* seg);

gboolean      segment_erase         (FileSegment *file, gboolean recurse);
gboolean      segment_copy          (FileSegment *from, FileSegment* to);
FileHandle*   segment_open          (FileSegment *file, gint flags);
File*         segment_file          (FileSegment* file);

RepoSegmentTypePropertyValue   segment_type          (FileSegment* file);
gssize                      segment_length        (FileSegment* file);

SerialSink*   segment_sink          (FileSegment *seg, gpointer data1, gpointer data2, gpointer data3, gboolean (* cont_onclose) (gpointer data1, gpointer data2, gpointer data3));
SerialSource* segment_source        (FileSegment *seg);

#define segment_is_type(s,t) segment_is_type_int(s,t,__FILE__,__LINE__)
#define segment_is_not_type(s,t) segment_is_not_type_int(s,t,__FILE__,__LINE__)

gboolean segment_is_type_int       (FileSegment* seg, gint type, const char* errfile, int errline);
gboolean segment_is_not_type_int   (FileSegment* seg, gint type, const char* errfile, int errline);
gboolean segment_is_type_noerr     (FileSegment* seg, gint type);
gboolean segment_is_not_type_noerr (FileSegment* seg, gint type);

gboolean segment_is_temp           (FileSegment* seg);
gboolean segment_is_not_temp       (FileSegment* seg);
gboolean segment_is_temp_noerr     (FileSegment* seg);
gboolean segment_is_temp_not_noerr (FileSegment* seg);

#define segment_may_not_access(s,t) segment_may_not_access_int(s,t,__FILE__,__LINE__)

gboolean segment_may_not_access_int   (FileSegment* seg, gint type, const char* errfile, int errline);
gboolean segment_may_not_access_noerr (FileSegment* seg, gint type);


/**********************************************************************/
/*			Directory Constructors                        */
/**********************************************************************/

/* This attempts to create a directory at file_get_path (file,
 * FP_Path).  Requires that path_dirname (file_get_path (file,
 * FP_Path)) be writable and that no file exists at file_get_path
 * (file, FP_Path).
 */
gboolean                    file_mkdir   (File       *file);

/* This attempts to create all subdirectories in repo of path.
 */
File*                       file_mkdir_p (Repository *repo,
					  Path       *path);

/**********************************************************************/
/*		Container/Directory Handle Operations                 */
/**********************************************************************/

/* The underlying implementation of the generic container is used
 * to implement a variety of containers and queries.
 *
 *   Directory -- This contains entries that are named by a nul-terminated
 *                string.
 *   Index     -- Either the underlying Hash or B+Tree using soft links,
 *                can be expanded with more index types (Glimpse?).
 *   Sequence  -- A collection of entries in order of creation.
 *   Log       -- Similar to the sequence, but different...
 */

/* A handle iterating over all entries in the container in their
 * natural order.
 */
DirectoryHandle*            file_cont_open        (File        *cont);

/* A handle iterating all entries in the directory with basenames
 * prefixed by PREFIX.
 */
DirectoryHandle*            file_cont_prefix     (File         *cont,
						  const char   *prefix);

DirectoryHandle*            file_cont_prefix_len (File         *cont,
						  const guint8 *prefix,
						  guint         len);

/* Note similarity, but difference, from file_initialize_entry.  This
 * accepts only duplicates with an exact match.  To avoid duplicates,
 * use file_initialize_entry.
 */
DirectoryHandle*            file_cont_lookup     (File           *index,
						  const char     *key);
DirectoryHandle*            file_cont_lookup_len (File           *index,
						  const guint8   *key,
						  guint           key_len);

/* Joins 2 or more handles returned by the file_index_iterate or
 * file_index_query functions.  The input handles must be of type
 * CT_HIndex or CT_BIndex.
 *
 * (I think) the queried handles must have been created with the
 * IQF_Duplicates flag (default)--that they may not be range queries.
 * But that's just a guess, the DB2 docs are unhelpful in this area.
 *
 * [@@@ Note: I haven't actually coded this function, but the
 * databases are properly laid out to support it so it _should_ be
 * trivial.  I am unhappy with the documentation so I'm waiting for
 * now.  I will do it as soon as I have a real need.]
 *
 * That leaves one missing function.  The join function below is much
 * like the file_opendir () iteration handle above.  The missing
 * function is the equivalent of file_opendir_prefix() for the joined
 * handle.
 */
DirectoryHandle*            file_cont_join      (File             *index,
						 DirectoryHandle **dh_array,
						 guint             dh_array_len);

/* Performance/implementation note: I've made the above interfaces
 * quite simple, but there are penalties on the performance of
 * multiple repeated queries.  It is only really important on joined
 * handles.  I will expose a way to reposition cursors, later.
 */

/* Returns the type of index
 */
ContainerType               file_cont_type     (File         *index);

/* To obtain a type name
 */
const char*                 file_cont_type_to_string (ContainerType type);

Path*                       dh_next      (DirectoryHandle* dh);
gboolean                    dh_close     (DirectoryHandle* dh);
ContainerType               dh_type      (DirectoryHandle* dh);

/**********************************************************************/
/*		                Indexing                              */
/**********************************************************************/

/* @@@ Think about how to put multiple DBs in one file, since they can
 * be small. */

/* This function creates a hashed secondary index out of FILE that
 * indexes segments named SEG_NAME.  When created it indexes the empty
 * set of files.
 */
gboolean         file_index_create   (File          *file,
				      Path          *seg_name,
				      ContainerType  ctype);

/* The link command adds files to an index.  DIR must be a directory,
 * and it adds all of the regular files in the directory (symlinks
 * are not followed) that have a segment present with the segment name
 * being indexed.  If recurse is true, then all subdirectories are
 * automatically indexed (including those added later).
 */
gboolean         file_index_link     (File         *index,
				      File         *dir,
				      gboolean      recurse);


/* Undoes the above linkage, possibly recursively.
 */
gboolean         file_index_unlink   (File         *index,
				      File         *dir,
				      gboolean      recurse);

/* Returns the seg_name of index
 */
Path*            file_index_seg_name (File         *index);

/**********************************************************************/
/*		            Sequences/Logs                            */
/**********************************************************************/

/* A sequence is a special file type that can be used for the creation
 * of numbered sequences of files.  The sequence named S in the
 * directory D will create files named:
 *
 *   /D/S/0
 *   /D/S/1
 *   ...
 *   /D/S/N
 *
 * Any of the file or segment creation operations will obtain a new
 * index before completing a transaction.
 * These include: close() after open(HV_replace),
 * (file|segment)_set_TYPE, symlink, index_create, sequence_create,
 * view_create, mkdir, etc ...
 */
gboolean      file_sequence_create             (File  *seq);

/* Return the value of the next sequence number to be assigned.  The
 * initial value is 0.  There is no failure value.
 */
guint32       file_sequence_value              (File  *seq);

/* Returns an element of the sequence, indexed by i.
 */
File*         file_sequence_initialize         (File    *seq,
						guint32  i);

/* Returns the next element, not present.  It consumes a sequence entry.
 */
File*         file_sequence_initialize_next    (File  *seq);

/* Logs are no different yet, but I plan to implement some sort of
 * checkpointing to flush them, and they print differently (does not
 * show contents).  */

/**********************************************************************/
/*		                Views                                 */
/**********************************************************************/

/* A view is a virtual read-only segment that is constructed on the
 * fly by application code. */

typedef gboolean SegmentViewPageIn  (FileSegment *seg,
				     SegmentView *view,
				     guint8      *pgbuf,
				     gssize       page_no,
				     gsize        in_len);

/* This function registers the page_in function for a named view */
SegmentView* segment_view_register (const char        *name,
				    SegmentViewPageIn  page_in);

/* Looks for a view with pre-registered name. */
SegmentView* segment_view_lookup   (const char        *name);

/* Creates a view out of SEG, of length LEN, constructed by view VIEW.
 * Yes, you must know the length before hand, and it MUST BE
 * DETERMINISTIC (if you expect automatic indexing to work). */
gboolean     segment_view_create   (FileSegment       *seg,
				    guint              len,
				    SegmentView       *view);

/* If SEG is a view, returns the view. */
SegmentView* segment_view          (FileSegment       *seg);

/* If SEG is a view, returns the view name. */
const char*  segment_view_name     (FileSegment       *seg);

/**********************************************************************/
/*			  Directory Triggers                          */
/**********************************************************************/

/* Triggers allow actions to be taken each time a directory is
 * modified.  These are used to internally to implement index updates
 * and do recursive index/trigger propagation.  They are also a part
 * of the automatic property mechanism below.  Generally speaking, the
 * trigger mechanism can be used to orchestrate general forms of
 * automatic indexing.  I haven't given a formal semantics to the
 * invokation order or dealt with certain recursion problems that may
 * arise if you do weird things.  */

typedef gboolean (DirectoryTrigger) (gint32       code,
				     const char  *code_name,
				     File        *target_dir,
				     File        *target_file,
				     Path        *target_seg_name,
				     FileHandle  *target_handle,
				     const char  *trigger_argument,
				     gint32       trigger_flags,
				     gint32       trigger_id,
				     gint32       trigger_parent_id,
				     gint32       trigger_event_type);

/* This registers a user trigger handler.  CODE must be an odd
 * integer.  Even codes are reserved for internal use.  CODE_NAME is
 * used for external identification purposes only. */

gboolean     trigger_type_register (gint32           code,
				    const char       *code_name,
				    DirectoryTrigger *trigger_func);

/* This adds a trigger to a directory DIR.  Code identifies the
 * trigger function, which is registered above.  There are two mask
 * arguments.  The segment name mask applies only the the
 * TE_SegmentCreate and TE_SegmentDelete trigger events, and may be
 * NULL to receive events for all segments.  Argument is a string
 * which is passed to the trigger function when it is invoked.  If
 * recursive_propagate is TRUE, then the trigger is duplicated in
 * child directories of dir, both at the time of creation, and for
 * directories created at a later time.  The return value is the
 * trigger id, or -1 on failure, and may be used to delete it with.
 */

gint32       trigger_add           (File             *dir,
				    gint32            code,
				    Path             *seg_name_mask,
				    gint32            trigger_event_mask,
				    gint32            trigger_flags,
				    const char       *argument);

gboolean     trigger_delete        (Repository       *repo,
				    File             *dir,
				    gint32            trigger_id);

/**********************************************************************/
/*		      Automatic Digest Property                       */
/**********************************************************************/

/* There is currently only one type of automatic property.  An automatic
 * property is a segment that is computed from another segment.
 * An automatic property computed on the default segment (/) will be
 * named in the segment named /auto for automatic deletion.  A message
 * digest property is stored in the name of the digest.  For example,
 * /MD5 is the MD5 checksum of the default segment.  This does not
 * cause the digest computation of the directory's existing contents.
 */

gboolean       auto_digest_property  (File                *dir,
				      Path                *seg_name,
				      const MessageDigest *md,
				      gboolean             recurse);

/* These two functions compute (or retrieve pre-computed values of)
 * digests on file segments.  The resulting value is saved in an auto
 * segment.
 */

const guint8*  file_digest           (File                *file,
				      const MessageDigest *md);

const guint8*  segment_digest        (FileSegment         *seg,
				      const MessageDigest *md);

/* Returns whether the segment in question is automatic.  If so, it
 * indicates that the segment will automatically be deleted when its
 * parent is deleted.
 */

gboolean       segment_is_auto       (FileSegment         *seg);

/* Digests may be asserted.  This is, for the most part, to facilitate
 * MD5 checksums on views.  Currently, the asserted digest value is
 * validated the first time the segment is read, but I may eventually
 * allow this to be turned off.
 */

gboolean       file_digest_assert    (File                *file,
				      const MessageDigest *md,
				      const guint8        *val);

gboolean       segment_digest_assert (FileSegment         *seg,
				      const MessageDigest *md,
				      const guint8        *val);

/**********************************************************************/
/*		    (Future) Automatic Enveloping?                    */
/**********************************************************************/

/**********************************************************************/
/*				 Misc                                 */
/**********************************************************************/

Repository* repository_of_filesegment (FileSegment*);
Repository* repository_of_file (File*);

extern Repository* _fs_repo;

Path* repository_system_user_home (void);

const char* eventdelivery_file_to_string    (File* x);
const char* eventdelivery_segment_to_string (FileSegment* x);
const char* eventdelivery_path_to_string    (Path* x);

SerialSource* repo_persist_dir_source (File *file, const char* prop_name);
SerialSink*   repo_persist_dir_sink   (File *file, const char* prop_name);
gboolean      repo_persist_dir_test  (File *file, const char* prop_name);
gboolean      repo_persist_dir_unset  (File *file, const char* prop_name);

SerialSource* repo_persist_file_source (File *file, const char* prop_name);
SerialSink*   repo_persist_file_sink   (File *file, const char* prop_name);
gboolean      repo_persist_file_test  (File *file, const char* prop_name);
gboolean      repo_persist_file_unset  (File *file, const char* prop_name);

SerialSource* repo_persist_segment_source (FileSegment *seg, const char* prop_name);
SerialSink*   repo_persist_segment_sink   (FileSegment *seg, const char* prop_name);
gboolean      repo_persist_segment_test  (FileSegment *seg, const char* prop_name);
gboolean      repo_persist_segment_unset  (FileSegment *seg, const char* prop_name);

GHashTable**  edsio_file_property_table (File *file);
GHashTable**  edsio_dir_property_table (File *dir);
GHashTable**  edsio_segment_property_table (FileSegment *seg);
GHashTable**  edsio_repo_property_table (Repository *repo);

gboolean handle_to_stdout (FileHandle* fh);
gboolean file_to_stdout (File* file);

#define TEMP_PAGE_SIZE         (1<<13)
#define FS_PAGE_SIZE           (1<<13)
#define SEGMENT_VIEW_PAGE_SIZE (1<<15)
#define DB_FIRST_PAGE_SIZE     (1<<10) /* Files <= this length are stored in the "shorts" hash db. */
#define INDEX_KEY_MAX_SIZE     (1<<10) /* This must be less than the above, for sanity and a fast impl. */
#define MEM_PAGE_SIZE          DB_FIRST_PAGE_SIZE /* This is equal to the DB_FIRST_PAGE_SIZE because
						   * it is used to implement the DB read handle on
						   * short files. */

#ifdef __cplusplus
}
#endif

#endif /* _REPO_H_ */
