/* -*-Mode: C;-*-
 * $Id: sequence.c 1.5 Wed, 28 Apr 1999 07:10:32 -0700 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "repo.h"
#include "repopriv.h"

#if 0
gboolean
sequence_library_init ()
{
  return TRUE;
}

gboolean
file_sequence_create (File  *file_stat)
{
  File* file;
  FileSegment *segs;

  if (file_is_temp (file_stat))
    return FALSE;

  file = driver_lstat (file_stat);

  if (file_is_type_noerr (file, FV_Sequence))
    {
      if (! (file = file_sequence_next_internal (file->driver_file)))
	return FALSE;
    }

  if (file_may_not_access (file, AT_Create))
    return FALSE;

  g_assert (file->repo->driver->simp_driver.segments_read);

  if (! file->driver_file->segs_read && ! file->repo->driver->simp_driver.segments_read (file->driver_file))
    return FALSE;

  for (segs = file->driver_file->segment_list;
       segs;
       segs = segs->next)
    {
      if (segment_is_not_type (segs, SV_NotPresent))
	return FALSE;
    }

  g_assert (file->repo->driver->simp_driver.make_sequence);

  if (! (* file->repo->driver->simp_driver.make_sequence) (file->driver_file))
    return FALSE;

  if (! driver_set_file_state (file->driver_file, FV_Sequence))
    return FALSE;

  return TRUE;
}

gint
file_sequence_value (File  *seq)
{
  if (file_is_not_type (seq, FV_Sequence))
    return -1;

  g_assert (seq->driver_file->driver_seqvalue);

  return seq->driver_file->driver_seqvalue (seq->driver_file);
}

File*
file_sequence_initialize_next (File  *seq)
{
  if (file_is_not_type (seq, FV_Sequence))
    return NULL;

  return file_sequence_next_internal (seq->driver_file);
}

File*
file_sequence_initialize_element (File  *seq,
				  guint  i)
{
  Path* npath;
  gint value;

  if (file_is_not_type (seq, FV_Sequence))
    return NULL;

  if ((value = seq->driver_file->driver_seqvalue (seq->driver_file)) < 0)
    return NULL;

  if (i >= value)
    {
      repo_generate_fileintint_event (EC_RepoSequenceIndexOutOfRange, seq, i, value);
      return NULL;
    }

  npath = path_append_format (path_dirname (seq->driver_file->driver_path), "%s:%d", path_basename (seq->driver_file->driver_path), i);

  return file_initialize (seq->repo, npath);
}

File*
file_sequence_next_internal (DriverFile* seq)
{
  gint next;
  File *nfile;

  g_assert (seq->type == FV_Sequence);

  g_assert (seq->driver_seqnext);

  do
    {
      Path* npath;

      if ((next = seq->driver_seqnext (seq)) < 0)
	return NULL;

      npath = path_append_format (path_dirname (seq->driver_path), "%s:%d", path_basename (seq->driver_path), next);

      nfile = file_initialize (seq->repo, npath);
    }
  while (file_is_not_type_noerr (nfile, FV_NotPresent));

  return nfile;
}

#if 0
File*
file_sequence_next_internal (FileSequence* seq)
{
  FileSequences* seqs;
  File* nfile;
  File* file = file_initialize (seq->dfile->repo, seq->dfile->driver_path);

  if (! file_get_sequences (file, FP_Sequences, & seqs))
    return NULL;

  do
    {
      Path* npath = path_append_format (seq->dfile->driver_path, "%s:%d", seq->name, seq->point++);

      nfile = file_initialize (seq->dfile->repo, npath);
    }
  while (file_is_not_type_noerr (nfile, FV_NotPresent));

  if (! file_set_sequences (file, FP_Sequences, seqs))
    return NULL;

  return nfile;
}
#endif
#endif
