/* -*-Mode: C;-*-
 * $Id: tool.c 1.41 Mon, 03 May 1999 19:48:53 -0700 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "repo.h"
#include <stdio.h>

static const char*
printable_path (Path* path)
{
  if (path == path_root ())
    return "/";

  return path_basename (path);
}

static void
prompt (Repository* repo)
{
  fprintf (stdout, "(tool) %s $ ", path_to_string (repo, repository_getcwd (repo)));
  fflush (stdout);
}

static void
noise (guint8* buf, guint len)
{
  gint i;

  for (i = 0; i < len; i += 1)
    {
      buf[i] = i & 0xff;
    }
}

static void
create_command (Repository* repo)
{
  const char* arg1;
  const char* arg2;
  Path *path1;
  File *file;
  FileHandle *handle;
  guint8 buf[100];
  guint len;

  arg1 = strtok (NULL, " \t\n");
  arg2 = strtok (NULL, " \t\n");

  if (! arg1 || ! arg2)
    {
      fprintf (stderr, "usage: create file length\n");
      return;
    }

  if (! (path1 = path_canonicalize (repo, arg1)) ||
      ! (file  = file_initialize (repo, path1)))
    return;

  len = atoi (arg2);

  if (! (handle = file_open (file, HV_NoSeek | HV_Replace)))
    return;

  while (len > 0)
    {
      noise (buf, MIN (len, 100));

      if (! handle_write (handle, buf, MIN (len, 100)))
	return;

      len -= MIN (len, 100);
    }

  if (! handle_close (handle))
    return;

  return;
}

static void
cmp_command (Repository* repo)
{
  gint pos = 0;

  const char* from = strtok (NULL, " \t\n");
  const char* to = strtok (NULL, " \t\n");

  Path* topath;
  Path* frompath;

  File* tofile;
  File* fromfile;

  FileHandle* toh;
  FileHandle* fromh;

  guint8 buf1[1024], buf2[1024];

  if (!to || !from)
    {
      fprintf (stderr, "usage: cp TO FROM @@@ THIS SHOULD NOT BE BACKWARDS\n");
      return;
    }

  topath = path_canonicalize (repo, to);
  frompath = path_canonicalize (repo, from);

  if (!topath || !frompath)
    return;

  tofile = file_initialize (repo, topath);
  fromfile = file_initialize (repo, frompath);

  if (!tofile || !fromfile)
    return;

  toh = file_open (tofile, HV_Read | HV_NoSeek);
  fromh = file_open (fromfile, HV_Read | HV_NoSeek);

  if (!toh || !fromh)
    return;

  for (;;)
    {
      gint readf = handle_read (fromh, buf1, 1024);
      gint readt = handle_read (toh, buf2, 1024);
      gint i, m = MIN(readf, readt);

      if (readf < 0 || readt < 0)
	return;

      for (i = 0; i < m; i += 1, pos += 1)
	{
	  if (buf1[i] != buf2[i])
	    {
	      printf ("files differ at position %d: %x vs. %x\n", pos, buf1[i], buf2[i]);
	      return;
	    }
	}

      if (m != 1024)
	{
	  if (readt == readf)
	    {
	      printf ("files are identical\n");
	      return;
	    }

	  if (m == readf)
	    {
	      printf ("EOF in %s\n", from);
	      return;
	    }

	  if (m == readt)
	    {
	      printf ("EOF in %s\n", to);
	      return;
	    }
	}
    }
}

static void
cat_command (Repository* repo)
{
  const char* arg1;
  const char* arg2;
  Path *path1;
  File* file;
  FileHandle *handle;
  guint8 buf[1024];
  gssize nread;

  arg1 = strtok (NULL, " \t\n");
  arg2 = strtok (NULL, " \t\n");

  if (arg2 == NULL)
    {
      guint tread = 0;

      path1 = path_canonicalize (repo, arg1);
      file = file_initialize (repo, path1);

      if (! (handle = file_open (file, HV_Read | HV_NoSeek)))
	return;

      while ((nread = handle_read (handle, buf, 1024)) > 0)
	{
	  tread += nread;
	  fwrite (buf, 1, nread, stdout);
	}

      g_print ("read %d bytes\n", tread);

      if (nread < 0)
	return;

      if (! handle_close (handle))
	return;
    }
  else if (strcmp (arg1, ">") == 0)
    {
      path1 = path_canonicalize (repo, arg2);
      file = file_initialize (repo, path1);

      if (! file || ! (handle = file_open (file, HV_NoSeek | HV_Replace)))
	return;

      while ((nread = fread (buf, 1, 8, stdin)) > 0)
	{
	  if (! handle_write (handle, buf, nread))
	    return;
	}

      if (nread < 0)
	return;

      if (! handle_close (handle))
	return;
    }
  else
    {
      fprintf (stderr, "usage: cat [>] file\n");
    }
}

static void
cd_command (Repository* repo)
{
  const char* to;
  Path* path;

  to = strtok (NULL, " \t\n");

  if (!to)
    {
      fprintf (stderr, "usage: cd DIR\n");
      return;
    }

  path = path_canonicalize (repo, to);

  if (path)
    repository_chdir (repo, path);
}

static void
mkdir_command (Repository* repo)
{
  const char* to;
  Path* path;
  File* file;

  to = strtok (NULL, " \t\n");

  if (!to)
    {
      fprintf (stderr, "usage: mkdir DIR\n");
      return;
    }

  path = path_canonicalize (repo, to);

  if (!path)
    return;

  file = file_initialize (repo, path);

  if (!file)
    return;

  if (! file_mkdir (file))
    return;
}

static void
rm_command (Repository* repo)
{
  const char* arg;
  gboolean recurse = FALSE;

  while ((arg = strtok (NULL, " \t\n")) != NULL)
    {
      Path* path;
      File* file;

      if (strcmp (arg, "-r") == 0)
	{
	  recurse = TRUE;
	  continue;
	}

      path = path_canonicalize (repo, arg);

      if (path == NULL)
	continue;

      file = file_initialize_link (repo, path);

      if (file == NULL)
	continue;

      if (! file_erase (file, recurse))
	continue;
    }
}

static void
ls_command (Repository* repo)
{
  const char* arg;
  guint flags = RLF_ShowSegmentHead | RLF_TestSerial | RLF_HideSegments | RLF_HideAutoSegments | RLF_HideSystemSegments;

  arg = strtok (NULL, " \t\n");

  while (arg)
    {
      if (strcmp (arg, "-R") == 0)
	{
	  flags |= RLF_Recursive;
	}
      else if (strcmp (arg, "-d") == 0)
	{
	  flags |= RLF_NotRecursive;
	}
      else if (strcmp (arg, "-o") == 0)
	{
	  flags |= RLF_ShowSerial;
	}
      else if (strcmp (arg, "-a") == 0)
	{
	  flags &= ~RLF_HideSegments;
	}
      else if (strcmp (arg, "-s") == 0)
	{
	  flags &= ~RLF_HideSystemSegments;
	  flags &= ~RLF_HideAutoSegments;
	}
      else
	{
	  break;
	}

      arg = strtok (NULL, " \t\n");
    }

  if (arg == NULL)
    arg = ".";

  do
    {
      Path* path;
      File* file, *stat_file;

      path = path_canonicalize (repo, arg);

      if (path == NULL)
	continue;

      file = file_initialize_link (repo, path);

      stat_file = file_initialize (repo, path);

      if (file_is_type_noerr (file, FV_SymbolicLink) &&
	  file_is_type_noerr (stat_file, FV_Directory))
	file = stat_file;

      if (file == NULL)
	continue;

      if (! repository_list_file (file, flags))
	return;
    }
  while ((arg = strtok (NULL, " \t\n")) != NULL);
}

static void
copy_command (Repository* repo)
{
  const char* from = strtok (NULL, " \t\n");
  const char* to = strtok (NULL, " \t\n");

  Path* topath;
  Path* frompath;

  File* tofile;
  File* fromfile;

  if (!to || !from)
    {
      fprintf (stderr, "usage: cp TO FROM\n");
      return;
    }

  topath = path_canonicalize (repo, to);
  frompath = path_canonicalize (repo, from);

  if (!topath || !frompath)
    return;

  tofile = file_initialize (repo, topath);
  fromfile = file_initialize (repo, frompath);

  if (!tofile || !fromfile)
    return;

  segment_copy (file_default_segment (fromfile),
		file_default_segment (tofile));
}

static void
digest_command (Repository* repo, const MessageDigest *md)
{
  const char* arg;

  arg = strtok (NULL, " \t\n");

  if (arg == NULL)
    arg = ".";

  do
    {
      Path* path;
      File* file;
      const guint8* digest;
      char buf[EDSIO_DIGEST_STRING_MAX];

      path = path_canonicalize (repo, arg);

      if (path == NULL)
	continue;

      file = file_initialize (repo, path);

      if (file == NULL)
	continue;

      if (file_is_not_type (file, FV_Regular))
	continue;

      if (! (digest = file_digest (file, md)))
	continue;

      edsio_digest_to_string (md, digest, buf);

      printf ("%s (%s) = %s\n", md->name, printable_path (path), buf);
    }
  while ((arg = strtok (NULL, " \t\n")) != NULL);
}

static void
ln_command (Repository* repo)
{
  const char* to = strtok (NULL, " \t\n");
  const char* from = strtok (NULL, " \t\n");

  Path* path;

  File* file;

  if (!to || !from)
    {
      fprintf (stderr, "usage: ln TO FROM\n");
      return;
    }

  path = path_canonicalize (repo, from);

  if (!path)
    return;

  file = file_initialize_link (repo, path);

  if (!file)
    return;

  if (! file_symlink (file, to))
    return;
}

void
repository_tool (Repository *repo)
{
  char read_buf[1024];

  prompt (repo);

  while (fgets (read_buf, 1024, stdin) != NULL)
    {
      const char* cmd = strtok (read_buf, " \t\n");

      if (cmd == NULL)
	{
	  /* nothing */
	}
      else if (strcmp (cmd, "ls") == 0)
	{
	  ls_command (repo);
	}
      else if (strcmp (cmd, "cd") == 0)
	{
	  cd_command (repo);
	}
      else if (strcmp (cmd, "ln") == 0)
	{
	  ln_command (repo);
	}
      else if (strcmp (cmd, "rm") == 0)
	{
	  rm_command (repo);
	}
      else if (strcmp (cmd, "mkdir") == 0)
	{
	  mkdir_command (repo);
	}
      else if (strcmp (cmd, "cat") == 0)
	{
	  cat_command (repo);
	}
      else if (strcmp (cmd, "create") == 0)
	{
	  create_command (repo);
	}
      else if (strcmp (cmd, "cp") == 0)
	{
	  copy_command (repo);
	}
      else if (strcmp (cmd, "cmp") == 0)
	{
	  cmp_command (repo);
	}
      else if (strcmp (cmd, "sha") == 0)
	{
	  digest_command (repo, edsio_message_digest_sha ());
	}
      else if (strcmp (cmd, "md5") == 0)
	{
	  digest_command (repo, edsio_message_digest_md5 ());
	}
      else
	{
	  fprintf (stderr, "unrecognized command: %s\n", cmd);
	}

      prompt (repo);
    }
}
