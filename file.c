/* -*-Mode: C;-*-
 * $Id: file.c 1.53 Sun, 02 May 1999 04:53:40 -0700 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "repo.h"
#include "repopriv.h"

File*
file_initialize_entry (File       *dir,
		       const char *entry_name)
{
  return file_initialize (dir->repo, path_append (dir->driver_file->driver_path, entry_name));
}

File*
file_initialize_parent  (File *file)
{
  return file_initialize (file->repo, path_dirname (file->driver_file->driver_path));
}

File*
file_initialize_sibling (File       *file,
			 const char *entry_name)
{
  return file_initialize (file->repo, path_append (path_dirname (file->driver_file->driver_path), entry_name));
}

File*
file_initialize (Repository *repo,
		 Path       *path)
{
  File* file;

  g_return_val_if_fail (repo && path, NULL);

  file = (* repo->driver->file_init) (repo, path);

  g_assert (file);

  return file;
}

File*
file_initialize_link (Repository *repo,
			    Path       *path)
{
  File* file;

  g_return_val_if_fail (repo && path, NULL);

  file = (* repo->driver->link_init) (repo, path);

  g_assert (file);

  return file;
}

File*
file_initialize_temp (Repository* repo)
{
  g_return_val_if_fail (repo, NULL);

  return (* repo->driver->temp_init) (repo);
}

gboolean
file_erase (File *file, gboolean recurse)
{
  g_return_val_if_fail (file, FALSE);

  return (* file->repo->driver->erase) (file, recurse);
}

gboolean
file_mkdir (File *file)
{
  g_return_val_if_fail (file, FALSE);

  return (* file->repo->driver->mkdir) (file);
}

gboolean
file_symlink (File *file, const char* path)
{
  g_return_val_if_fail (file && path, FALSE);

  return (* file->repo->driver->symlink) (file, path);
}

FileSegment*
file_segment_first (File* file)
{
  g_assert (file->repo->driver->simp_driver.segments_read);

  if (! file->driver_file->segs_read && ! file->repo->driver->simp_driver.segments_read (file->driver_file))
    return FALSE;

  file->driver_file->segs_read = TRUE;

  file_default_segment (file);

  return file->driver_file->segment_list;
}

FileSegment*
file_segment_next     (FileSegment *seg)
{
  return seg->next;
}

SegmentView*
segment_view (FileSegment *seg)
{
  const char* name;

  if (segment_is_not_type (seg, SV_View))
    return NULL;

  if (! (name = segment_view_name (seg)))
    return NULL;

  return segment_view_lookup (name);
}

const char*
segment_view_name (FileSegment *seg)
{
  if (segment_is_not_type (seg, SV_View))
    return NULL;

  return seg->seg_file->segment_view_name (seg);
}

RepoSegmentTypePropertyValue
segment_type (FileSegment* seg)
{
  return seg->seg_type;
}

FileHandle*
file_open (File *file, gint flags)
{
  g_return_val_if_fail (file, NULL);

  return (* file->repo->driver->open_file) (file_default_segment (file), flags);
}

FileHandle*
segment_open (FileSegment *seg, gint flags)
{
  g_return_val_if_fail (seg, NULL);

  return (* seg->repo->driver->open_file) (seg, flags);
}

gboolean
file_unmodified_since (File* file, const guint8* data, guint len)
{
  g_return_val_if_fail (file && len>0 && data, FALSE);

  return (* file->repo->driver->simp_driver.unmodified_since) (file->driver_file, data, len);
}

gboolean
file_properties_restore   (File* file, const guint8* data, guint len)
{
  g_return_val_if_fail (file && len>0 && data, FALSE);

  return (* file->repo->driver->simp_driver.properties_restore) (file->driver_file, data, len);
}

RepoFileTypePropertyValue
file_type (File* file)
{
  g_return_val_if_fail (file, FALSE);

  return file->driver_file->type;
}

gssize
segment_length (FileSegment* seg)
{
  if (segment_is_not_type (seg, SV_Regular | SV_View))
    return -1;

  return seg->seg_file->segment_length (seg);
}

gssize
file_length (File* file)
{
  return segment_length (file_default_segment (file));
}

Path*
file_dest_path (File* file)
{
  return file->driver_file->driver_path;
}

Path*
file_access_path (File* file)
{
  return file->access_path;
}

gboolean
file_put_internal (File *file, guint8 *stage, gsize stage_len)
{
  /* This can be more efficient. */
  FileHandle *handle = file_open (file, HV_Replace);

  if (! handle)
    return FALSE;

  if (! handle_write (handle, stage, stage_len))
    return FALSE;

  if (! handle_close (handle))
    return FALSE;

  return TRUE;
}

gboolean
segment_erase (FileSegment *file, gboolean force_all)
{
  g_return_val_if_fail (file, FALSE);

  return file->repo->driver->erase_segment (file, force_all);
}

File*
segment_file (FileSegment* seg)
{
  return file_initialize (seg->repo, seg->seg_file->driver_path);
}

/*
 */

static void
append_ored_value (GString* str, const char* value)
{
  if (str->len > 0)
    g_string_sprintfa (str, "|%s", value);
  else
    g_string_append (str, value);
}

static const char*
filetype_to_string (int type)
{
  GString* str = temporary_string ("");

  if (type & FV_NotPresent) append_ored_value (str, "FV_NotPresent");

  if (type & FV_Regular) append_ored_value (str, "FV_Regular");

  if (type & FV_SymbolicLink) append_ored_value (str, "FV_SymbolicLink");

  if (type & FV_Directory) append_ored_value (str, "FV_Directory");

  if (type & FV_Index) append_ored_value (str, "FV_Index");

  if (type & FV_Invalid) append_ored_value (str, "FV_Invalid");

  return str->str;
}

static const char*
segtype_to_string (int type)
{
  GString* str = temporary_string (NULL);

  if (type & SV_NotPresent) append_ored_value (str, "SV_NotPresent");
  if (type & SV_Regular)    append_ored_value (str, "SV_Regular");
  if (type & SV_View)       append_ored_value (str, "SV_View");
  if (type & SV_Invalid)    append_ored_value (str, "SV_Invalid");

  return str->str;
}

/* Type/Temp predicates
 */

static gboolean
segment_is_type_int2 (FileSegment* seg, gint type2, gboolean error, gboolean not_negated, const char* errfile, int errline)
{
  gint result;
  gint type = type2;

  if (type & ~ (SV_NotPresent | SV_Regular | SV_View | SV_Invalid))
    {
      repo_generate_void_event (EC_RepoInvalidArgument);
      return FALSE;
    }

  g_assert (seg);

  if (! not_negated)
    type = ~ type;

  result = seg->seg_type & type;

  if (result && error)
    repo_generate_segmentstringstring_event_internal (EC_RepoInvalidSegmentType,
						      errfile,
						      errline,
						      seg,
						      segtype_to_string (~ type),
						      segtype_to_string (seg->seg_type));

  return result;
}

static gboolean
segment_is_temp_int (FileSegment* seg, gboolean error, gboolean not_negated)
{
  gboolean result;

  g_assert (seg);

  result = seg->seg_file->is_temp;

  if (! not_negated)
    result = ! result;

  if (result && error)
    {
      if (not_negated)
	repo_generate_segment_event (EC_RepoSegmentIsTemporary, seg);
      else
	repo_generate_segment_event (EC_RepoSegmentIsNotTemporary, seg);
    }

  return result;
}

gboolean segment_is_type_int       (FileSegment* seg, gint type, const char* errfile, int errline) { return segment_is_type_int2 (seg, type, TRUE, TRUE, errfile, errline); }
gboolean segment_is_not_type_int   (FileSegment* seg, gint type, const char* errfile, int errline) { return segment_is_type_int2 (seg, type, TRUE, FALSE, errfile, errline); }
gboolean segment_is_type_noerr     (FileSegment* seg, gint type) { return segment_is_type_int2 (seg, type, FALSE, TRUE, NULL, 0); }
gboolean segment_is_not_type_noerr (FileSegment* seg, gint type) { return segment_is_type_int2 (seg, type, FALSE, FALSE, NULL, 0); }

gboolean segment_is_temp           (FileSegment* seg) { return segment_is_temp_int (seg, TRUE, TRUE); }
gboolean segment_is_not_temp       (FileSegment* seg) { return segment_is_temp_int (seg, TRUE, FALSE); }
gboolean segment_is_temp_noerr     (FileSegment* seg) { return segment_is_temp_int (seg, FALSE, TRUE); }
gboolean segment_is_not_temp_noerr (FileSegment* seg) { return segment_is_temp_int (seg, FALSE, FALSE); }

static gboolean
file_is_type_int2 (File* file, gint type2, gboolean error, gboolean not_negated, const char* errfile, int errline)
{
  gint result;
  gint type = type2;

  if (type & ~ (FV_NotPresent | FV_Regular | FV_SymbolicLink | FV_Directory | FV_Invalid | FV_Index | FV_Sequence))
    {
      repo_generate_void_event (EC_RepoInvalidArgument);
      return FALSE;
    }

  g_assert (file);

  if (! not_negated)
    type = ~ type;

  result = file->driver_file->type & type;

  if (result && error)
    repo_generate_filestringstring_event_internal (EC_RepoInvalidFileType,
						   errfile,
						   errline,
						   file,
						   filetype_to_string (~ type),
						   filetype_to_string (file->driver_file->type));

  return result;
}

gboolean file_is_type_int       (File* file, gint type, const char* errfile, int errline) { return file_is_type_int2 (file, type, TRUE, TRUE, errfile, errline); }
gboolean file_is_not_type_int   (File* file, gint type, const char* errfile, int errline) { return file_is_type_int2 (file, type, TRUE, FALSE, errfile, errline); }
gboolean file_is_type_noerr     (File* file, gint type) { return file_is_type_int2 (file, type, FALSE, TRUE, NULL, 0); }
gboolean file_is_not_type_noerr (File* file, gint type) { return file_is_type_int2 (file, type, FALSE, FALSE, NULL, 0); }

static gboolean
file_is_temp_int (File* file, gboolean error, gboolean not_negated)
{
  gboolean result;

  g_assert (file);

  result = file->driver_file->is_temp;

  if (! not_negated)
    result = ! result;

  if (result && error)
    {
      if (not_negated)
	repo_generate_file_event (EC_RepoFileIsTemporary, file);
      else
	repo_generate_file_event (EC_RepoFileIsNotTemporary, file);
    }

  return result;
}

gboolean file_is_temp           (File* file) { return file_is_temp_int (file, TRUE, TRUE); }
gboolean file_is_not_temp       (File* file) { return file_is_temp_int (file, TRUE, FALSE); }
gboolean file_is_temp_noerr     (File* file) { return file_is_temp_int (file, FALSE, TRUE); }
gboolean file_is_not_temp_noerr (File* file) { return file_is_temp_int (file, FALSE, FALSE); }

/* Access predicates
 */

static gboolean
file_may_not_access_int2 (File* file, gint type, gboolean error, const char* errfile, int errline)
{
  if (type & AT_Read)
    {
      if (file_is_type_int2 (file, FV_Regular | FV_Directory, error, FALSE, errfile, errline))
	return TRUE;

      if (! file->repo->driver->simp_driver.file_mayread (file->driver_file))
	{
	  if (error)
	    repo_generate_file_event_internal (EC_RepoFileCannotBeRead, errfile, errline, file);

	  return TRUE;
	}
    }

  if (type & AT_Modify)
    {
      if (file_is_type_int2 (file, FV_Regular | FV_Directory, error, FALSE, errfile, errline))
	return TRUE;

      if (! file->repo->driver->simp_driver.file_maymodify (file->driver_file))
	{
	  if (error)
	    repo_generate_file_event_internal (EC_RepoFileCannotBeModified, errfile, errline, file);

	  return TRUE;
	}
    }

  if (type & (AT_Replace | AT_Unlink | AT_Create))
    {
      File* dir = file_initialize (file->repo, path_dirname (file->driver_file->driver_path));

      if (file_is_type_int2 (dir, FV_Directory, error, FALSE, errfile, errline))
	return TRUE;

      if (! dir->repo->driver->simp_driver.file_maymodify (dir->driver_file))
	{
	  if (error)
	    repo_generate_file_event_internal (EC_RepoFileCannotBeModified, errfile, errline, dir);

	  return TRUE;
	}

      if (type & AT_Unlink)
	{
	  if (file_is_type_int2 (file, FV_Regular, error, FALSE, errfile, errline))
	    return TRUE;
	}

      if (type & AT_Create)
	{
	  if (file_is_type_int2 (file, FV_NotPresent, error, FALSE, errfile, errline))
	    return TRUE;
	}
    }

  return FALSE;
}

gboolean file_may_not_access_int       (File* file, gint type, const char* errfile, int errline) { return file_may_not_access_int2 (file, type, TRUE, errfile, errline); }
gboolean file_may_not_access_noerr     (File* file, gint type) { return file_may_not_access_int2 (file, type, FALSE, NULL, 0); }

static gboolean
segment_may_not_access_int2 (FileSegment* seg, gint type, gboolean error, const char* errfile, int errline)
{
  if (type & AT_Read)
    {
      if (segment_is_type_int2 (seg, SV_Regular, error, FALSE, errfile, errline))
	return TRUE;

      if (! seg->repo->driver->simp_driver.file_mayread (seg->seg_file))
	{
	  if (error)
	    repo_generate_segment_event_internal (EC_RepoSegmentCannotBeRead, errfile, errline, seg);

	  return TRUE;
	}
    }

  if (type & AT_Modify)
    {
      if (segment_is_type_int2 (seg, SV_Regular, error, FALSE, errfile, errline))
	return TRUE;

      if (! seg->repo->driver->simp_driver.file_maymodify (seg->seg_file))
	{
	  if (error)
	    repo_generate_segment_event_internal (EC_RepoSegmentCannotBeModified, errfile, errline, seg);

	  return TRUE;
	}
    }

  if (type & (AT_Replace | AT_Unlink | AT_Create))
    {
      File* dir = file_initialize (seg->repo, path_dirname (seg->seg_file->driver_path));

      if (file_is_type_int2 (dir, FV_Directory, error, FALSE, errfile, errline))
	return TRUE;

      if (! dir->repo->driver->simp_driver.file_maymodify (dir->driver_file))
	{
	  if (error)
	    repo_generate_file_event_internal (EC_RepoFileCannotBeModified, errfile, errline, dir);

	  return TRUE;
	}

      if (type & AT_Unlink)
	{
	  if (segment_is_type_int2 (seg, SV_Regular, error, FALSE, errfile, errline))
	    return TRUE;
	}

      if (type & AT_Create)
	{
	  if (segment_is_type_int2 (seg, SV_NotPresent, error, FALSE, errfile, errline))
	    return TRUE;
	}
    }

  return FALSE;
}

gboolean
segment_may_not_access_int   (FileSegment* file, gint type, const char* errfile, int errline)
{
  return segment_may_not_access_int2 (file, type, TRUE, errfile, errline);
}

gboolean
segment_may_not_access_noerr (FileSegment* file, gint type)
{
  return segment_may_not_access_int2 (file, type, FALSE, NULL, 0);
}

/**********************************************************************/
/*			   Virtual Segments                           */
/**********************************************************************/

static GHashTable* _system_views;

SegmentView*
segment_view_register (const char       *name,
		       SegmentViewPageIn page_in)
{
  SegmentView* view;

  if (! _system_views)
    _system_views = g_hash_table_new (g_str_hash, g_str_equal);

  view = g_hash_table_lookup (_system_views, name);

  if (view)
    {
      if (view->page_in != page_in)
	{
	  repo_generate_string_event (EC_RepoDuplicateViewRegistered, name);
	  return NULL;
	}

      return view;
    }

  view = g_new0 (SegmentView, 1);

  view->name = g_strdup (name);
  view->page_in = page_in;

  g_hash_table_insert (_system_views, (void*) view->name, view);

  return view;
}

SegmentView*
segment_view_lookup (const char *name)
{
  SegmentView* view = g_hash_table_lookup (_system_views, name);

  if (! view)
    repo_generate_string_event (EC_RepoNoViewRegistered, name);

  return view;
}

Path*
segment_name (FileSegment* seg)
{
  return seg->seg_name;
}

gboolean
segment_is_default (FileSegment* seg)
{
  return seg->seg_is_default;
}

gboolean
segment_to_view (FileSegment* seg, SegmentView* view)
{
  g_return_val_if_fail (seg && view, FALSE);

  if (segment_is_not_type (seg, SV_Regular))
    return FALSE;

  g_assert (seg->repo->driver->simp_driver.segment_to_view);

  return seg->repo->driver->simp_driver.segment_to_view (seg, view->name);
}
