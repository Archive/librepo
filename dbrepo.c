/* -*-Mode: C;-*-
 * PRCS - The Project Revision Control System
 * Copyright (C) 1997, 1998  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: dbrepo.c 1.85 Mon, 03 May 1999 04:42:34 -0700 jmacd $
 */

#include "repo.h"
#include "repopriv.h"

#define MAXKEY 128

typedef struct _SerialRepoState RepoState;

DB_INFO zero_dbinfo;

typedef struct _DBRepository      DBRepository;
typedef struct _DBDriverFile      DBDriverFile;
typedef struct _DBDirectoryHandle DBDirectoryHandle;
typedef struct _DBFileSegment     DBFileSegment;

struct _DBRepository {
  Repository repo;

  RepoState* state;

  DB     *directory_dbp;   /* directory structure (btree) */
  DB     *shorts_dbp;      /* short files (hash) */
  DB     *prefix_dbp;      /* directory prefix inversion (hash) */
  DB     *admin_dbp;       /* misc stuff (hash) */

  GPtrArray *all_dbps;

  DriverFile *root_dir;
  DriverFile *root_parent_dir;

  DB_ENV *db_env;
  DB_TXN *db_txnid;

  Path   *fs_base;
};

struct _DBDriverFile {
  DriverFile dfile;

  DBDriverFile *parent;

  union {
    DBCollection   *rec_df_coll;
    DBLink         *rec_df_link;
    void           *rec_df_void;
  } rec;
};

#define df_coll rec.rec_df_coll
#define df_link rec.rec_df_link
#define df_void rec.rec_df_void

struct _DBDirectoryHandle {

  DirectoryHandle dh;

  DBC *curs;

  DriverFile *dfile;

  gboolean after;
  gboolean split_key;

  gint first;
  gint second;

  DBT key, data;

  GString* key_str;

  Path   *last_path;
};

struct _DBFileSegment {
  FileSegment seg;

  DBSegment  *rec;
};

#define DB_STORAGE_VIEW 0
#define DB_STORAGE_SHORT 1
#define DB_STORAGE_LONG 2

#define DB_MAX_SHORT_LEN 4000

/* Declarations
 */

static gboolean    db_repo_will_handle (const char* url, RepositoryDriver* driver);
static Repository* db_repo_create (const char* url, RepositoryDriver* driver);
static Repository* db_repo_initialize_internal (const char* url, RepositoryDriver* driver, int flag);
static gboolean    db_repo_exists (const char* url, RepositoryDriver* driver);
static Repository* db_repo_initialize (const char* url, RepositoryDriver* driver);
static Path*       db_repo_canonicalize (Repository* repo, const char* url);
static const char* db_repo_path_to_string (Repository* repo, Path* path);
static DriverFile* db_repo_lstat (Repository* repo, DriverFile* parent, Path* path);
static gboolean    db_repo_make_dir (DriverFile* dfile);
static gboolean    db_repo_make_symlink (DriverFile* path, const char* link);
static gboolean    db_repo_make_sequence (DriverFile* file);
static DirectoryHandle* db_repo_opendir (DriverFile* file, const char* prefix);
static DirectoryHandle* db_repo_opendir_internal (DriverFile* file, const char* prefix, gboolean segs);
static gboolean    db_repo_dh_close (DirectoryHandle* dh);
static gboolean    db_repo_dh_next  (DirectoryHandle* dh0, Path** res);
static gboolean    db_repo_dh_next_internal (DirectoryHandle* dh0, Path** res, Path **seg);
static gboolean    db_repo_dh_index_key (DirectoryHandle* dh, const guint8** key, guint* key_len);

static gboolean    db_repo_delete_dir (DriverFile* file);
static gboolean    db_repo_delete_symlink (DriverFile* file);
static gboolean    db_repo_delete_index (DriverFile* file);

static gboolean      db_repo_commit (Repository *repo);
static gboolean      db_repo_close (Repository *repo);
static DB_TXN*       db_repo_txn (Repository *repo);
static SerialSink*   db_sink_key (Repository* repo, DB* dbp, const char* key);
static SerialSource* db_source_key (Repository* repo, DB* dbp, const char* key);
static SerialType    db_unserialize_generic (Repository* repo, const char* key, void** ent);

static gboolean    db_repo_properties_restore (DriverFile* file, const guint8* data, guint len);
static gboolean    db_repo_unmodified_since   (DriverFile* file, const guint8* data, guint len);

static FileHandle* db_open_replace  (FileSegment* seg, gint flags);
static FileHandle* db_open_read     (FileSegment* seg, gint flags);

static gboolean        db_repo_make_view (FileSegment* seg, guint len, const char* name);
static gboolean        db_repo_segment_to_view (FileSegment* seg, const char* name);


static gboolean        db_repo_segments_read   (DriverFile* dfile);
static FileSegment*    db_repo_segment_stat    (DriverFile* file, Path* name);
static gboolean        db_repo_segment_unlink  (FileSegment* seg);
static guint           db_repo_segment_length    (FileSegment* seg);
static const char*     db_repo_segment_view_name (FileSegment* seg);

static const char* db_skey (Repository* repo);
static const char* db_dkey (Repository* repo);

static const char* db_key_build (DriverFile* df, Path* seg_name);

static gboolean    db_key_split   (Repository* repo,
				   DriverFile* df,
				   const char* key,
				   Path** path,
				   Path** seg_name);

static const char* db_key_iterate (Repository* repo, KeyIterator *ki);

static const char* db_repo_driver_readlink (DriverFile* df);
static Path*       db_repo_driver_segname  (DriverFile* df);
static gint        db_repo_driver_seqvalue (DriverFile* df);
static gint        db_repo_driver_seqnext  (DriverFile* df);

static gboolean db_repo_save_state (Repository *repo);
static gboolean db_repo_store_prefix (DriverFile* dfile, const char* prefix);
static gboolean db_repo_unstore_prefix (DriverFile* dfile, const char* prefix);
static gboolean db_repo_index_insert_file (DriverFile* index, DriverFile* file, const guint8* key, guint key_len);
static gboolean db_repo_index_delete_file (DriverFile* index, DriverFile* file, const guint8* key, guint key_len);

static FileHandle* db_repo_sequence_open (FileSegment* seg);

static DbFileHandleClose db_repo_close_replace;

static gboolean db_repo_save_df (DriverFile *df, gint type);
static gboolean db_repo_save_fs (FileSegment *seg);

static ContainerType db_repo_driver_indextype (DriverFile* file);

static gboolean db_repo_make_index         (DriverFile       *file,
					    Path             *seg_name,
					    ContainerType         type);

static DirectoryHandle* db_repo_index_query (DriverFile       *index,
					     const guint8     *key,
					     guint             key_len,
					     IndexQueryFlag    flags);
static DirectoryHandle* db_repo_index_iterate (DriverFile     *index,
					       IndexQueryFlag  flags);


/* Local path constructors
 */

static Path*
db_fs_base (Repository *repo)
{
  DBRepository *dbrepo = (DBRepository*) repo;

  return path_append (dbrepo->fs_base, "files");
}

static const char*
db_fs_path_to_string (Repository *repo, Path* fs_path)
{
  DBRepository *dbrepo = (DBRepository*) repo;

  return path_to_string_simple (path_suffix (fs_path, path_length (fs_path) - path_length (dbrepo->fs_base) - 1), '/');
}

static Path*
db_fs_path_from_string (Repository *repo, const char* fs_path_string)
{
  Path *add;

  if (! (add = path_canonicalize_simple (path_root (), '/', fs_path_string)))
    return NULL;

  return path_append_path (db_fs_base (repo), add);
}

/* Initialization
 */

gboolean
db_repo_will_handle (const char* url, RepositoryDriver* driver)
{
  return strncmp (url, "db:", strlen ("db:")) == 0;
}

DB_TXN*
db_repo_txn (Repository *repo)
{
  DBRepository *dbrepo = (DBRepository*) repo;

  if (! dbrepo->db_txnid && txn_begin (dbrepo->db_env->tx_info, NULL, &dbrepo->db_txnid))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "txn_begin");
      return NULL;
    }

  return dbrepo->db_txnid;
}

Repository*
db_repo_initialize_internal (const char* url, RepositoryDriver* driver, int flag)
{
  Repository *repo = repository_new (driver);
  DBRepository *dbrepo = (DBRepository*) repo;
  const char *path = url + strlen ("db:");
  File *base_file;
  SerialSource *src;
  DriverFile *root;
  DBDriverFile *parent_root;

  repo->supports_segments = TRUE;

  if (! (dbrepo->fs_base = path_canonicalize (_fs_repo, path)))
    return NULL;

  base_file = file_initialize (_fs_repo, dbrepo->fs_base);

  if (flag & DB_CREATE)
    {
      if (file_is_not_type (base_file, FV_NotPresent))
	return NULL;

      if (! file_mkdir_p (_fs_repo, db_fs_base (repo)))
	return NULL;
    }
  else
    {
      if (file_is_not_type (base_file, FV_Directory))
	return NULL;
    }

  repo->relative_path = path_root ();

  if (! (dbrepo->db_env = db_env_txn (path)))
    return NULL;

  zero_dbinfo.db_pagesize = 0;
  zero_dbinfo.flags = 0;

  if (db_open (path_to_string (_fs_repo, path_append (dbrepo->fs_base, "directory")),
	       DB_BTREE,
	       flag,
	       0666,
	       dbrepo->db_env,
	       &zero_dbinfo,
	       &dbrepo->directory_dbp))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "db_open");
      return NULL;
    }

  if (db_open (path_to_string (_fs_repo, path_append (dbrepo->fs_base, "shorts")),
	       DB_HASH,
	       flag,
	       0666,
	       dbrepo->db_env,
	       &zero_dbinfo,
	       &dbrepo->shorts_dbp))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "db_open");
      return NULL;
    }

  if (db_open (path_to_string (_fs_repo, path_append (dbrepo->fs_base, "prefix")),
	       DB_HASH,
	       flag,
	       0666,
	       dbrepo->db_env,
	       &zero_dbinfo,
	       &dbrepo->prefix_dbp))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "db_open");
      return NULL;
    }

  if (db_open (path_to_string (_fs_repo, path_append (dbrepo->fs_base, "admin")),
	       DB_HASH,
	       flag,
	       0666,
	       dbrepo->db_env,
	       &zero_dbinfo,
	       &dbrepo->admin_dbp))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "db_open");
      return NULL;
    }

  dbrepo->all_dbps = g_ptr_array_new ();

  g_ptr_array_add (dbrepo->all_dbps, dbrepo->directory_dbp);
  g_ptr_array_add (dbrepo->all_dbps, dbrepo->shorts_dbp);
  g_ptr_array_add (dbrepo->all_dbps, dbrepo->prefix_dbp);
  g_ptr_array_add (dbrepo->all_dbps, dbrepo->admin_dbp);

  if (flag & DB_CREATE)
    {
      SerialSink* sink;
      KeyIterator ski, lki, dki;

      lki.k = 6;
      ski.k = 6;
      dki.k = 6;

      lki.depth = 1;
      ski.depth = 4;
      dki.depth = 4;

      lki.count = 0;
      ski.count = 0;
      dki.count = 0;

      if (! (sink = db_sink_key (repo, dbrepo->admin_dbp, "*repo-state*")))
	return FALSE;

      if (! serialize_repostate (sink, &lki, &ski, &dki) ||
	  ! sink->sink_close (sink))
	return FALSE;

      sink->sink_free (sink);
    }

  if (! (src = db_source_key (repo, dbrepo->admin_dbp, "*repo-state*")))
    return FALSE;

  if (! unserialize_repostate (src, & dbrepo->state) ||
      ! src->source_close (src))
    return FALSE;

  src->source_free (src);

  parent_root = g_new0 (DBDriverFile, 1);

  parent_root->dfile.repo = repo;
  parent_root->df_dir = g_new (SerialDir, 1);
  parent_root->df_dir->prefix = "*root-parent*";

  dbrepo->root_parent_dir = (DriverFile*) parent_root;

  root = db_repo_lstat (repo, NULL, path_root ());

  g_assert (root);

  if (flag & DB_CREATE)
    {
      g_assert (root->type == FV_NotPresent);

      if (! db_repo_make_dir (root))
	return NULL;

      if (! db_repo_commit (repo))
	return NULL;
    }
  else
    {
      g_assert (root->type == FV_Directory);
    }

  dbrepo->root_dir = root;

  return repo;
}

Repository*
db_repo_initialize (const char* url, RepositoryDriver* driver)
{
  return db_repo_initialize_internal (url, driver, 0);
}

Repository*
db_repo_create (const char* url, RepositoryDriver* driver)
{
  return db_repo_initialize_internal (url, driver, DB_CREATE);
}

gboolean
db_repo_exists (const char* url, RepositoryDriver* driver)
{
  const char *path = url + strlen ("db:");
  File *base_file;
  Path *fs_base;
  Repository* repo;

  if (! (fs_base = path_canonicalize (_fs_repo, path)))
    return FALSE;

  base_file = file_initialize (_fs_repo, fs_base);

  if (file_is_not_type_noerr (base_file, FV_Directory))
    return FALSE;

  repo = db_repo_initialize_internal (url, driver, 0);

  if (repo)
    repository_close (repo);

  return repo != NULL;
}

SerialType
db_unserialize_generic (Repository* repo, const char* key, void** ent)
{
  DBRepository *dbrepo = (DBRepository*) repo;
  SerialSource* source = db_source_key (repo, dbrepo->directory_dbp, key);
  SerialType it;

  if (! source)
    {
      (* ent) = NULL;
      return ST_NotFound;
    }

  if (! serializeio_unserialize_generic_acceptable (source, ST_File | ST_Dir | ST_Symlink, &it, ent) ||
      ! source->source_close (source))
    it = ST_Error;

  source->source_free (source);

  return it;
}

/* Permissions
 */

static gboolean
db_repo_mayread_driver_file (DriverFile* file)
{
  return TRUE;
}

static gboolean
db_repo_maymodify_driver_file (DriverFile* file)
{
  return TRUE;
}

/* System-specific stamps and properties.
 */

static gboolean
db_repo_file_properties (DriverFile* file, guint8** arg, guint32* arg_len)
{
  /* @@@ */
  abort ();
}

gboolean
db_repo_properties_restore (DriverFile* file, const guint8* data, guint len)
{
  /* @@@ */
  abort ();
}

gboolean
db_repo_unmodified_since (DriverFile* file, const guint8* data, guint len)
{
  /* @@@ */
  abort ();
}

/* Canonicalize
 */

Path*
db_repo_canonicalize (Repository* repo, const char* url0)
{
  return path_canonicalize_internal (repo, repo->relative_path, '/', FALSE, url0);
}

Path*
db_repo_canonicalize_long (Repository *repo,
			   Path       *relative,
			   const char *url0)
{
  return path_canonicalize_internal (repo, relative, '/', FALSE, url0);
}

const char*
db_repo_path_to_string (Repository* repo, Path* path)
{
  return path_to_string_simple (path, '/');
}

/* Stat ops
 */

gboolean
db_repo_segments_read (DriverFile* file)
{
  DirectoryHandle* dh;
  Path *p, *s;
  DBDriverFile *dbfile = (DBDriverFile*) file;
  gboolean result = TRUE;

  if (! (dh = db_repo_opendir_internal ((DriverFile*) dbfile->parent, path_basename (file->driver_path), TRUE)))
    return FALSE;

  for (;;)
    {
      if (! db_repo_dh_next_internal (dh, &p, &s))
	goto bail;

      if (p == NULL)
	break;

      file_segment_internal (file, s);
    }

  if (0)
    {
    bail:
      result = FALSE;
    }

  if (! db_repo_dh_close (dh))
    return FALSE;

  return result;
}

DriverFile*
db_repo_lstat (Repository* repo, DriverFile* _parent, Path* path)
{
  DBRepository *dbrepo = (DBRepository*) repo;
  DBDriverFile *df = g_new0 (DBDriverFile, 1);
  DBDriverFile *parent = (DBDriverFile*) _parent;
  const char *key;
  void       *ent = NULL;
  SerialType  type;

  if (parent)
    g_assert (parent->dfile.type == FV_Directory);

  df->parent = (DBDriverFile*) (parent ? _parent : dbrepo->root_parent_dir);
  df->dfile.repo = repo;
  df->dfile.driver_path = path;

  df->dfile.segment_stat      = db_repo_segment_stat;
  df->dfile.segment_unlink    = db_repo_segment_unlink;
  df->dfile.segment_length    = db_repo_segment_length;
  df->dfile.segment_view_name = db_repo_segment_view_name;
  df->dfile.driver_readlink   = db_repo_driver_readlink;
  df->dfile.driver_segname    = db_repo_driver_segname;
  df->dfile.driver_seqvalue   = db_repo_driver_seqvalue;
  df->dfile.driver_seqnext    = db_repo_driver_seqnext;
  df->dfile.driver_indextype  = db_repo_driver_indextype;

  key = db_key_build (& df->dfile, path_root ());

  type = db_unserialize_generic (repo, key, &ent);

  if (type == ST_Error)
    return NULL;

  if (ent == NULL)
    {
      df->dfile.type = FV_NotPresent;
    }
  else
    {
      df->df_void = ent;

      switch (type)
	{
	case ST_Symlink:
	  df->dfile.type = FV_SymbolicLink;
	  break;
	case ST_Dir:
	  df->dfile.type = FV_Directory;
	  break;
	case ST_File:
	  df->dfile.type = FV_Regular;
	  break;
	case ST_Sequence:
	  df->dfile.type = FV_Sequence;
	  break;
	case ST_Index:
	  df->dfile.type = FV_Index;

	  if (! (df->df_index->seg_name = path_canonicalize_simple (path_root (), '/', df->df_index->seg_name_str)))
	    return NULL;

	  if (! (df->df_index->fs_path = db_fs_path_from_string (repo, df->df_index->fs_path_str)))
	    return NULL;

	  break;
	default:
	  abort ();
	}
    }

  return (DriverFile*) df;
}

FileSegment*
db_repo_segment_stat (DriverFile* _file, Path* name)
{
  DBDriverFile *file = (DBDriverFile*) _file;
  DBFileSegment *seg;

  const char *key;
  void       *ent = NULL;
  SerialType  type;

  key = db_key_build (& file->dfile, name);

  type = db_unserialize_generic (_file->repo, key, &ent);

  seg = g_new0 (DBFileSegment, 1);

  seg->seg.repo = _file->repo;
  seg->seg.seg_file = _file;
  seg->seg.seg_name = name;
  seg->seg.seg_is_default = (name == path_root ());

  /* currently, each default segment is gotten from the DB twice... optimize later */
  /* seg_len, seg_type, view_name */
  switch (type)
    {
    case ST_NotFound:
    case ST_Sequence:
      seg->seg.seg_type = SV_NotPresent;
      break;

    case ST_File:
      {
	seg->rec = (SerialFile*) ent;
	seg->rec->key = g_strdup (seg->rec->key);

	if (seg->rec->storage != DB_STORAGE_VIEW)
	  {
	    g_assert (seg->rec->storage == DB_STORAGE_LONG || seg->rec->len < DB_MAX_SHORT_LEN);

	    seg->seg.seg_type = SV_Regular;
	  }
	else
	  {
	    seg->seg.seg_type = SV_View;
	  }
      }
      break;

    default:
      seg->seg.seg_type = SV_Invalid;
      break;
    }

  return & seg->seg;
}

const char*
db_repo_driver_readlink (DriverFile* dfile)
{
  DBDriverFile* df = (DBDriverFile*) dfile;

  return df->df_link->link;
}

Path*
db_repo_driver_segname (DriverFile* dfile)
{
  DBDriverFile* df = (DBDriverFile*) dfile;

  return df->df_index->seg_name;
}

gboolean
db_repo_make_symlink (DriverFile* dfile, const char* link)
{
  DBDriverFile* df = (DBDriverFile*) dfile;

  df->df_link = g_new0 (SerialSymlink, 1);
  df->df_link->link = g_strdup (link);

  if (! db_repo_save_df (dfile, FV_SymbolicLink))
    return FALSE;

  return TRUE;
}

gint
db_repo_driver_seqvalue (DriverFile* dfile)
{
  DBDriverFile* df = (DBDriverFile*) dfile;

  return df->df_seq->value;
}

gint
db_repo_driver_seqnext (DriverFile* dfile)
{
  DBDriverFile* df = (DBDriverFile*) dfile;
  gint value;

  value = df->df_seq->value++;

  if (! db_repo_save_df (dfile, FV_Sequence))
    return FALSE;

  return value;
}

gboolean
db_repo_make_sequence (DriverFile* dfile)
{
  DBDriverFile* df = (DBDriverFile*) dfile;

  df->df_seq = g_new0 (SerialSequence, 1);

  if (! db_repo_save_df (dfile, FV_Sequence))
    return FALSE;

  return TRUE;
}

gboolean
db_repo_make_view (FileSegment* seg, guint len, const char* name)
{
  DBFileSegment* dbseg = (DBFileSegment*) seg;

  g_assert (! dbseg->rec);

  dbseg->rec = g_new0 (SerialFile, 1);

  dbseg->rec->len = len;
  dbseg->rec->key = g_strdup (name);
  dbseg->rec->storage = DB_STORAGE_VIEW;

  return db_repo_save_fs (seg);
}

gboolean
db_repo_segment_to_view (FileSegment* seg, const char* name)
{
  DBFileSegment* dbseg = (DBFileSegment*) seg;

  dbseg->rec->key = g_strdup (name);
  dbseg->rec->storage = DB_STORAGE_VIEW;
  seg->seg_type = SV_View;

  return db_repo_save_fs (seg);
}

gboolean
db_repo_make_dir (DriverFile* dfile)
{
  DBDriverFile* df = (DBDriverFile*) dfile;
  const char* nprefix;

  if (! (nprefix = db_dkey (dfile->repo)))
    return FALSE;

  df->df_dir = g_new0 (SerialDir, 1);

  df->df_dir->prefix = g_strdup (nprefix);

  if (! db_repo_store_prefix (dfile, df->df_dir->prefix))
    return FALSE;

  if (! db_repo_save_df (dfile, FV_Directory))
    return FALSE;

  return TRUE;
}

DirectoryHandle*
db_repo_opendir (DriverFile* file, const char* prefix)
{
  return db_repo_opendir_internal (file, prefix, FALSE);
}

DirectoryHandle*
db_repo_opendir_internal (DriverFile* file, const char* prefix, gboolean segs)
{
  DBRepository *dbrepo = (DBRepository*) file->repo;
  DBDirectoryHandle *dh = g_new0 (DBDirectoryHandle, 1);
  DBDriverFile *dfile = (DBDriverFile*) file;

  dh->dh.repo = file->repo;
  dh->dh.next = db_repo_dh_next;
  dh->dh.close = db_repo_dh_close;

  dh->first = DB_SET_RANGE;
  dh->second = DB_NEXT;
  dh->split_key = TRUE;

  dh->dfile = file;

  dh->data.flags = DB_DBT_USERMEM | DB_DBT_PARTIAL;
  dh->data.ulen = 0;

  g_assert (dfile->df_dir->prefix);

  dh->key_str = g_string_new (dfile->df_dir->prefix);

  g_string_sprintfa (dh->key_str, "/%s", prefix);

  if (segs)
    g_string_append_c (dh->key_str, '/');

  init_str_key (& dh->key, dh->key_str->str);

  dh->key.size -= 1; /* don't use its nul */

  if ( (* dbrepo->directory_dbp->cursor) (dbrepo->directory_dbp, db_repo_txn (file->repo), &dh->curs, 0))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "cursor");
      g_free (dh);
      return NULL;
    }

  return & dh->dh;
}

gboolean
db_repo_dh_close (DirectoryHandle* dh0)
{
  DBDirectoryHandle* dh = (DBDirectoryHandle*) dh0;
  gboolean ret = TRUE;

  if (dh->key_str)
    g_string_free (dh->key_str, TRUE);

  if (dh->curs->c_close (dh->curs))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "c_close");
      ret = FALSE;
    }

  g_free (dh);

  return ret;
}

/* Deletion functions
 */

gboolean
db_repo_delete_symlink (DriverFile* dfile)
{
  DBRepository *dbrepo = (DBRepository*) dfile->repo;
  const char* key = db_key_build (dfile, path_root ());
  DBDriverFile *dbfile = (DBDriverFile*) dfile;

  if (! db_object_del (dfile->repo, dbrepo->directory_dbp, db_repo_txn (dfile->repo), key))
    return FALSE;

  g_assert (dbfile->df_link);

  g_free ((void*) dbfile->df_link->link);
  g_free (dbfile->df_link);

  dbfile->df_link = NULL;

  return TRUE;
}

gboolean
db_repo_delete_index (DriverFile* dfile)
{
  DBRepository *dbrepo = (DBRepository*) dfile->repo;
  const char* key = db_key_build (dfile, path_root ());
  DBDriverFile *dbfile = (DBDriverFile*) dfile;

  if (! db_object_del (dfile->repo, dbrepo->directory_dbp, db_repo_txn (dfile->repo), key))
    return FALSE;

  /* @@@ delete and close index file (transaction?) */

  g_assert (dbfile->df_index);

  g_free ((void*) dbfile->df_index->seg_name_str);
  g_free ((void*) dbfile->df_index->fs_path_str);
  g_free (dbfile->df_index);

  dbfile->df_index = NULL;

  return TRUE;
}

gboolean
db_repo_delete_dir (DriverFile* dfile)
{
  DBRepository *dbrepo = (DBRepository*) dfile->repo;
  const char* key = db_key_build (dfile, path_root ());
  DBDriverFile *dbfile = (DBDriverFile*) dfile;

  if (! db_object_del (dfile->repo, dbrepo->directory_dbp, db_repo_txn (dfile->repo), key))
    return FALSE;

  if (! db_repo_unstore_prefix (dfile, dbfile->df_dir->prefix))
    return FALSE;

  g_assert (dbfile->df_dir);

  g_free ((void*) dbfile->df_dir->prefix);
  g_free (dbfile->df_dir);

  dbfile->df_dir = NULL;

  return TRUE;
}

gboolean
db_repo_segment_unlink (FileSegment* seg)
{
  DBRepository *dbrepo = (DBRepository*) seg->repo;
  DBFileSegment* dbseg = (DBFileSegment*) seg;
  const char* key = db_key_build (seg->seg_file, seg->seg_name);

  if (! db_object_del (seg->repo, dbrepo->directory_dbp, db_repo_txn (seg->repo), key))
    return FALSE;

  g_free (dbseg->rec);
  dbseg->rec = NULL;

  return TRUE;
}

guint
db_repo_segment_length (FileSegment* seg)
{
  DBFileSegment* dbseg = (DBFileSegment*) seg;

  return dbseg->rec->len;
}

const char*
db_repo_segment_view_name (FileSegment* seg)
{
  DBFileSegment* dbseg = (DBFileSegment*) seg;

  return dbseg->rec->key;
}

/* Sequence functions
 */

gboolean
db_repo_close_sequence (FileHandle *fh,
			void       *data,
			gssize      len,
			guint8*     immediate,
			Path*       fs_path)
{
  FileSegment *orig_seg = data, *seg;
  File* nfile;

  if (! (nfile = file_sequence_next_internal (orig_seg->seg_file)))
    return FALSE;

  seg = file_segment (nfile, orig_seg->seg_name);

  if (! db_repo_close_replace (fh, seg, len, immediate, fs_path))
    return FALSE;

  return TRUE;
}

FileHandle*
db_repo_sequence_open (FileSegment* seg)
{
  DBRepository *dbrepo = (DBRepository*) seg->repo;

  return db_file_handle_new (db_fs_base (seg->repo),
			     & dbrepo->state->lki,
			     seg,
			     db_repo_close_sequence,
			     segment_handle_to_string,
			     HV_Replace);
}

FileHandle*
db_open_read (FileSegment* seg, gint flags)
{
  DBRepository *dbrepo = (DBRepository*) seg->repo;
  DBFileSegment *dbseg = (DBFileSegment*) seg;

  g_assert (dbseg->rec);

  if (dbseg->rec->storage == DB_STORAGE_SHORT)
    {
      guint8 *buf = g_malloc (dbseg->rec->len);
      DBT dkey, data;
      int err;

      clear_dbts (&dkey, &data);

      init_str_key (&dkey, dbseg->rec->key);

      data.ulen = dbseg->rec->len;
      data.data = buf;
      data.flags = DB_DBT_USERMEM;

      if ( (err = (*dbrepo->shorts_dbp->get) (dbrepo->shorts_dbp, db_repo_txn (seg->repo), &dkey, &data, 0)))
	{
	  if (err == DB_NOTFOUND)
	    {
	      repo_generate_segmentstringerrno_event (EC_RepoShortNotFound, seg, dbseg->rec->key);
	      return NULL;
	    }

	  repo_generate_stringerrno_event (EC_RepoDbError, "get");
	  return NULL;
	}

      return mem_file_handle_new (buf, dbseg->rec->key, dbseg->rec->len, flags);
    }
  else
    {
      Path* path;

      if (! (path = db_fs_path_from_string (seg->repo, dbseg->rec->key)))
	return NULL;

      return fs_file_handle_new (path_to_string (_fs_repo, path),
				 path,
				 dbseg->rec->len,
				 seg,
				 NULL,
				 flags);
    }
}

FileHandle*
db_open_replace (FileSegment* seg, gint flags)
{
  DBRepository *dbrepo = (DBRepository*) seg->repo;

  return db_file_handle_new (db_fs_base (seg->repo),
			     & dbrepo->state->lki,
			     seg,
			     db_repo_close_replace,
			     segment_handle_to_string,
			     flags);
}

gboolean
db_repo_close_replace (FileHandle *fh,
		       void       *data,
		       gssize      len,
		       guint8*     immediate,
		       Path*       fs_path)
{
  FileSegment *seg = data;
  DBFileSegment *dbseg = (DBFileSegment*) seg;
  DBRepository *dbrepo = (DBRepository*) seg->repo;

  if (! driver_set_segment_state (seg, NULL, SV_NotPresent))
    return FALSE;

  g_assert (immediate || fs_path);

  if (! db_repo_save_state (seg->repo))
    return FALSE;

  if (! dbseg->rec)
    dbseg->rec = g_new0 (SerialFile, 1);

  dbseg->rec->len = len;

  g_free ((void*) dbseg->rec->key);

  if (fs_path)
    {
      const char* loc;

      loc = db_fs_path_to_string (seg->repo, fs_path);

      dbseg->rec->storage = DB_STORAGE_LONG;
      dbseg->rec->key = g_strdup (loc);

      g_assert (! immediate);
    }
  else
    {
      const char* hkey;
      DBT key, data;

      g_assert (len <= DB_FIRST_PAGE_SIZE);

      hkey = db_skey (seg->repo);

      clear_dbts (&key, &data);

      init_str_key (&key, hkey);

      data.size = len;
      data.data = immediate;

      dbseg->rec->storage = DB_STORAGE_SHORT;
      dbseg->rec->key = g_strdup (hkey);

      if ((* dbrepo->shorts_dbp->put) (dbrepo->shorts_dbp, db_repo_txn (seg->repo), &key, &data, 0))
	{
	  repo_generate_stringerrno_event (EC_RepoDbError, "put");
	  return FALSE;
	}
    }

  if (! driver_set_segment_state (seg, fh, SV_Regular))
    return FALSE;

  return db_repo_save_fs (seg);
}

/* Index functions
 */

gboolean
db_repo_save_fs (FileSegment *seg)
{
  DBFileSegment* dbseg = (DBFileSegment*) seg;
  DBRepository *dbrepo = (DBRepository*) seg->repo;

  const char *key;
  SerialSink* sink;
  gboolean res;

  /* Update the segment record */

  key = db_key_build (seg->seg_file, seg->seg_name);

  if (! (sink = db_sink_key (seg->repo, dbrepo->directory_dbp, key)))
    return FALSE;

  res = serialize_file_obj (sink, dbseg->rec);
  res &= sink->sink_close (sink);
  sink->sink_free (sink);

  return TRUE;
}

gboolean
db_repo_save_df (DriverFile *df, gint type)
{
  DBRepository *dbrepo = (DBRepository*) df->repo;
  DBDriverFile *dbfile = (DBDriverFile*) df;
  const char *key;
  SerialSink* sink;
  gboolean res;

  key = db_key_build (df, path_root ());

  if (! (sink = db_sink_key (df->repo, dbrepo->directory_dbp, key)))
    return FALSE;

  switch ((RepoFileTypePropertyValue)type)
    {
    case FV_Directory:
      res = serialize_dir_obj (sink, dbfile->df_dir);
      break;
    case FV_Index:
      res = serialize_index_obj (sink, dbfile->df_index);
      break;
    case FV_SymbolicLink:
      res = serialize_symlink_obj (sink, dbfile->df_link);
      break;
    case FV_Sequence:
      res = serialize_sequence_obj (sink, dbfile->df_seq);
      break;
    case FV_NotPresent:
    case FV_Invalid:
    case FV_Regular:
      abort();
      break;
    }

    res &= sink->sink_close (sink);
  sink->sink_free (sink);

  return TRUE;
}

ContainerType
db_repo_driver_indextype (DriverFile* file)
{
  DBDriverFile *dbfile = (DBDriverFile*) file;

  return dbfile->df_index->type;
}

gboolean
db_repo_make_index (DriverFile       *dfile,
		    Path             *index_seg_name,
		    ContainerType         type)
{
  DBRepository *dbrepo = (DBRepository*) dfile->repo;
  Path* index_fs_path;
  const char *index_fs_path_str, *index_seg_name_str;
  DBDriverFile *dbfile = (DBDriverFile*) dfile;
  DB* dbp;

  if (! (index_fs_path = db_generate_data_path (db_fs_base (dfile->repo), & dbrepo->state->lki)))
    return FALSE;

  if (! db_repo_save_state (dfile->repo))
    return FALSE;

  zero_dbinfo.flags = DB_DUP | DB_DUPSORT;

  if (db_open (path_to_string (_fs_repo, index_fs_path),
	       DB_HASH,
	       DB_CREATE,
	       0666,
	       dbrepo->db_env,
	       &zero_dbinfo,
	       &dbp))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "db_open");
      return FALSE;
    }

  g_ptr_array_add (dbrepo->all_dbps, dbp);

  index_fs_path_str = db_fs_path_to_string (dfile->repo, index_fs_path);
  index_seg_name_str = path_to_string_simple (index_seg_name, '/');

  dbfile->df_index = g_new0 (SerialIndex, 1);

  dbfile->df_index->seg_name = index_seg_name;
  dbfile->df_index->fs_path = index_fs_path;
  dbfile->df_index->type = type;

  dbfile->df_index->seg_name_str = g_strdup (index_seg_name_str);
  dbfile->df_index->fs_path_str = g_strdup (index_fs_path_str);

  dbfile->df_index->dbp = dbp;

  if (! db_repo_save_df (dfile, FV_Index))
    return FALSE;

  return TRUE;
}

gboolean
db_repo_index_open (DBDriverFile* dbfile)
{
  DBRepository *dbrepo = (DBRepository*) dbfile->dfile.repo;

  zero_dbinfo.flags = DB_DUP | DB_DUPSORT;

  if (db_open (path_to_string (_fs_repo, dbfile->df_index->fs_path),
	       DB_HASH,
	       0,
	       0666,
	       dbrepo->db_env,
	       &zero_dbinfo,
	       (DB**) &dbfile->df_index->dbp))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "db_open");
      return FALSE;
    }

  return TRUE;
}

gboolean
db_repo_index_insert_file (DriverFile* index, DriverFile* file, const guint8* key_buf, guint key_len)
{
  DBT key, data;
  const char* datastr;
  DBDriverFile *dbfile = (DBDriverFile*) index;
  DB *dbp;

  if (! dbfile->df_index->dbp && ! db_repo_index_open (dbfile))
    return FALSE;

  dbp = dbfile->df_index->dbp;

  clear_dbts (& key, & data);

  key.data = (void*) key_buf;
  key.size = key_len;

  datastr = db_key_build (file, path_root ());

  init_str_key (& data, datastr);

  if (dbp->put (dbp, db_repo_txn (index->repo), &key, &data, 0))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "put");
      return FALSE;
    }

  return TRUE;
}

gboolean
db_repo_index_delete_file (DriverFile* index, DriverFile* file, const guint8* key_buf, guint key_len)
{
  DBT key, data;
  const char* datastr;
  int datastr_len;
  DBDriverFile *dbfile = (DBDriverFile*) index;
  DB *dbp;
  DBC *curs;
  gboolean after = FALSE;
  gboolean ret = TRUE, err;

  if (! dbfile->df_index->dbp && ! db_repo_index_open (dbfile))
    return FALSE;

  dbp = dbfile->df_index->dbp;

  clear_dbts (& key, & data);

  key.data = (void*) key_buf;
  key.size = key_len;

  datastr = db_key_build (file, path_root ());

  datastr_len = strlen (datastr);

  if (dbp->cursor (dbp, db_repo_txn (index->repo), & curs, 0))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "cursor");
      return FALSE;
    }

  while ((err = curs->c_get (curs, &key, &data, after ? DB_NEXT_DUP : DB_SET)) == 0)
    {
      after = TRUE;

      if (data.size == (datastr_len + 1) &&
	  strcmp (data.data, datastr) == 0)
	{
	  if (curs->c_del (curs, 0))
	    {
	      repo_generate_stringerrno_event (EC_RepoDbError, "c_del");
	      ret = FALSE;
	    }

	  goto done;
	}
    }

  if (err == DB_NOTFOUND)
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "c_get");
      ret = FALSE;
    }
  else if (err != 0)
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "c_get");
      ret = FALSE;
    }

 done:

  if (curs->c_close (curs))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "c_close");
      ret = FALSE;
    }

  return ret;
}

gboolean
db_repo_dh_index_key (DirectoryHandle* dh0, const guint8** key, guint* key_len)
{
  DBDirectoryHandle* dh = (DBDirectoryHandle*) dh0;

  (* key) = dh->key.data;
  (* key_len) = dh->key.size;

  return TRUE;
}

gboolean
db_repo_dh_next (DirectoryHandle* dh0, Path** res)
{
  return db_repo_dh_next_internal (dh0, res, NULL);
}

gboolean
db_repo_dh_next_internal (DirectoryHandle* dh0, Path** res, Path **seg)
{
  DBDirectoryHandle* dh = (DBDirectoryHandle*) dh0;
  Path* p;
  Path* s;
  gint err;

  (* res) = NULL;

  if (seg)
    (* seg) = NULL;

 again:

  err = (* dh->curs->c_get) (dh->curs, &dh->key, &dh->data, dh->after ? dh->second : dh->first);

  dh->after = TRUE;

  if (err != 0)
    {
      if (err == DB_NOTFOUND)
	return TRUE;

      repo_generate_stringerrno_event (EC_RepoDbError, "c_get");
      return FALSE;
    }

  /* For [@@@DIRECTORY ONLY] range queries, check that the prefix has not been passed. */
  if (dh->first == DB_SET_RANGE)
    {
      g_assert (dh->key_str);

      if (memcmp (dh->key_str->str, dh->key.data, dh->key_str->len) != 0)
	return TRUE;
    }

  /*g_print ("%s\n", dh->split_key ? dh->key.data : dh->data.data);*/

  if (! db_key_split (dh0->repo, dh->dfile, dh->split_key ? dh->key.data : dh->data.data, & p, & s))
    return FALSE;

  if (! seg && p == dh->last_path)
    goto again;

  dh->last_path = p;

  (* res) = p;

  if (seg)
    (* seg) = s;

  return TRUE;
}

DirectoryHandle*
db_repo_index_iterate (DriverFile* index, IndexQueryFlag flags)
{
  DBDirectoryHandle *dh = g_new0 (DBDirectoryHandle, 1);
  DBDriverFile *dfile = (DBDriverFile*) index;
  DB *dbp;

  if (! dfile->df_index->dbp && ! db_repo_index_open (dfile))
    return FALSE;

  dbp = dfile->df_index->dbp;

  dh->first = DB_FIRST;
  dh->second = DB_NEXT;
  dh->split_key = FALSE;

  dh->dh.repo = index->repo;
  dh->dh.next = db_repo_dh_next;
  dh->dh.close = db_repo_dh_close;
  dh->dh.index_key = db_repo_dh_index_key;

  if (flags & IQF_NoIndexKey)
    {
      dh->key.flags = DB_DBT_USERMEM | DB_DBT_PARTIAL;
      dh->key.ulen = 0;
    }

  if (dbp->cursor (dbp, db_repo_txn (index->repo), &dh->curs, 0))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "cursor");
      g_free (dh);
      return NULL;
    }

  return & dh->dh;
}

DirectoryHandle*
db_repo_index_query (DriverFile       *index,
		     const guint8     *key,
		     guint             key_len,
		     IndexQueryFlag    flags)
{
  /* @@@ implement flags */
  DBDirectoryHandle *dh = g_new0 (DBDirectoryHandle, 1);
  DBDriverFile *dfile = (DBDriverFile*) index;
  DB *dbp;

  if (! dfile->df_index->dbp && ! db_repo_index_open (dfile))
    return FALSE;

  dbp = dfile->df_index->dbp;

  dh->first = DB_SET;
  dh->second = DB_NEXT_DUP;
  dh->split_key = FALSE;

  dh->dh.repo = index->repo;
  dh->dh.next = db_repo_dh_next;
  dh->dh.close = db_repo_dh_close;
  dh->dh.index_key = db_repo_dh_index_key;

  dh->key.data = (void*) key;
  dh->key.size = key_len;

  if (dbp->cursor (dbp, db_repo_txn (index->repo), &dh->curs, 0))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "cursor");
      g_free (dh);
      return NULL;
    }

  return & dh->dh;
}

/* Close
 */
gboolean
db_repo_close (Repository *repo)
{
  DBRepository *dbrepo = (DBRepository*) repo;
  int i;

  if (dbrepo->db_txnid != NULL)
    {
      if (txn_commit (dbrepo->db_txnid) == DB_RUNRECOVERY)
	{
	  repo_generate_stringerrno_event (EC_RepoDbError, "txn_commit");
	  return FALSE;
	}

      dbrepo->db_txnid = NULL;
    }

  for (i = 0; i < dbrepo->all_dbps->len; i += 1)
    {
      DB* dbp = dbrepo->all_dbps->pdata[i];

      if ( (* dbp->close) (dbp, 0))
	{
	  repo_generate_stringerrno_event (EC_RepoDbError, "close");
	  return FALSE;
	}
    }

  if (! db_env_close (dbrepo->db_env))
    return FALSE;

  return TRUE;
}

gboolean
db_repo_commit (Repository *repo)
{
  DBRepository *dbrepo = (DBRepository*) repo;
  int i;

  if (! repository_commit (_fs_repo))
    return FALSE;

  if (dbrepo->db_txnid && txn_commit (dbrepo->db_txnid))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "txn_commit");
      return FALSE;
    }

  dbrepo->db_txnid = NULL;

  for (i = 0; i < dbrepo->all_dbps->len; i += 1)
    {
      DB* dbp = dbrepo->all_dbps->pdata[i];

      if ( (* dbp->sync) (dbp, 0))
	{
	  repo_generate_stringerrno_event (EC_RepoDbError, "sync");
	  return FALSE;
	}
    }

  return TRUE;
}

gboolean
db_repo_init ()
{
  static gboolean already = FALSE;

  if (! already)
    {
      SimpleDriver db_driver;

      memset (&db_driver, 0, sizeof (db_driver));

      db_driver.canonicalize = db_repo_canonicalize;
      db_driver.canonicalize_long = db_repo_canonicalize_long;
      db_driver.will_handle = db_repo_will_handle;
      db_driver.path_to_string = db_repo_path_to_string;
      db_driver.initialize = db_repo_initialize;
      db_driver.create = db_repo_create;
      db_driver.exists = db_repo_exists;
      db_driver.lstat = db_repo_lstat;
      db_driver.sequence_open = db_repo_sequence_open;

      db_driver.make_dir = db_repo_make_dir;
      db_driver.make_view = db_repo_make_view;
      db_driver.make_sequence = db_repo_make_sequence;
      db_driver.segment_to_view = db_repo_segment_to_view;
      db_driver.make_symlink = db_repo_make_symlink;

      db_driver.delete_symlink = db_repo_delete_symlink;
      db_driver.delete_dir = db_repo_delete_dir;
      db_driver.delete_index = db_repo_delete_index;

      db_driver.opendir = db_repo_opendir;
      db_driver.commit = db_repo_commit;
      db_driver.close = db_repo_close;
      db_driver.open_replace = db_open_replace;
      db_driver.open_read = db_open_read;
      db_driver.segments_read = db_repo_segments_read;

      db_driver.properties_restore = db_repo_properties_restore;
      db_driver.unmodified_since = db_repo_unmodified_since;
      db_driver.file_properties = db_repo_file_properties;
      db_driver.file_maymodify = db_repo_maymodify_driver_file;
      db_driver.file_mayread = db_repo_mayread_driver_file;

      db_driver.make_index = db_repo_make_index;
      db_driver.index_query = db_repo_index_query;
      db_driver.index_iterate = db_repo_index_iterate;
      db_driver.index_insert_file = db_repo_index_insert_file;
      db_driver.index_delete_file = db_repo_index_delete_file;

      driver_new (sizeof (DBRepository), &db_driver, "Berkeley DB 2.0 (Sleepycat) Database Repository");

      already = TRUE;
    }

  return TRUE;
}

/* Misc specialized DB access routines
 */

/* This stores a reverse prefix->directory map entry */
gboolean
db_repo_store_prefix (DriverFile* dfile, const char* prefix)
{
  DBRepository *dbrepo = (DBRepository*) dfile->repo;
  const char* dkey = path_to_string_simple (dfile->driver_path, '/');
  DBT key, data;

  clear_dbts (& key, & data);

  init_str_key (& key, prefix);
  key.size -= 1;

  init_str_key (& data, dkey);

  if (dbrepo->prefix_dbp->put (dbrepo->prefix_dbp, db_repo_txn (dfile->repo), &key, &data, 0))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "put");
      return FALSE;
    }

  return TRUE;
}

/* This deletes a reverse prefix->directory map entry */
gboolean
db_repo_unstore_prefix (DriverFile* dfile, const char* prefix)
{
  DBRepository *dbrepo = (DBRepository*) dfile->repo;
  DBT key;

  clear_dbts (& key, NULL);

  init_str_key (& key, prefix);
  key.size -= 1;

  if (dbrepo->prefix_dbp->del (dbrepo->prefix_dbp, db_repo_txn (dfile->repo), &key, 0))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "del");
      return FALSE;
    }

  return TRUE;
}

/* looks for a directory with a certain prefix */
DriverFile*
db_repo_lookup_prefix (Repository *repo, const char* prefix, guint prefix_len)
{
  /* @@@ cache this result! */
  DBRepository* dbrepo = (DBRepository*) repo;
  DBT key, data;
  int err;
  File* f;
  Path* p;

  clear_dbts (& key, & data);

  key.data = (void*) prefix;
  key.size = prefix_len;

  if ((err = dbrepo->prefix_dbp->get (dbrepo->prefix_dbp, db_repo_txn (repo), &key, &data, 0)))
    {
      if (err == DB_NOTFOUND)
	{
	  char* it = g_strndup (prefix, prefix_len);
	  /* this is stupid */
	  repo_generate_string_event (EC_RepoInconsistentPrefixDatabase, it);
	  g_free (it);
	  return NULL;
	}

      repo_generate_stringerrno_event (EC_RepoDbError, "get");
      return NULL;
    }

  if (! (p = path_canonicalize_simple (path_root (), '/', data.data)))
    return NULL;

  f = file_initialize (repo, p);

  if (file_is_not_type (f, FV_Directory))
    return NULL;

  return f->driver_file;
}

/* This saves the state vector in the admin DB */
gboolean
db_repo_save_state (Repository *repo)
{
  DBRepository *dbrepo = (DBRepository*) repo;
  SerialSink *sink;

  if (! (sink = db_sink_key (repo, dbrepo->admin_dbp, "*repo-state*")))
    return FALSE;

  if (! serialize_repostate_obj (sink, dbrepo->state) ||
      ! sink->sink_close (sink))
    return FALSE;

  sink->sink_free (sink);

  return TRUE;
}

/* DB Source
 */
typedef struct _DbSerialSource DbSerialSource;

struct _DbSerialSource {
  SerialSource source;

  Repository* repo;

  guint8* source_buf;
  guint source_len;
  guint source_pos;
};

static gboolean   db_source_close          (SerialSource* source);
static gboolean   db_source_read           (SerialSource* source, guint8 *ptr, guint32 len);
static void       db_source_free           (SerialSource* source);

SerialSource*
db_source (Repository* repo, guint8* seg, guint seg_len)
{
  DbSerialSource* it = g_new0(DbSerialSource, 1);

  serializeio_source_init (&it->source, NULL, db_source_close, db_source_read, db_source_free, NULL, NULL);

  it->repo = repo;
  it->source_buf = seg;
  it->source_len = seg_len;

  return &it->source;
}

static SerialSource*
db_source_key (Repository* repo, DB* dbp, const char* key)
{
  DBT dkey, data;
  gint err;

  clear_dbts (&dkey, &data);

  init_str_key (&dkey, key);

  if ( (err = dbp->get (dbp, db_repo_txn (repo), &dkey, &data, 0)) )
    {
      if (err == DB_NOTFOUND)
	return NULL;

      repo_generate_stringerrno_event (EC_RepoDbError, "get");
      return NULL;
    }

  return db_source (repo, data.data, data.size);
}

gboolean
db_source_close (SerialSource* source)
{
  return TRUE;
}

gboolean
db_source_read (SerialSource* source, guint8 *ptr, guint32 len)
{
  DbSerialSource* ssource = (DbSerialSource*) source;

  if (len + ssource->source_pos > ssource->source_len)
    {
      repo_generate_source_event (EC_RepoSourceEof, source);
      return FALSE;
    }

  memcpy (ptr, ssource->source_buf + ssource->source_pos, len);

  ssource->source_pos += len;

  return TRUE;
}

void
db_source_free (SerialSource* source)
{
  g_free (source);
}

/* DB Sink
 */

typedef struct _DbSerialSink DbSerialSink;

struct _DbSerialSink {
  SerialSink sink;

  Repository* repo;

  GByteArray* stage;

  const char* key;

  DB* dbp;
};

static gboolean     db_sink_close    (SerialSink* sink);
static gboolean     db_sink_write    (SerialSink* sink, const guint8 *ptr, guint32 len);
static void         db_sink_free     (SerialSink* sink);

SerialSink*
db_sink_key (Repository* repo, DB* dbp, const char* key)
{
  DbSerialSink* it = g_new0 (DbSerialSink, 1);

  serializeio_sink_init (&it->sink,
			 NULL,
			 db_sink_close,
			 db_sink_write,
			 db_sink_free,
			 NULL);

  it->repo = repo;
  it->key = key;
  it->dbp = dbp;

  return &it->sink;
}

gboolean
db_sink_close (SerialSink* sink)
{
  DbSerialSink* ssink = (DbSerialSink*) sink;
  DBT key, data;

  clear_dbts (&key, &data);

  init_str_key (&key, ssink->key);
  data.size = ssink->stage->len;
  data.data = ssink->stage->data;

  if (ssink->dbp->put (ssink->dbp, db_repo_txn (ssink->repo), &key, &data, 0))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "put");
      return FALSE;
    }

  return TRUE;
}

gboolean
db_sink_write (SerialSink* sink, const guint8 *ptr, guint32 len)
{
  DbSerialSink* ssink = (DbSerialSink*) sink;

  if (! ssink->stage)
    ssink->stage = g_byte_array_new ();

  g_byte_array_append (ssink->stage, ptr, len);

  return TRUE;
}

void
db_sink_free (SerialSink* sink)
{
  DbSerialSink* ssink = (DbSerialSink*) sink;

  g_byte_array_free (ssink->stage, TRUE);

  g_free (sink);
}

/* Key manipulation
 */

const char*
db_skey (Repository* repo)
{
  DBRepository *dbrepo = (DBRepository*) repo;

  return db_key_iterate (repo, & dbrepo->state->ski);
}

const char*
db_dkey (Repository* repo)
{
  DBRepository *dbrepo = (DBRepository*) repo;

  return db_key_iterate (repo, & dbrepo->state->dki);
}

const char*
db_key_iterate (Repository* repo, KeyIterator *ki)
{
  const guint8* key;
  static char keystr[MAXKEY];
  int key_len, i;

  g_assert (ki->k == 6);

  key = key_iterate (ki, & key_len);

  for (i = 0; i < key_len; i += 1)
    keystr[i] = sorta_base64_table[key[i]];

  if (! db_repo_save_state (repo))
    return FALSE;

  keystr[key_len] = 0;

  return keystr;
}

const char*
db_key_build (DriverFile* file, Path* seg_name)
{
  DBDriverFile *df = (DBDriverFile*) file;
  GString* str;
  Path *p = path_append_path (path_absolute (path_basename (file->driver_path)), seg_name);

  str = temporary_string (NULL);

  g_string_append (str, df->parent->df_dir->prefix);
  g_string_append (str, path_to_string_simple (p, '/'));

  if (seg_name == path_root ())
    g_string_append_c (str, '/');

  return str->str;
}

gboolean
db_key_split (Repository* repo, DriverFile* df, const char* key, Path** path, Path** seg_name)
{
  DBRepository *dbrepo = (DBRepository*) repo;
  DBDriverFile* dbfile;
  const char* first_slash = strchr (key, '/');
  const char* second_slash;
  Path* s;

  *path = NULL;
  *seg_name = NULL;

  if (! first_slash)
    {
      repo_generate_string_event (EC_RepoInvalidKey, key);
      return FALSE;
    }

  second_slash = strchr (first_slash + 1, '/');

  if (! second_slash)
    {
      repo_generate_string_event (EC_RepoInvalidKey, key);
      return FALSE;
    }

  if (! (s = path_canonicalize_simple (path_root (), '/', second_slash)))
    return FALSE;

  (* seg_name) = s;

  if (strncmp (key, "*root-parent*", 13) == 0)
    {
      (*path) = path_root ();

      g_assert (df == dbrepo->root_parent_dir);

      return TRUE;
    }

  if (! df)
    {
      if (! (df = db_repo_lookup_prefix (repo, key, first_slash - key)))
	return FALSE;
    }

  dbfile = (DBDriverFile*) df;

  g_assert (strncmp (key, dbfile->df_dir->prefix, first_slash - key) == 0);

  (* path) = path_append_n (df->driver_path, first_slash + 1, second_slash - first_slash - 1);

  return TRUE;
}
