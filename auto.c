/* -*-Mode: C;-*-
 * $Id$
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "repo.h"
#include "repopriv.h"

typedef struct _DigestVerifyData DigestVerifyData;

struct _DigestVerifyData {
  FileSegment         *seg;
  const MessageDigest *md;
  AutoSegment         *a;
  AutoSegments        *as;
};

static RepoSegmentAutoSegmentsProperty SP_AutoSegments;

static gboolean auto_property_register (FileSegment *seg, const char* name, gboolean positive);
static gboolean auto_digest_verify_close (FileHandle* fh, void* data);
static gboolean auto_digest_write (FileSegment *seg, const guint8* digest, const MessageDigest *md, gboolean positive);
static guint8*  auto_digest_read (FileSegment *mdseg, const MessageDigest* md);

static void pack_autos (AutoSegments *as);
static void unpack_autos (AutoSegments *as);

static gboolean auto_digest_trigger (gint32       code,
				     const char  *code_name,
				     File        *target_dir,
				     File        *target_file,
				     Path        *target_seg_name,
				     FileHandle  *target_handle,
				     const char  *trigger_argument,
				     gint32       trigger_id,
				     gint32       trigger_flags,
				     gint32       trigger_parent_id,
				     gint32       trigger_event_type);

gboolean
auto_library_init (void)
{
  if (! trigger_type_register_internal (AUTO_DIGEST_TRIGGER_TYPE, "Automatic Digest Property Trigger", auto_digest_trigger))
    return FALSE;

  if (! edsio_new_segment_autosegments_property ("Auto", PF_Persistent, & SP_AutoSegments))
    return FALSE;

  return TRUE;
}

/* Auto Digests
 */

gboolean
auto_digest_trigger (gint32       code,
		     const char  *code_name,
		     File        *target_dir,
		     File        *target_file,
		     Path        *target_seg_name,
		     FileHandle  *target_handle,
		     const char  *trigger_argument,
		     gint32       trigger_id,
		     gint32       trigger_flags,
		     gint32       trigger_parent_id,
		     gint32       trigger_event_type)
{
  const MessageDigest* md;

  if (! (md = edsio_message_digest_by_name (trigger_argument)))
    return FALSE;

  switch (trigger_event_type)
    {
    case TE_OpenReplace:

      if (! handle_digest_compute (target_handle, md))
	return FALSE;

      break;

    case TE_SegmentCreate:

      {
	FileSegment *seg = file_segment (target_file, target_seg_name);

	if (target_handle)
	  {
	    const guint8* digest;

	    g_assert (seg->seg_type == SV_Regular);

	    if (! (digest = handle_digest (target_handle, md)))
	      return FALSE;

	    if (! auto_digest_write (seg, digest, md, TRUE))
	      return FALSE;
	  }
	else
	  {
	    g_assert (seg->seg_type == SV_View);
#if 0 /* This is bad */
	    if (! segment_digest (seg, md))
	      return FALSE;
#endif
	  }
      }
      break;

    default:
      abort ();
    }

  return TRUE;
}

gboolean
auto_digest_property (File                *dir,
		      Path                *seg_name,
		      const MessageDigest *md,
		      gboolean             recurse)
{
  g_return_val_if_fail (dir && seg_name && md, FALSE);

  if (file_is_not_type (dir, FV_Directory))
    return FALSE;

  if (! trigger_add (dir,
		     AUTO_DIGEST_TRIGGER_TYPE,
		     seg_name,
		     TE_OpenReplace | TE_SegmentCreate,
		     recurse ? TF_Recursive : 0,
		     md->name))
    return FALSE;

  return TRUE;
}

void
unpack_autos (AutoSegments *as)
{
  int i;

  if (! as->segs_write)
    as->segs_write = g_ptr_array_new ();

  for (i = 0; i < as->segs_len; i += 1)
    g_ptr_array_add (as->segs_write, (void*) as->segs[i]);
}

void
pack_autos (AutoSegments *as)
{
  as->segs = (AutoSegment**) as->segs_write->pdata;
  as->segs_len = as->segs_write->len;
}

gboolean
auto_property_register (FileSegment *seg, const char* name, gboolean positive)
{
  AutoSegment *a;
  AutoSegments *as;

  if (! segment_test_autosegments (seg, SP_AutoSegments))
    {
      as = g_new0 (AutoSegments, 1);
    }
  else
    {
      if (! segment_get_autosegments (seg, SP_AutoSegments, & as))
	return FALSE;
    }

  unpack_autos (as);

  a = g_new0 (AutoSegment, 1);

  a->code = AUTO_DIGEST_AUTO_PROPERTY_CODE;
  a->name = name;
  a->positive = positive;

  g_ptr_array_add (as->segs_write, a);

  pack_autos (as);

  if (! segment_set_autosegments (seg, SP_AutoSegments, as))
    return FALSE;

  return TRUE;
}

gboolean
auto_property_delete_trigger (File* dir, File* target, Path* seg_name, gint32 event_type)
{
  FileSegment* seg = file_segment (target, seg_name);
  AutoSegments *as;
  int i;

  if (! segment_test_autosegments (seg, SP_AutoSegments))
    return TRUE;

  if (! segment_get_autosegments (seg, SP_AutoSegments, & as))
    return FALSE;

  for (i = 0; i < as->segs_len; i += 1)
    {
      FileSegment* dseg = file_segment (target, path_append (seg_name, as->segs[i]->name));

      if (! segment_erase (dseg, FALSE))
	return FALSE;
    }

  if (! segment_unset_autosegments (seg, SP_AutoSegments))
    return FALSE;

  return TRUE;
}

const guint8*
file_digest (File                *file,
	     const MessageDigest *md)
{
  return segment_digest (file_default_segment (file), md);
}

gboolean
auto_digest_write (FileSegment *seg, const guint8* digest, const MessageDigest *md, gboolean positive)
{
  FileSegment *out_seg = file_segment_internal (seg->seg_file, path_append (seg->seg_name, md->name));
  FileHandle *out;

  if (! auto_property_register (seg, md->name, positive))
    return FALSE;

  if (! (out = segment_open (out_seg, HV_Replace)))
    return FALSE;

  if (! handle_write (out, digest, md->cksum_len))
    return FALSE;

  if (! handle_close (out))
    return FALSE;

  return TRUE;
}

guint8*
auto_digest_read (FileSegment *mdseg, const MessageDigest* md)
{
  guint8* digest;
  FileHandle *fh;

  g_assert (segment_length (mdseg) == md->cksum_len);

  digest = g_malloc (md->cksum_len);

  if (! (fh = segment_open (mdseg, HV_Read)))
    return NULL;

  if (! handle_read (fh, digest, md->cksum_len))
    return NULL;

  if (! handle_close (fh))
    return NULL;

  return digest;
}

const guint8*
segment_digest (FileSegment         *seg,
		const MessageDigest *md)
{
  FileHandle *fh;
  guint8 *digest;
  FileSegment *mdseg;

  if (seg->repo->supports_segments)
    {
      mdseg = file_segment_internal (seg->seg_file, path_append (seg->seg_name, md->name));

      if (mdseg->seg_type == SV_Regular)
	{
	  if (! (digest = auto_digest_read (mdseg, md)))
	    return FALSE;

	  return digest;
	}
    }

  if (! (fh = segment_open (seg, HV_Read)))
    return FALSE;

  handle_digest_compute (fh, md);

  if (! (digest = (void*) handle_digest (fh, md)))
    return FALSE;

  if (! handle_close (fh))
    return FALSE;

  if (seg->repo->supports_segments)
    {
      if (! auto_digest_write (seg, digest, md, TRUE))
	return FALSE;
    }

  return digest;
}

gboolean
segment_is_auto (FileSegment *act_seg)
{
  FileSegment* seg;
  AutoSegments *as;
  int i;

  if (act_seg->seg_name == path_root ())
    return FALSE;

  seg = file_segment_internal (act_seg->seg_file, path_dirname (act_seg->seg_name));

  if (! segment_test_autosegments (seg, SP_AutoSegments))
    return FALSE;

  if (! segment_get_autosegments (seg, SP_AutoSegments, & as))
    return FALSE;

  for (i = 0; i < as->segs_len; i += 1)
    {
      if (strcmp (as->segs[i]->name, path_basename (act_seg->seg_name)) == 0)
	return TRUE;
    }

  return FALSE;
}

gboolean
file_digest_assert (File                *file,
		    const MessageDigest *md,
		    const guint8        *val)
{
  return segment_digest_assert (file_default_segment (file), md, val);
}

gboolean
segment_digest_assert (FileSegment         *seg,
		       const MessageDigest *md,
		       const guint8        *val)
{
  FileSegment *mdseg;

  g_assert (seg->repo->supports_segments);

  mdseg = file_segment_internal (seg->seg_file, path_append (seg->seg_name, md->name));

  if (segment_is_not_type (mdseg, SV_NotPresent))
    return FALSE;

  if (! auto_digest_write (seg, val, md, FALSE))
    return FALSE;

  return TRUE;
}

gboolean
auto_digest_verify_close (FileHandle* fh, void* vdata)
{
  DigestVerifyData* data = vdata;
  const guint8* digest;
  guint8* assert_digest;
  FileSegment *mdseg = file_segment_internal (data->seg->seg_file, path_append (data->seg->seg_name, data->md->name));

  if (! (digest = handle_digest (fh, data->md)))
    return FALSE;

  if (! (assert_digest = auto_digest_read (mdseg, data->md)))
    return FALSE;

  if (memcmp (digest, assert_digest, data->md->cksum_len) != 0)
    {
      repo_generate_segmentstring_event (EC_RepoDigestAssertionFailed, data->seg, data->md->name);
      return FALSE;
    }

#if 0 /* Always assert view digests. */
  data->a->positive = TRUE;

  if (! segment_set_autosegments (data->seg, SP_AutoSegments, data->as))
    return FALSE;
#endif

  g_free (data);

  return TRUE;
}

gboolean
auto_digest_verify (FileSegment *seg,
		    FileHandle  *fh)
{
  AutoSegments *as;
  int i;

  if (! seg->repo->supports_segments)
    return TRUE;

  if (! segment_test_autosegments (seg, SP_AutoSegments))
    return TRUE;

  if (! segment_get_autosegments (seg, SP_AutoSegments, & as))
    return FALSE;

  for (i = 0; i < as->segs_len; i += 1)
    {
      DigestVerifyData* data;
      const MessageDigest* md;

      g_assert (as->segs[i]->code == AUTO_DIGEST_AUTO_PROPERTY_CODE);

      if (as->segs[i]->positive)
	continue;

      if (! (md = edsio_message_digest_by_name (as->segs[i]->name)))
	return FALSE;

      data = g_new (DigestVerifyData, 1);

      data->seg = seg;
      data->md = md;
      data->a = as->segs[i];
      data->as = as;

      handle_digest_compute (fh, md);

      handle_close_hook (fh, data, auto_digest_verify_close);
    }

  return TRUE;
}
