/* -*-Mode: C;-*-
 * $Id: repo.c 1.93 Thu, 06 May 1999 00:58:18 -0700 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "repo.h"
#include "repopriv.h"
#include <edsiostdio.h>
#include <ctype.h>

#if TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif

static FileSegment* repo_persist_dir_segment (File* dir, const char* prop_name);

static void repository_close_check (void);

static GSList* _repository_system_drivers = NULL;
static gboolean _repository_system_open = FALSE;

gboolean
repository_system_init (void)
{
  gboolean res = TRUE;

  if (_repository_system_open)
    return TRUE;

  res &= edsio_library_init ();
  res &= repo_edsio_init ();
  res &= mem_pool_init ();
  res &= fs_repo_init ();
  res &= db_repo_init ();
  res &= driver_library_init ();
  res &= sequence_library_init ();
  res &= trigger_library_init ();
  res &= index_library_init ();
  res &= auto_library_init ();

  g_atexit (repository_close_check);

  if (res && ! (_fs_repo = repository_initialize ("file:")))
    return FALSE;

  _repository_system_open = TRUE;

  return res;
}

void
repository_close_check (void)
{
  if (_repository_system_open)
    g_warning ("Repository system not closed");
}

gboolean
repository_system_close (void)
{
  if (! repository_commit (_fs_repo))
    return FALSE;

  if (! repository_close (_fs_repo))
    return FALSE;

  _repository_system_open = FALSE;

  return TRUE;
}

void
repository_register_driver (RepositoryDriver *driver)
{
  _repository_system_drivers = g_slist_prepend (_repository_system_drivers, driver);
}

Repository*
repository_initialize (const char* url)
{
  GSList* drivers;

  g_return_val_if_fail (url, NULL);

  for (drivers = _repository_system_drivers; drivers; drivers = drivers->next)
    {
      RepositoryDriver* driver = (RepositoryDriver*) drivers->data;

      if (driver->simp_driver.will_handle (url, driver))
	return (* driver->simp_driver.initialize) (url, driver);
    }

  repo_generate_string_event (EC_RepoNoDriver, url);

  return NULL;
}

Repository*
repository_create (const char* url)
{
  GSList* drivers;

  g_return_val_if_fail (url, NULL);

  for (drivers = _repository_system_drivers; drivers; drivers = drivers->next)
    {
      RepositoryDriver* driver = (RepositoryDriver*) drivers->data;

      if (driver->simp_driver.will_handle (url, driver))
	return (* driver->simp_driver.create) (url, driver);
    }

  repo_generate_string_event (EC_RepoNoDriver, url);

  return NULL;
}

gboolean
repository_exists (const char* url)
{
  GSList* drivers;

  g_return_val_if_fail (url, FALSE);

  for (drivers = _repository_system_drivers; drivers; drivers = drivers->next)
    {
      RepositoryDriver* driver = (RepositoryDriver*) drivers->data;

      if (driver->simp_driver.will_handle (url, driver))
	return (* driver->simp_driver.exists) (url, driver);
    }

  repo_generate_string_event (EC_RepoNoDriver, url);

  return FALSE;
}

gboolean
repository_commit (Repository* repo)
{
  g_return_val_if_fail (repo, FALSE);

  return (* repo->driver->commit) (repo);
}

gboolean
repository_rollback (Repository* repo)
{
  g_return_val_if_fail (repo, FALSE);

  return (* repo->driver->rollback) (repo);
}

gboolean
repository_close (Repository* repo)
{
  g_return_val_if_fail (repo, FALSE);

  return (* repo->driver->close) (repo);
}

Repository*
repository_new (RepositoryDriver* driver)
{
  Repository* repo = (Repository*) g_malloc0 (driver->repo_sizeof);

  repo->driver        = driver;
  repo->relative_path = path_root ();
  repo->all_files     = g_ptr_array_new ();

  repo->lstat_files            = g_hash_table_new (g_direct_hash, g_direct_equal);
  repo->stat_files             = g_hash_table_new (g_direct_hash, g_direct_equal);
  repo->driver_files           = g_hash_table_new (g_direct_hash, g_direct_equal);
  repo->open_directory_handles = g_hash_table_new (g_direct_hash, g_direct_equal);
  /*repo->open_file_handles    = g_hash_table_new (g_direct_hash, g_direct_equal);*/

  if (! driver_init (repo))
    return NULL;

  return repo;
}

gboolean
file_copy (File* file1, File* file2)
{
  /* @@@ segments? */
  return segment_copy (file_default_segment (file1), file_default_segment (file2));
}

gboolean
segment_copy (FileSegment* file1, FileSegment* file2)
{
  FileHandle* h1 = NULL, *h2 = NULL;

  h1 = segment_open (file1, HV_Read | HV_NoSeek);

  if (! h1)
    goto fail;

  h2 = segment_open (file2, HV_Replace);

  if (! h2)
    goto fail;

  if (! handle_copy (h1, h2, 0, handle_length (h1)))
    goto fail;

  if (! handle_close (h1))
    {
      h1 = NULL;
      goto fail;
    }

  h1 = NULL;

  if (! handle_close (h2))
    {
      h2 = NULL;
      goto fail;
    }

  h2 = NULL;

  return TRUE;

fail:

  if (h1) handle_abort (h1);
  if (h2) handle_abort (h2);

  return FALSE;
}

File*
file_mkdir_p (Repository* repo, Path* path)
{
  File* f;

  g_return_val_if_fail (repo && path, FALSE);

  if (path == path_root ())
    return file_initialize (repo, path_root ());

  if (! file_mkdir_p (repo, path_dirname (path)))
    return NULL;

  f = file_initialize (repo, path);

  if (file_is_not_type (f, FV_Directory | FV_NotPresent))
    return NULL;

  if (file_is_type_noerr (f, FV_Directory))
    return f;

  if (! file_mkdir (f))
    return NULL;

  return f;
}

Path*
repository_getcwd (Repository* repo)
{
  return repo->relative_path;
}

gboolean
repository_chdir (Repository* repo, Path* new_path)
{
  File* dir = file_initialize (repo, new_path);

  if (file_is_not_type (dir, FV_Directory))
    return FALSE;

  repo->relative_path = new_path;

  return TRUE;
}

GString*
temporary_string (const char* init)
{
#define TEMP_STRINGS 32
  static GString** strs = NULL;
  static gint path_i = 0;
  GString *ret;

  if (strs == NULL)
    {
      gint i;

      strs = g_new (GString*, TEMP_STRINGS);

      for (i = 0; i < TEMP_STRINGS; i += 1)
	strs[i] = g_string_new (NULL);
    }

  ret = strs[path_i];

  g_string_truncate (ret, 0);

  if (init)
    g_string_append (ret, init);

  path_i += 1;
  path_i %= TEMP_STRINGS;

  return ret;
}

void
clear_dbts (DBT*one, DBT*two)
{
  if (one) memset (one, 0, sizeof (*one));
  if (two) memset (two, 0, sizeof (*two));
}

void
clear_dbt (DBT*one)
{
  if (one) memset (one, 0, sizeof (*one));
}

void
init_str_key (DBT* key, const char* str)
{
  key->data = (void*) str;
  key->size = strlen (str) + 1;
}

SerialSource*
segment_source (FileSegment* seg)
{
  FileHandle* fh;

  fh = seg->repo->driver->open_file (seg, HV_Read);

  if (! fh)
    return NULL;

  return handle_source (fh);
}

SerialSink*
segment_sink (FileSegment *seg, gpointer data1, gpointer data2, gpointer data3, gboolean (* cont_onclose) (gpointer data1, gpointer data2, gpointer data3))
{
  FileHandle* fh;

  fh = seg->repo->driver->open_file (seg, HV_Replace);

  if (! fh)
    return NULL;

  return handle_sink (fh, data1, data2, data3, cont_onclose);
}

FileSegment*
file_default_segment (File* file)
{
  return file_segment (file, path_root ());
}

FileSegment*
file_segment (File* file, Path* name)
{
  return file_segment_internal (file->driver_file, name);
}

FileSegment*
file_segment_internal (DriverFile* file, Path* name)
{
  FileSegment* fs;

  for (fs = file->segment_list; fs; fs = fs->next)
    {
      if (fs->seg_name == name)
	return fs;
    }

  fs = file->segment_stat (file, name);

  fs->next = file->segment_list;
  file->segment_list = fs;

  return fs;
}

Repository* repository_of_filesegment (FileSegment* it) { return it->repo; }
Repository* repository_of_file (File* it) { return it->repo; }

const char*
eventdelivery_file_to_string (File* x)
{
  return g_strdup (path_to_string (x->repo, file_access_path (x)));
}

const char*
eventdelivery_path_to_string (Path* x)
{
  return g_strdup (path_to_string (_fs_repo, x));
}

const char*
segment_handle_to_string (void* data)
{
  return eventdelivery_segment_to_string (data);
}

const char*
eventdelivery_segment_to_string (FileSegment* x)
{
  return g_strdup_printf ("%s%s%s",
			  path_to_string (x->repo, x->seg_file->driver_path),
			  x->seg_is_default ? "" : ".",
			  x->seg_is_default ? "" : ((path_to_string_simple (x->seg_name, '.') + 1)));
}

SerialSource*
repo_persist_file_source (File *file, const char* prop_name)
{
  FileSegment* pseg = file_segment (file, path_absolute (prop_name));

  return segment_source (pseg);
}

SerialSink*
repo_persist_file_sink   (File *file, const char* prop_name)
{
  FileSegment* pseg = file_segment (file, path_absolute (prop_name));

  return segment_sink (pseg, NULL, NULL, NULL, NULL);
}

gboolean
repo_persist_file_test  (File *file, const char* prop_name)
{
  FileSegment* pseg = file_segment (file, path_absolute (prop_name));

  return segment_is_type_noerr (pseg, SV_Regular);
}

gboolean
repo_persist_file_unset  (File *file, const char* prop_name)
{
  FileSegment* pseg = file_segment (file, path_absolute (prop_name));

  return segment_erase (pseg, FALSE);
}

SerialSource*
repo_persist_segment_source (FileSegment *seg, const char* prop_name)
{
  FileSegment* pseg = file_segment_internal (seg->seg_file, path_append (seg->seg_name, prop_name));

  return segment_source (pseg);
}

SerialSink*
repo_persist_segment_sink   (FileSegment *seg, const char* prop_name)
{
  FileSegment* pseg = file_segment_internal (seg->seg_file, path_append (seg->seg_name, prop_name));

  return segment_sink (pseg, NULL, NULL, NULL, NULL);
}

gboolean
repo_persist_segment_test  (FileSegment *seg, const char* prop_name)
{
  FileSegment* pseg = file_segment_internal (seg->seg_file, path_append (seg->seg_name, prop_name));

  return segment_is_type_noerr (pseg, SV_Regular);
}

gboolean
repo_persist_segment_unset  (FileSegment *seg, const char* prop_name)
{
  FileSegment* pseg = file_segment_internal (seg->seg_file, path_append (seg->seg_name, prop_name));

  return segment_erase (pseg, FALSE);
}

FileSegment*
repo_persist_dir_segment (File* dir, const char* prop_name)
{
  File *file;

  if (file_is_not_type (dir, FV_Directory))
    return NULL;

  file = file_initialize (dir->repo, path_append (dir->driver_file->driver_path, prop_name));

  return file_default_segment (file);
}

SerialSource*
repo_persist_dir_source (File *dir, const char* prop_name)
{
  FileSegment *pseg;

  if (! (pseg = repo_persist_dir_segment (dir, prop_name)))
    return NULL;

  return segment_source (pseg);
}

SerialSink*
repo_persist_dir_sink   (File *dir, const char* prop_name)
{
  FileSegment* pseg;

  if (! (pseg = repo_persist_dir_segment (dir, prop_name)))
    return NULL;

  return segment_sink (pseg, NULL, NULL, NULL, NULL);
}

gboolean
repo_persist_dir_test  (File *dir, const char* prop_name)
{
  FileSegment* pseg;

  if (! (pseg = repo_persist_dir_segment (dir, prop_name)))
    return NULL;

  return segment_is_type_noerr (pseg, SV_Regular);
}

gboolean
repo_persist_dir_unset  (File *dir, const char* prop_name)
{
  FileSegment* pseg;

  if (! (pseg = repo_persist_dir_segment (dir, prop_name)))
    return NULL;

  return segment_erase (pseg, FALSE);
}

GHashTable** edsio_segment_property_table (FileSegment *seg) { return & seg->_edsio_property_table; }
GHashTable** edsio_file_property_table (File *file)          { return & file->driver_file->_edsio_property_table; }
GHashTable** edsio_dir_property_table (File *file)           { return & file->driver_file->_edsio_property_table; }
GHashTable** edsio_repo_property_table (Repository *repo)    { return & repo->_edsio_property_table; }

gboolean
file_to_stdout (File* file)
{
  FileHandle* fh;

  if (! (fh = file_open (file, HV_Read)))
    return FALSE;

  if (! handle_copy (fh, _stdout_handle, 0, handle_length (fh)))
    return FALSE;

  handle_close (fh);

  return TRUE;
}

FileHandle*
FH (RepoFileHandle* fh)
{
  return (FileHandle*) fh;
}

RepoFileHandle*
RFH (FileHandle* fh)
{
  return (RepoFileHandle*) fh;
}

#define NAME_COLS 30

static const char*
format_basename (Path *p)
{
  static const char* last_str;
  const char* str;
  static GString* last_fmt_str = NULL;

  if (! last_fmt_str)
    last_fmt_str = g_string_new (NULL);

  if (p == NULL)
    {
      last_str = NULL;
      return NULL;
    }

  if (p == path_root ())
    str = ".";
  else
    str = path_basename (p);

  if (last_str && strcmp (last_str, str) == 0)
    {
      return "";
    }

  g_string_truncate (last_fmt_str, 0);

  g_string_append (last_fmt_str, str);

  if (last_fmt_str->len < (NAME_COLS - 2))
    {
      g_string_append_c (last_fmt_str, ' ');

      while (last_fmt_str->len < NAME_COLS)
	g_string_append_c (last_fmt_str, '.');
    }

  last_str = str;

  return last_fmt_str->str;
}

#define LH _stdout_handle

/* @@@ YOU ARE HERE: Make it format the list_file stuff a little
 * better with "file ...." and eliminate repetition.
 */

static gboolean
print_sequence (File* f)
{
  Path *p = file_access_path (f);

  if (! handle_printf (LH, "s %10s %-30s %15s <Value: %d>\n", "-", format_basename (p), "", file_sequence_value (f)))
    return FALSE;

  return TRUE;
}

static gboolean
print_directory (File* f)
{
  Path *p = file_access_path (f);

  if (! handle_printf (LH, "d %10s %-30s\n", "-", format_basename (p)))
    return FALSE;

  return TRUE;
}

static gboolean
print_notpresent (File* f)
{
  Path *p = file_access_path (f);

  if (! handle_printf (LH, "n %10s %-30s\n", "-", format_basename (p)))
    return FALSE;

  return TRUE;
}

static gboolean
print_regular (FileSegment* seg, gint flags)
{
  Path* p = file_access_path (segment_file (seg));
  Path* n = segment_name (seg);
  gboolean found_type = FALSE;
  void* object = NULL;
  SerialType type = 0;

  if (segment_is_type_noerr (seg, SV_NotPresent))
    return TRUE;

  if (flags & RLF_HideSegments &&
      n != path_root ())
    return TRUE;

  if (flags & RLF_HideAutoSegments &&
      segment_is_auto (seg))
    return TRUE;

  if (flags & RLF_HideSystemSegments)
    {
      if (n == path_absolute ("Triggers"))
	return TRUE;

      if (n == path_absolute ("Auto"))
	return TRUE;

      if (n == path_absolute ("Ilinks"))
	return TRUE;
    }

  if (! handle_printf (LH, "r %10d %-30s %-15s", segment_length (seg), format_basename (p), pts (n)))
    return FALSE;

  switch (segment_type (seg))
    {
    case SV_Regular:

      if (flags & (RLF_TestSerial | RLF_ShowSerial))
	{
	  SerialSource* source;

	  if (! (source = segment_source (seg)))
	    return FALSE;

	  if (serializeio_unserialize_generic_noerr (source,
						     & type,
						     & object) &&
	      source->source_close (source))
	    {
	      found_type = TRUE;

	      if ((flags & RLF_TestSerial) &&
		  ! handle_printf (LH, " <Type: %s>", serializeio_generic_type_to_string (type)))
		return FALSE;
	    }

	  source->source_free (source);
	}

      if (flags & RLF_ShowSegmentHead && ! found_type)
	{
#define SEG_HEAD_SIZE 8
	  FileHandle *fh;
	  guint8 buf[SEG_HEAD_SIZE];
	  int i, c;

	  if (! (fh = segment_open (seg, HV_Read)))
	    return FALSE;

	  if ((c = handle_read (fh, buf, SEG_HEAD_SIZE)) < 0)
	    return FALSE;

	  if (! handle_close (fh))
	    return FALSE;

	  if (! handle_printf (LH, " <Head: \""))
	    return FALSE;

	  for (i = 0; i < c; i += 1)
	    {
	      guint8 c = buf[i];

	      switch (c)
		{
		case '\n': if (! handle_printf (LH, "\\n")) return FALSE; break;
		case '\t': if (! handle_printf (LH, "\\t")) return FALSE; break;
		case '\v': if (! handle_printf (LH, "\\v")) return FALSE; break;
		case '\r': if (! handle_printf (LH, "\\r")) return FALSE; break;
		case '\f': if (! handle_printf (LH, "\\f")) return FALSE; break;

		default:
		  if (isprint (c))
		    {
		      if (! handle_putc (LH, c))
			return FALSE;
		    }
		  else
		    {
		      if (! handle_printf (LH, "\\%03o", c))
			return FALSE;
		    }
		  break;
		}
	    }

	  if (! handle_printf (LH, "\">"))
	    return FALSE;
	}

      break;
    case SV_View:
      if (! handle_printf (LH, " <View: %s>", segment_view_name (seg)))
	return FALSE;
      break;
    case SV_Invalid:
    case SV_NotPresent:
      abort ();
    }

  if (found_type && flags & RLF_ShowSerial)
    {
      if (! handle_printf (LH, " "))
	return FALSE;

      serializeio_generic_print (type, object, 2);
    }
  else
    {
      if (! handle_printf (LH, "\n"))
	return FALSE;
    }

  return TRUE;
}

static gboolean
print_symlink (File* f)
{
  if (! handle_printf (LH, "l %10s %-30s %-15s <Link: %s>\n",
		       "-",
		       format_basename (file_access_path (f)),
		       "",
		       file_link (f)))
     return FALSE;

    return TRUE;
}

static gboolean
print_index (File* f)
{
  if (! handle_printf (LH, "i %10s %-30s %-15s <Index: %s>\n",
		       "-",
		       format_basename (file_access_path (f)),
		       "",
		       pts (file_index_seg_name (f))))
     return FALSE;

  return TRUE;
}

gboolean
repository_list_file (File* f, gint flags)
{
  FileSegment *segs;
  FileSegment *ds = file_default_segment (f);
  gboolean recursive = !(flags & RLF_NotRecursive);
  gboolean directory_recurse = (flags & RLF_Recursive);

  switch (file_type (f))
    {
    case FV_Invalid:
      abort ();
    case FV_NotPresent:
      if (! print_notpresent (f))
	return FALSE;
      break;
    case FV_Sequence:
      if (! print_sequence (f))
	return FALSE;
      break;
    case FV_Regular:
      if (! print_regular (ds, flags))
	return FALSE;
      break;
    case FV_SymbolicLink:
      if (! print_symlink (f))
	return FALSE;
      break;
    case FV_Index:
      if (! print_index (f))
	return FALSE;
      break;
    case FV_Directory:
      if (recursive)
	{
	  DirectoryHandle *dh;
	  Path *p;
	  GSList *ds = NULL, *dsp;

	  if (! (dh = file_opendir (f)))
	    return FALSE;

	  while ((p = dh_next (dh)))
	    {
	      File *e = file_initialize_link (f->repo, p);

	      switch (e->driver_file->type)
		{
		case FV_Directory:
		case FV_Index:

		  if (directory_recurse)
		    ds = g_slist_prepend (ds, e);
		  break;
		case FV_NotPresent:
		  continue;
		default:
		  break;
		}

	      if (! repository_list_file (e, flags | RLF_NotRecursive))
		return FALSE;
	    }

	  if (! dh_close (dh))
	    return FALSE;

	  ds = g_slist_reverse (ds);

	  for (dsp = ds; dsp; dsp = dsp->next)
	    {
	      File *e = dsp->data;

	      switch (e->driver_file->type)
		{
		case FV_Directory:
		  if (! handle_printf (LH, "Directory %s:\n", path_to_string (f->repo, e->access_path)))
		    return FALSE;

		  if (! repository_list_file (e, flags))
		    return FALSE;

		  break;
		case FV_Index:
		  {
		    DirectoryHandle *dh;
		    Path *p;

		    if (! handle_printf (LH,
					 "%s: %s on %s:\n",
					 file_index_type_to_string (file_index_type (e)),
					 path_to_string (e->repo, e->access_path),
					 path_to_string_simple (file_index_seg_name (e), '/')))
		      return FALSE;

		    if (! (dh = file_index_iterate (e, 0)))
		      return FALSE;

		    while ((p = dh_next (dh)))
		      {
			const guint8* key;
			guint key_len;
			int i;

			if (! dh_index_key (dh, &key, &key_len))
			  return FALSE;

			if (! handle_printf (LH, "  "))
			  return FALSE;

			for (i = 0; i < key_len; i += 1)
			  {
			    if (! handle_printf (LH, "%02x", key[i]))
			      return FALSE;
			  }

			i *= 2;

			if (i < 30 && !handle_printf (LH, "%*s", 30 - i, ""))
			  return FALSE;

			if (path_dirname (p) == path_dirname (e->access_path))
			  {
			    if (! handle_printf (LH, " %s\n", path_basename (p)))
			      return FALSE;
			  }
			else
			  {
			    if (! handle_printf (LH, " %s\n", path_to_string (e->repo, p)))
			      return FALSE;
			  }
		      }

		    if (! dh_close (dh))
		      return FALSE;
		  }

		  break;
		default:
		  abort ();
		}
	    }

	  g_slist_free (ds);

	  return TRUE;
	}
      else
	{
	  if (! print_directory (f))
	    return FALSE;
	}
      break;
    }

  for (segs = file_segment_first (f);
       segs;
       segs = file_segment_next (segs))
    {
      if (segs != ds && ! print_regular (segs, flags))
	return FALSE;
    }

  return TRUE;
}

gboolean
repository_list (Repository *repo, gint flags)
{
  File *root;

  root = file_initialize (repo, path_root ());

  return repository_list_file (root, flags);
}
