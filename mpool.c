/* -*-Mode: C;-*-
 * $Id: mpool.c 1.32 Tue, 27 Apr 1999 22:35:57 -0700 jmacd $
 *
 * Copyright (C) 1997, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "repo.h"
#include "repopriv.h"

/*
 * File types for DB mpools.
 */
#define REPO_MPOOL_REAL 1
#define REPO_MPOOL_TEMP 2

/*#define DEBUG_PAGER*/

typedef struct _PageTuple     PageTuple;
typedef struct _TempPageTuple TempPageTuple;

struct _TempPageTuple {
  const void *mem;
  guint       ref_count;
};

struct _PageManager {
  DB_MPOOLFILE *mf;

  GHashTable *page_map;
  GHashTable *page_no_map;

  GArray     *mapped_pages;
  GArray     *free_pages;

  gint page_no_seq;

  gsize    page_size;
  gboolean needs_pgin;

  gboolean      (* page_in)  (RepoFileHandle *handle,
			      void       *pgaddr,
			      gssize      page_no,
			      gsize       in_len);
  gboolean      (* page_out) (RepoFileHandle *handle,
			      void       *pgaddr,
			      gssize      page_no,
			      gsize       out_len);

  void     (* release)    (PageManager *pager, gpointer unique);

  gboolean (* map_page)   (RepoFileHandle *fh, gpointer unique, gint pageno, guint onpage, const void **mem);
  gboolean (* unmap_page) (RepoFileHandle *fh, gpointer unique, gint pageno, guint onpage, const void **mem);
};

struct _PageTuple {
  RepoFileHandle *handle;
  gpointer    unique;
  gssize      page_no;
  gssize      mf_page_no;
  gssize      page_is_backed;
  void       *mem;
  gint        ref_count;
};

static DB_ENV          app_env;
static DB_MPOOL       *app_mpool;

static gint  mem_pool_page_tuple_equal (gconstpointer v, gconstpointer v2);
static guint mem_pool_page_tuple_hash  (gconstpointer v);
static gint  mem_pool_pgin             (db_pgno_t pgno, void *pgaddr, DBT *pgcookie);

static void     mem_pool_real_release    (PageManager* pager, gpointer unique);
static gboolean mem_pool_real_map_page   (RepoFileHandle *fh, gpointer unique, gint pageno, guint onpage, const void **mem);
static gboolean mem_pool_real_unmap_page (RepoFileHandle *fh, gpointer unique, gint pageno, guint onpage, const void **mem);

static void     mem_pool_temp_release    (PageManager* pager, gpointer unique);
static gboolean mem_pool_temp_map_page   (RepoFileHandle *fh, gpointer unique, gint pageno, guint onpage, const void **mem);
static gboolean mem_pool_temp_unmap_page (RepoFileHandle *fh, gpointer unique, gint pageno, guint onpage, const void **mem);

gsize
mem_pool_page_size (PageManager *pager)
{
  return pager->page_size;
}

gint
mem_pool_page_tuple_equal (gconstpointer v, gconstpointer v2)
{
  const PageTuple *p1 = (const PageTuple*) v;
  const PageTuple *p2 = (const PageTuple*) v2;

  return p1->unique == p2->unique && p1->page_no == p2->page_no;
}

guint
mem_pool_page_tuple_hash (gconstpointer v)
{
  const PageTuple *p1 = (const PageTuple*) v;

  return (guint) (p1->unique + p1->page_no);
}

gboolean
mem_pool_init ()
{
  gint res;

  app_env.db_errfile = stderr;
  app_env.db_errpfx = "dberr";
  app_env.db_verbose = TRUE;
  app_env.mp_size = 1<<20;

  res = db_appinit ("/tmp/db", NULL, db_env(), DB_CREATE | DB_INIT_MPOOL | DB_MPOOL_PRIVATE);

  if (res != 0)
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "db_appinit");
      app_mpool = NULL;
      return FALSE;
    }

  app_mpool = app_env.mp_info;

  /* No pgout function is used because it is a temporary mpoolfile.  */
  if (memp_register (app_mpool, REPO_MPOOL_REAL, mem_pool_pgin , 0))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "memp_register");
      return FALSE;
    }

  if (memp_register (app_mpool, REPO_MPOOL_TEMP, 0, 0))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "memp_register");
      return FALSE;
    }

  return TRUE;
}

DB_ENV*
db_env ()
{
  return &app_env;
}

DB_ENV*
db_env_txn (const char* dir)
{
  DB_ENV *env = g_new0 (DB_ENV, 1);
  gint res;

  env->db_errfile = stderr;
  env->db_errpfx = "dberr";
  env->db_verbose = TRUE;
  env->mp_info = app_mpool;
  /*env->mp_size = 1<<23;*/

  res = db_appinit (dir,
		    NULL,
		    env,
		    DB_CREATE |
		    DB_INIT_LOCK |
		    DB_INIT_TXN |
		    DB_INIT_LOG |
		    DB_MPOOL_PRIVATE);

  if (res != 0)
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "db_appinit");
      return NULL;
    }

  return env;
}

gboolean
db_env_close  (DB_ENV* env)
{
  env->mp_info = NULL;

  if (db_appexit (env))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "db_appexit");
      return FALSE;
    }

  return TRUE;
}

gssize
mem_pool_map_page (FileHandle *fh, guint pgno, const guint8** mem)
{
  RepoFileHandle* rfh = (RepoFileHandle*) fh;

  gssize res = file_position_rem_on_page_no (& fh->fh_file_len, pgno);

  g_assert (res >= 0);

  if (res > 0 &&
      ! rfh->pager->map_page (rfh,
			      fh,
			      pgno,
			      res,
			      (const void**) mem))
    return -1;

  return res;
}

gboolean
mem_pool_unmap_page (FileHandle *fh, guint pgno, const guint8** mem)
{
  RepoFileHandle* rfh = (RepoFileHandle*) fh;
  gboolean res;
  gssize rem = file_position_rem_on_page_no (& fh->fh_file_len, pgno);

  res = (* rfh->pager->unmap_page) (rfh, fh, pgno, rem, (const void**) mem);

  if (res) *mem = NULL;

  return res;
}

void
mem_pool_release (PageManager *pager, gpointer unique)
{
  return (* pager->release) (pager, unique);
}

void
mem_pool_pager_free (PageManager* pm)
{
  memp_fclose (pm->mf);

#ifdef DEBUG_PAGER
  g_print ("*** page free: %p \n", pm);
#endif

  if (pm->page_map) g_hash_table_destroy (pm->page_map);
  if (pm->page_no_map) g_hash_table_destroy (pm->page_no_map);
  if (pm->mapped_pages) g_array_free (pm->mapped_pages, TRUE);

  g_free (pm);
}

gint
mem_pool_pgin (db_pgno_t pgno, void *pgaddr, DBT *pgcookie)
{
  PageManager *pager = * ((PageManager**) (pgcookie->data));

  pager->needs_pgin = TRUE;

  return 0;
}

/* These two functions grant and revoke page numbers from the
 * temp file.  initially, they are allocated from the sequence
 * number page_no_seq, but they are returned into the array
 * pager->free_pages.
 */
gssize
mem_pool_next_page (PageManager* pager)
{
  if (pager->free_pages && pager->free_pages->len > 0)
    {
      gint x = g_array_index (pager->free_pages, gint, pager->free_pages->len - 1);

      g_array_set_size (pager->free_pages, pager->free_pages->len - 1);

      return x;
    }

  return pager->page_no_seq ++;
}

void
mem_pool_release_page (PageManager* pager, gssize page_no)
{
  if (! pager->free_pages)
    pager->free_pages = g_array_new (FALSE, FALSE, sizeof (gint));

  g_hash_table_remove (pager->page_no_map, &page_no);
  g_array_append_val  (pager->free_pages, page_no);
}

/* Real pager.
 */

PageManager*
mem_pool_pager_new (gsize        page_size,
		    PageInFunc  *page_in,
		    PageOutFunc *page_out)
{
  PageManager *pager = g_new0 (PageManager, 1);
  DBT cookie;
  DB_MPOOL_FINFO mpinfo;

  cookie.data = &pager;
  cookie.size = sizeof (PageManager*);

  pager->page_size  = page_size;
  pager->page_in    = page_in;
  pager->page_out   = page_out;
  pager->release    = mem_pool_real_release;
  pager->map_page   = mem_pool_real_map_page;
  pager->unmap_page = mem_pool_real_unmap_page;

  mpinfo.ftype = REPO_MPOOL_REAL;
  mpinfo.pgcookie = &cookie;
  mpinfo.lsn_offset = -1;
  mpinfo.clear_len = 1;

  if (memp_fopen (app_mpool,
		  NULL,
		  0,
		  0,
		  page_size,
		  &mpinfo,
		  &pager->mf))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "memp_fopen");
      g_free (pager);
      return NULL;
    }

  pager->page_map = g_hash_table_new (mem_pool_page_tuple_hash, mem_pool_page_tuple_equal);
  pager->page_no_map = g_hash_table_new (g_int_hash, g_int_equal);

  return pager;
}

static gboolean
mem_pool_real_map_page (RepoFileHandle *fh, gpointer unique, gint pageno, guint onpage, const void **mem)
{
  PageManager *pager = fh->pager;
  PageTuple tup;
  PageTuple *map;

#ifdef DEBUG_PAGER
  g_print ("*** map page:   %p %p %d\n", fh->pager, fh, pageno);
#endif

  tup.unique  = unique;
  tup.page_no = pageno;

  map = g_hash_table_lookup (pager->page_map, &tup);

  if (map)
    {
      if (map->mem)
	{
	  map->ref_count += 1;

	  *mem = map->mem;

	  return TRUE;
	}

      /* otherwise, the mapping still exists but it is not pinned. */
      g_assert (map->ref_count == 0);

      map->handle = fh;
      map->ref_count = 1;
      pager->needs_pgin = FALSE;
    }
  else
    {
      map = g_new0 (PageTuple, 1);

      map->handle = fh;
      map->ref_count = 1;
      pager->needs_pgin = TRUE;

      map->unique = unique;
      map->page_no = pageno;
      map->mf_page_no = mem_pool_next_page (pager);

      if (fh->handle.fh_open_mode == HV_Read)
	map->page_is_backed = file_position_rem_on_page_no (&fh->handle.fh_file_len, pageno);
      else
	map->page_is_backed = 0;

      g_hash_table_insert (pager->page_map, map, map);
      g_hash_table_insert (pager->page_no_map, &map->mf_page_no, map);
    }

  if (memp_fget (pager->mf,
		 &map->mf_page_no,
		 DB_MPOOL_CREATE,
		 mem))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "memp_fget");
      return FALSE;
    }

  if (pager->needs_pgin && map->page_is_backed > 0)
    {
      if ((! (* pager->page_in) (map->handle, (void*) *mem, map->page_no, map->page_is_backed)))
	return FALSE;
    }

  map->mem = (void*) (*mem);

  return TRUE;
}

static gboolean
mem_pool_real_unmap_page (RepoFileHandle *fh, gpointer unique, gint pageno, guint onpage, const void** mem0_ptr)
{
  PageManager *pager = fh->pager;
  PageTuple tup;
  PageTuple *map;

#ifdef DEBUG_PAGER
  g_print ("*** unmap page: %p %p %d\n", fh->pager, fh, pageno);
#endif

  tup.unique  = unique;
  tup.page_no = pageno;

  map = g_hash_table_lookup (pager->page_map, &tup);

  g_assert (map);

  g_assert (map->mem == (*mem0_ptr));

  /* if replacing, write through, set page_is_backed true */
  if (fh->handle.fh_open_flags & HV_Replace)
    {
      map->page_is_backed = onpage;

      if (! (* pager->page_out) (map->handle, map->mem, map->page_no, onpage))
	return FALSE;
    }

  map->ref_count -= 1;

  if (map->ref_count)
    return TRUE;

  /* okay, what about the page tuple?  -- the mapping stays even though
   * the file is unreferenced.  This allows MPOOL to keep the page in
   * memory in case it is opened soon.  The release function actually
   * purges it. */

  if (memp_fput (pager->mf, map->mem, DB_MPOOL_CLEAN))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "memp_fput");
      return FALSE;
    }

  map->mem = NULL;

  return TRUE;
}

typedef struct {
  PageManager* pager;
  gpointer     unique;
} ReleaseData;

static gboolean
mem_pool_real_release_entry (gpointer  key,
			     gpointer  value,
			     gpointer  user_data)
{
  ReleaseData* rd = (ReleaseData*) user_data;
  PageTuple* map = (PageTuple*) value;
  PageManager* pager = rd->pager;
  gpointer unique = rd->unique;

  if (map->unique == unique)
    {
      mem_pool_release_page (pager, map->mf_page_no);

      g_free (map);

      return TRUE;
    }

  return FALSE;
}

static void
mem_pool_real_release (PageManager* pager, gpointer unique)
{
  ReleaseData rd;

  rd.unique = unique;
  rd.pager = pager;

  g_hash_table_foreach_remove (pager->page_map, mem_pool_real_release_entry, &rd);
}

/* Temp pager
 */

PageManager*
mem_pool_temp_pager_new (void)
{
  PageManager *pager = g_new0 (PageManager, 1);
  DBT cookie;
  DB_MPOOL_FINFO mpinfo;

  cookie.data = &pager;
  cookie.size = sizeof (PageManager*);

  pager->page_size  = TEMP_PAGE_SIZE;
  pager->page_in    = NULL;
  pager->page_out   = NULL;
  pager->release    = mem_pool_temp_release;
  pager->map_page   = mem_pool_temp_map_page;
  pager->unmap_page = mem_pool_temp_unmap_page;

  mpinfo.ftype = REPO_MPOOL_TEMP;
  mpinfo.pgcookie = &cookie;
  mpinfo.lsn_offset = -1;
  mpinfo.clear_len = 1;

  if (memp_fopen (app_mpool,
		  NULL,
		  0,
		  0,
		  TEMP_PAGE_SIZE,
		  &mpinfo,
		  &pager->mf))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "memp_fopen");
      g_free (pager);
      return NULL;
    }

  pager->mapped_pages = g_array_new (FALSE, TRUE, sizeof (TempPageTuple));

  return pager;
}

void
mem_pool_temp_release (PageManager* pager, gpointer unique)
{
}

gboolean
mem_pool_temp_map_page (RepoFileHandle *fh, gpointer unique, gint pageno, guint onpage, const void **mem)
{
  TempPageTuple *tpt;

#ifdef DEBUG_PAGER
  g_print ("*** map page:   %p %p %d\n", fh->pager, fh, pageno);
#endif

  if (fh->pager->mapped_pages->len <= pageno)
    g_array_set_size (fh->pager->mapped_pages, pageno+1);

  tpt = &g_array_index (fh->pager->mapped_pages, TempPageTuple, pageno);

  if (tpt->mem)
    {
      *mem = tpt->mem;
      tpt->ref_count += 1;
      return TRUE;
    }

  if (memp_fget (fh->pager->mf,
		 &pageno,
		 DB_MPOOL_CREATE,
		 mem))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "memp_fget");
      return FALSE;
    }

  tpt->mem = *mem;
  tpt->ref_count = 1;

  return TRUE;
}

gboolean
mem_pool_temp_unmap_page (RepoFileHandle *fh, gpointer unique, gint pageno, guint onpage, const void** mem0_ptr)
{
  TempPageTuple *tpt;

#ifdef DEBUG_PAGER
  g_print ("*** unmap page: %p %p %d\n", fh->pager, fh, pageno);
#endif

  if (fh->pager->mapped_pages->len <= pageno)
    g_array_set_size (fh->pager->mapped_pages, pageno+1);

  tpt = &g_array_index (fh->pager->mapped_pages, TempPageTuple, pageno);

  g_assert (tpt->mem == (*mem0_ptr));

  (*mem0_ptr) = NULL;

  tpt->ref_count -= 1;

  if (tpt->ref_count > 0)
    return TRUE;

  if (memp_fput (fh->pager->mf, (void*) tpt->mem, DB_MPOOL_DIRTY))
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "memp_fput");
      return FALSE;
    }

  tpt->mem = NULL;

  return TRUE;
}
