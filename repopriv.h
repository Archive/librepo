/* -*-Mode: C;-*-
 * $Id: repopriv.h 1.103 Sun, 02 May 1999 04:53:40 -0700 jmacd $
 *
 * Copyright (C) 1997, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#ifndef _REPOPRIV_H_
#define _REPOPRIV_H_

typedef struct _SerialDBSegment     DBSegment;
typedef struct _SerialDBLink        DBLink;
typedef struct _SerialDBCollection  DBCollection;

typedef struct _SerialTrigger Trigger;
typedef struct _SerialTriggers Triggers;
typedef struct _RepositoryDriver RepositoryDriver;
typedef struct _SimpleDriver SimpleDriver;
typedef struct _RepoFileHandle RepoFileHandle;
typedef struct _SerialKeyIterator KeyIterator;
typedef struct _SerialIlinks Ilinks;
typedef struct _SerialAutoSegments AutoSegments;
typedef struct _SerialAutoSegment  AutoSegment;

#include "config.h"

#include <zlib.h>

#ifdef HAVE_DB2_INCLUDE_DIR
#include <db2/db.h>
#else
#include <db.h>
#endif

#include "repo.h"
#include "mpool.h"
#include "repo_edsio.h"

/**********************************************************************/
/*			        Types                                 */
/**********************************************************************/

struct _SimpleDriver {
  /* Handle */
  gboolean    (*will_handle)  (const char* url, RepositoryDriver* driver);

  /* Path ops */

  Path*       (* canonicalize)      (Repository* repo, const char* url);
  Path*       (* canonicalize_long) (Repository *repo,
				     Path       *relative,
				     const char *native_filename);
  const char* (* path_to_string)    (Repository* repo, Path* path);

  /* Repository ops */

  Repository* (* initialize) (const char* url, RepositoryDriver* driver);
  Repository* (* create)     (const char* url, RepositoryDriver* driver);
  gboolean    (* exists)     (const char* url, RepositoryDriver* driver);
  gboolean    (* close)      (Repository *repo);

  /* Low Level: queries */
  DriverFile*      (* lstat)         (Repository* repo, DriverFile* parent, Path* driver_path);
  gboolean         (* segments_read) (DriverFile* dfile);
  FileHandle*      (* sequence_open) (FileSegment* seg);

  DirectoryHandle* (* cont_open)     (DriverFile* dfile, const char* prefix);

  /* Low Level: state changes */
  gboolean    (* make_symlink)     (DriverFile* file, const char* link);

#if 0
  gboolean    (* make_dir)         (DriverFile* dir);
  gboolean    (* make_index)       (DriverFile* file, Path *seg_name, ContainerType type);
  gboolean    (* make_sequence)    (DriverFile* file);
#endif

  gboolean    (* delete_symlink)   (DriverFile* file);

#if 0
  gboolean    (* delete_dir)       (DriverFile* file);
  gboolean    (* delete_index)     (DriverFile* file);
  gboolean    (* delete_sequence)  (DriverFile* file);
#endif

  /* Indexing functions */
  gboolean    (* index_insert_file) (DriverFile* index, DriverFile* file, const guint8* key, guint key_len);
  gboolean    (* index_delete_file) (DriverFile* index, DriverFile* file, const guint8* key, guint key_len);

#if 0
  DirectoryHandle* (* index_query) (DriverFile* index, const guint8* key, guint key_len, IndexQueryFlag flags);
  DirectoryHandle* (* index_iterate) (DriverFile* index, IndexQueryFlag flags);
#endif

  /* Low Level: transactions */
  gboolean    (* commit)           (Repository *repo);
  gboolean    (* rollback)         (Repository *repo);

  /* Properties and stamps. */
  gboolean    (* properties_restore) (DriverFile* file, const guint8* data, guint len);
  gboolean    (* unmodified_since)   (DriverFile* file, const guint8* data, guint len);

  /* Segment creation */
  FileHandle* (* open_read)          (FileSegment* seg, gint flags);
  FileHandle* (* open_replace)       (FileSegment* seg, gint flags);
  gboolean    (* make_view)          (FileSegment* seg, guint len, const char* view_name);
  gboolean    (* segment_to_view)    (FileSegment* seg, const char* view_name);

  /* Other ? */

  gboolean (* file_properties)   (DriverFile* dfile, guint8** arg, guint32* arg_len);
  gboolean (* file_mayread)      (DriverFile* dfile);
  gboolean (* file_maymodify)    (DriverFile* dfile);
};

struct _RepositoryDriver {

  /* Repository parameters. */
  gint                repo_sizeof;
  SimpleDriver        simp_driver;
  const char         *driver_name;

  /* Repository ops */

  gboolean    (*commit)       (Repository* repo);
  gboolean    (*rollback)     (Repository* repo);
  gboolean    (*close)        (Repository* repo);

  /* File ops */

  File*       (*file_init)         (Repository *repo, Path *path);
  File*       (*link_init)         (Repository *repo, Path *path);
  File*       (*temp_init)         (Repository *repo);
  gboolean    (*mkdir)             (File* file);
  gboolean    (*symlink)           (File* file, const char* path);

  gboolean    (*erase)             (File* file, gboolean recurse);

  gboolean    (*erase_segment)     (FileSegment* seg, gboolean force_all);

  FileHandle* (*open_file)         (FileSegment* seg, gint flags);
};

struct _Repository {
  GHashTable         *_edsio_property_table;

  /* The driver responsible for this repository implementation. */
  RepositoryDriver* driver;

  /* Relative path. */
  Path       *relative_path;

  /* Array of all (including temp) files. */
  GPtrArray     *all_files;

  /* Additional Properties */
  GHashTable *lstat_files;
  GHashTable *stat_files;
  GHashTable *driver_files;

  /* Link/File dependencies: this is an indexed set of relations. */
  GRelation* link_file_deps;

  gint        temp_count;
  DriverFile *invalid_driver_file;

  GHashTable *open_directory_handles;
  /*GHashTable *open_file_handles;*/

  gboolean    supports_segments;
};

struct _FileSegment {
  Repository                   *repo;

  GHashTable                   *_edsio_property_table;

  DriverFile                   *seg_file;
  Path                         *seg_name;
  gboolean                      seg_is_default;

  RepoSegmentTypePropertyValue  seg_type;

  /* List of segments for the seg_file. */
  FileSegment *next;
};

/* A File is unique within a repository for each distinct pair
 * (access_path x is_stat).  Temporary files are unique, and have
 * (is_stat = TRUE) and (access_path = /tmp/SOMETHINGUNIQUE).
 */
struct _File {
  Repository *repo;

  gboolean    is_stat;
  Path       *access_path;
  DriverFile *driver_file;
};

/* A DriverFile is unique for each dest_path.
 */
struct _DriverFile {

  Repository *repo;
  RepoFileTypePropertyValue type;

  GHashTable *_edsio_property_table;

  FileSegment *segment_list;
  Path        *driver_path;
  guint        is_temp : 1;
  guint        segs_read : 1;

  /* Functions */
  FileSegment*      (* segment_stat)      (DriverFile* file, Path* name);

  gboolean          (* segment_unlink)    (FileSegment* seg);
  guint             (* segment_length)    (FileSegment* seg);
  const char*       (* segment_view_name) (FileSegment* seg);

  const char*       (* driver_readlink)   (DriverFile* file);
  Path*             (* driver_segname)    (DriverFile* file);
  gint              (* driver_seqvalue)   (DriverFile* file);
  gint              (* driver_seqnext)    (DriverFile* file);
  ContainerType     (* driver_indextype)  (DriverFile* file);
};

struct _RepoFileHandle {
  FileHandle   handle;
  PageManager *pager;
};

struct _DirectoryHandle {
  Repository *repo;

  ContainerType type;

  guint  flags;
  guint  open_mode;

  guint  failure : 1;
  guint  done    : 1;

  gboolean         (* next)       (DirectoryHandle* dh, Path** next_ptr);
  gboolean         (* close)      (DirectoryHandle* dh);
  gboolean         (* index_key)  (DirectoryHandle* dh, const guint8** key, guint* key_len);
};

struct _SegmentView {
  const char        *name;
  SegmentViewPageIn *page_in;
};


/**********************************************************************/
/*			     File Handlers                            */
/**********************************************************************/

typedef gboolean (FsFileHandleClose) (FileHandle *fh, Path* current, void* data);

/* read/write */
FileHandle* fs_file_handle_new (const char        *native_name,
				Path              *path,
				guint              length,
				void              *data,
				FsFileHandleClose *close,
				gint               flags);

typedef gboolean (DbFileHandleClose) (FileHandle *fh,
				      void       *data,
				      gssize      len,
				      guint8     *immediate,
				      Path       *fs_path);

typedef const gchar* (FileHandleName) (void       *data);

/* write only */
FileHandle* db_file_handle_new (Path              *fs_base,
				KeyIterator       *ki,
				void              *data,
				DbFileHandleClose *close,
				FileHandleName    *name,
				gint               flags);

typedef gboolean (TempFileHandleClose) (FileHandle  *fh,
					void        *data,
					gssize       len,
					PageManager *pager);

/* read/write */
FileHandle* temp_file_handle_new (PageManager *pager, guint len, void* data, TempFileHandleClose *close, FileHandleName *name, gint flags);

/* Read only types */
FileHandle* view_file_handle_new (FileSegment  *seg, SegmentView  *view, gint flags);
FileHandle* mem_file_handle_new (const guint8* buf, const char* name, guint len, gint flags);

/**********************************************************************/
/*				 Misc                                 */
/**********************************************************************/

/* @@@ make the usage of this more randomized (add
 * position-independence for better hashing)? */
const char sorta_base64_table[64];

const guint8* key_iterate (KeyIterator* ki, int *key_len);

Path* db_generate_data_path (Path* fsbase, KeyIterator *ki);

const char* segment_handle_to_string (void* data);

gboolean db_object_rename (Repository* repo, DB* dbp, DB_TXN* txn, const char* key, const char* key2);
gboolean db_object_del (Repository* repo, DB* dbp, DB_TXN* txn, const char* key);

const char* db_get_name (DB *dbp, DB_TXN *txn);

gboolean segment_is_compressed_func (FileSegment* seg);

FileHandle* FH (RepoFileHandle* fh);

gboolean trigger_library_init (void);
gboolean driver_library_init (void);
gboolean sequence_library_init (void);
gboolean index_library_init (void);
gboolean db_repo_init (void);
gboolean fs_repo_init (void);
gboolean auto_library_init (void);

gboolean driver_set_segment_state (FileSegment *seg, FileHandle *fh, RepoSegmentTypePropertyValue nstate);
gboolean driver_set_file_state (DriverFile *file, RepoFileTypePropertyValue nstate);

gboolean trigger_execute (File* dir, File *target, Path* target_segment_name, FileHandle* target_handle, FileSegment* target_handle_seg, gint32 event_type);

File* driver_lstat (File* file_stat);

File* file_sequence_next_internal (DriverFile* seq);

DriverFile* driver_lstat_driver_path (Repository* repo, DriverFile* parent, Path* dpath);

void repository_register_driver (RepositoryDriver *driver);

RepositoryDriver* driver_new (gint                repo_sizeof,
			      SimpleDriver       *simp_driver,
			      const char         *name);

Repository* repository_new (RepositoryDriver* driver);

Path* path_intern (const char* name, int segs);

GString* temporary_string (const char* str);

void clear_dbts (DBT* one, DBT* two);
void clear_dbt (DBT* one);
void init_str_key (DBT* key, const char* str);

Path*       path_canonicalize_internal (Repository* repo, Path* relative, char sep, gboolean simple, const char* url0);

FileSegment* file_segment_internal (DriverFile* file, Path* name);

gboolean file_triggers (File* file, Triggers** ts);

gboolean driver_init (Repository* repo);

const char* pts (Path* p);

gboolean     trigger_type_register_internal (gint32           code,
					     const char       *code_name,
					     DirectoryTrigger *trigger_func);

gboolean file_index_delete_triggers (File *index);

gboolean trigger_delete_internal (Repository       *repo,
				  File             *dir,
				  gboolean          deliver,
				  gint32            trigger_id,
				  gint32            parent_id);

gboolean auto_property_delete_trigger (File* dir, File* target, Path* seg_name, gint32 event_type);
gboolean auto_digest_verify (FileSegment* seg, FileHandle *fh);

File* df2f (DriverFile *df);
File* df2pf (DriverFile *df);

#define INDEX_TRIGGER_TYPE 2
#define AUTO_DIGEST_TRIGGER_TYPE 4
#define AUTO_DIGEST_AUTO_PROPERTY_CODE 1

#endif /* _REPOPRIV_H_ */
