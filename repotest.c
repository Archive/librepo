/* -*-Mode: C;-*-
 * $Id: repotest.c 1.21 Sun, 02 May 1999 04:53:40 -0700 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "repo.h"
#include <edsiostdio.h>

#define FILL_SIZE (1<<20)

Repository* repo;
guint8 fill_buffer[FILL_SIZE];

void
fail ()
{
  g_warning ("FAILURE\n");

  exit (1);
}

File*
empty_file ()
{
  static gint count = 0;
  char buf[10];

  sprintf (buf, "file%d", count++);

  return file_initialize (repo, path_canonicalize (repo, buf));
}

File*
fill_this_file (File* f, guint len, guint8* md5)
{

  FileHandle* fh;
  gint i;
  EdsioMD5Ctx ctx;

  g_assert (len <= FILL_SIZE);

  for (i = 0; i < len; i += 1)
    {
      if (i & (1<<9))
	fill_buffer[i] = i&0xff;
      else
	fill_buffer[i] = 255 - (i&0xff);
    }

  if (md5)
    {
      edsio_md5_init (&ctx);
      edsio_md5_update (&ctx, fill_buffer, len);
      edsio_md5_final (md5, &ctx);
    }

  fh = file_open (f, HV_Replace);

  if (! handle_write (fh, fill_buffer, len))
    fail ();

  if (! handle_close (fh))
    fail ();

  return f;
}

File*
fill_file (guint len, guint8* md5)
{
  File* f = empty_file ();

  return fill_this_file (f, len, md5);
}

void
fill_handle (FileHandle *fh, guint len)
{
  gint i;

  g_assert (len <= FILL_SIZE);

  for (i = 0; i < len; i += 1)
    {
      if (i & (1<<9))
	fill_buffer[i] = i&0xff;
      else
	fill_buffer[i] = 255 - (i&0xff);
    }

  if (! handle_write (fh, fill_buffer, len))
    fail ();

  if (! handle_close (fh))
    fail ();
}

void
compare_files (File* fromfile, File* tofile, gint fromflags, gint toflags)
{
  gint pos = 0;

  FileHandle* toh;
  FileHandle* fromh;

  guint8 buf1[1<<12], buf2[1<<12];

  toh = file_open (tofile, HV_Read | HV_NoSeek | toflags);
  fromh = file_open (fromfile, HV_Read | HV_NoSeek | fromflags);

  if (!toh || !fromh)
    fail ();

  for (;;)
    {
      gint readf = handle_read (fromh, buf1, 1<<12);
      gint readt = handle_read (toh, buf2, 1<<12);
      gint i, m = MIN(readf, readt);

      if (readf < 0 || readt < 0)
	fail ();

      for (i = 0; i < m; i += 1, pos += 1)
	{
	  if (buf1[i] != buf2[i])
	    fail ();
	}

      if (m != (1<<12))
	{
	  if (readt == readf)
	    {
	      handle_close (toh);
	      handle_close (fromh);
	      return;
	    }

	  fail ();
	}
    }
}

#define FUNC(i) ((7*(i)%13) % 256)

gboolean
test_view_page_in (FileSegment *seg,
		   SegmentView *view,
		   guint8      *pgbuf,
		   gssize       page_no,
		   gsize        in_len)
{
  gsize i;

  for (i = 0; i < in_len; i += 1)
    pgbuf[i] = FUNC(i + (page_no * SEGMENT_VIEW_PAGE_SIZE));

  return TRUE;
}

void
test_view_size (gint size)
{
  /* views */
  File* f1;
  SegmentView* view;
  FileHandle* fh;
  FileSegment* seg;
  gint i;

  /*g_print ("view test %d\n", size);*/

  f1 = empty_file ();

  view = segment_view_register ("Test View", test_view_page_in);

  seg = file_default_segment (f1);

  if (segment_is_not_type (seg, SV_NotPresent))
    fail ();

  if (! segment_view_create (seg, size, view))
    fail ();

  if (! (fh = file_open (f1, HV_Read)))
    fail ();

  for (i = 0; i < size; i += 1)
    {
      gint c = handle_getc (fh);

      if (c < 0)
	fail ();

      if (c != FUNC(i))
	fail ();
    }

  if (! handle_eof (fh))
    fail ();

  if (! handle_close (fh))
    fail ();

  if (segment_is_not_type (seg, SV_View))
    fail ();

  if (! segment_erase (file_default_segment (f1), FALSE))
    fail ();

  if (segment_is_not_type (seg, SV_NotPresent))
    fail ();
}

const char* strip3 (const char* str) { return str + 3; }

void
test_path_ops ()
{
  {
    /* path map, basenames ops */
    Path* p = path_absolute3 ("blah", "it", "what");
    Path* q = path_prepend_basenames (p, "hi_");
    Path* r = path_absolute3 ("hi_blah", "hi_it", "hi_what");

    Path* a = path_map (r, strip3);

    Path* x = path_absolute3 ("blah", "it", "what");
    Path* y = path_append_basenames (x, "_hi");
    Path* z = path_absolute3 ("blah_hi", "it_hi", "what_hi");

    g_assert (a == p);
    g_assert (q == r);
    g_assert (y == z);
  }

  {
    /* path suffix */
    Path* p1 = path_absolute4 ("a", "b", "c", "d");
    Path* p2 = path_absolute3 ("b", "c", "d");
    Path* p3 = path_suffix (p1, 3);

    g_assert (p2 == p3);
  }
}

void
test_file_size (gint size)
{
  File *f1 = fill_file (size, NULL);
  File *f2 = fill_file (size, NULL);

  /*g_print ("file test %d\n", size);*/

  compare_files (f1, f2, 0, 0);
}

void test_file_types ()
{
  /* file and segment types */

  Path* path = path_absolute ("test_file_types");

  File* fs = file_initialize      (repo, path);
  File* fl = file_initialize_link (repo, path);

  FileSegment *s1 = file_default_segment (fs);
  FileSegment *s2 = file_segment         (fs, path_absolute2 ("foo", "bar"));

  FileHandle* fh;

  if (file_is_not_type    (fs, FV_NotPresent)) fail ();
  if (file_is_not_type    (fl, FV_NotPresent)) fail ();
  if (segment_is_not_type (s1, SV_NotPresent)) fail ();
  if (segment_is_not_type (s2, SV_NotPresent)) fail ();

  if (! file_mkdir (fs)) fail ();

  if (file_is_not_type    (fs, FV_Directory))  fail ();
  if (file_is_not_type    (fl, FV_Directory))  fail ();
  if (segment_is_not_type (s1, SV_Invalid))    fail ();
  if (segment_is_not_type (s2, SV_NotPresent)) fail ();

  if (! file_erase (fs, FALSE)) fail ();

  if (file_is_not_type    (fs, FV_NotPresent)) fail ();
  if (file_is_not_type    (fl, FV_NotPresent)) fail ();
  if (segment_is_not_type (s1, SV_NotPresent)) fail ();
  if (segment_is_not_type (s2, SV_NotPresent)) fail ();

  if (! file_symlink (fs, "not_present")) fail ();

  if (strcmp (file_link (fl), "not_present") != 0) fail ();

  if (file_is_not_type    (fs, FV_NotPresent))   fail ();
  if (file_is_not_type    (fl, FV_SymbolicLink)) fail ();
  if (segment_is_not_type (s1, SV_Invalid))      fail ();
  if (segment_is_not_type (s2, SV_NotPresent))   fail ();

  if (! file_erase (fs, FALSE)) fail ();

  if (file_is_not_type    (fs, FV_NotPresent)) fail ();
  if (file_is_not_type    (fl, FV_NotPresent)) fail ();
  if (segment_is_not_type (s1, SV_NotPresent)) fail ();
  if (segment_is_not_type (s2, SV_NotPresent)) fail ();

  if (! (fh = file_open (fs, HV_Replace))) fail ();

  if (! handle_printf (fh, "short stupid file")) fail ();

  if (file_is_not_type    (fs, FV_NotPresent)) fail ();
  if (file_is_not_type    (fl, FV_NotPresent)) fail ();
  if (segment_is_not_type (s1, SV_NotPresent)) fail ();
  if (segment_is_not_type (s2, SV_NotPresent)) fail ();

  if (! handle_close (fh)) fail ();

  if (! (fh = segment_open (s2, HV_Replace))) fail ();

  if (! handle_printf (fh, "short stupid foo/bar segment")) fail ();

  if (file_is_not_type    (fs, FV_Regular)) fail ();
  if (file_is_not_type    (fl, FV_Regular)) fail ();
  if (segment_is_not_type (s1, SV_Regular))     fail ();
  if (segment_is_not_type (s2, SV_NotPresent))     fail ();

  if (! handle_close (fh)) fail ();

  if (file_is_not_type    (fs, FV_Regular)) fail ();
  if (file_is_not_type    (fl, FV_Regular)) fail ();
  if (segment_is_not_type (s1, SV_Regular))     fail ();
  if (segment_is_not_type (s2, SV_Regular))     fail ();

  if (! file_erase (fs, FALSE)) fail ();

  if (file_is_not_type    (fs, FV_NotPresent)) fail ();
  if (file_is_not_type    (fl, FV_NotPresent)) fail ();
  if (segment_is_not_type (s1, SV_NotPresent)) fail ();
  if (segment_is_not_type (s2, SV_NotPresent)) fail ();
}

void
test_sequences ()
{
  File *seq;
  File *f1, *f2, *f3, *f4;

  seq = file_initialize (repo, path_absolute ("testing"));

  if (! file_sequence_create (seq)) fail ();

  f1 = file_initialize (repo, path_absolute ("testing:0"));
  f2 = file_initialize (repo, path_absolute ("testing:1"));

  fill_this_file (seq, 1587, NULL);
  fill_this_file (seq, 7845, NULL);

  f3 = fill_file (1587, NULL);
  f4 = fill_file (7845, NULL);

  compare_files (f1, f3, 0, 0);
  compare_files (f2, f4, 0, 0);

  if (file_is_not_type (f1, FV_Regular)) fail ();
  if (file_is_not_type (f2, FV_Regular)) fail ();
  if (file_length (f1) != 1587) fail ();
  if (file_length (f2) != 7845) fail ();
}

void
test_index (File* f, int count, int iterate_count)
{
  DirectoryHandle *dh;
  Path *ie;

  if (! (dh = file_index_query (f, "parity:0", strlen ("parity:0"), IQF_Duplicates))) fail ();

  while ((ie = dh_next (dh)))
    {
      guint32 x;

      if (! strtoui_checked (path_basename (ie), &x, "Test number")) fail ();

      g_assert (x%2 == 0);

      count -= 1;
    }

  g_assert (count == 0);

  if (! dh_close (dh)) fail ();

  if (! (dh = file_index_iterate (f, IQF_NoIndexKey))) fail ();

  while ((ie = dh_next (dh)))
    {
      iterate_count -= 1;
    }

  g_assert (iterate_count == 0);

  if (! dh_close (dh)) fail ();
}

Path* sn;

void
write_parity (File* e, int i)
{
  FileSegment *ea;
  FileHandle *fh;

  ea = file_segment (e, sn);

  if (! (fh = segment_open (ea, HV_Replace))) fail ();

  if (! handle_printf (fh, "parity:%d", i%2)) fail ();

  if (! handle_close (fh)) fail ();
}

void
test_indexing ()
{
  File* f = empty_file ();
  File* d = empty_file ();
  File* e;
  gint i;

  sn = path_absolute ("Attribute");

  if (! file_index_create (f, sn, IT_Hash)) fail ();
  if (! file_mkdir (d))                     fail ();

  if (file_is_not_type (f, FV_Index))     fail ();
  if (file_is_not_type (d, FV_Directory)) fail ();

  /* */

  for (i = 0; i < 11; i += 1)
    {
      Path* p = path_append_format (file_access_path (d), "%d", i);

      e = file_initialize (repo, p);

      write_parity (e, i);
    }

  /* */

  if (! file_index_link (f, d, FALSE))     fail ();

  /*repository_list (repo, RLF_Recursive | RLF_RecurseOne | RLF_ListIndices);*/

  test_index (f, 6, 11);

  if (! file_erase (e, FALSE)) fail ();

  test_index (f, 5, 10);

  write_parity (e, 10);

  test_index (f, 6, 11);

  if (! file_index_unlink (f, d, FALSE))   fail ();

  test_index (f, 0, 0);

  /* */

  if (! file_erase (f, TRUE))       fail ();
  if (! file_erase (d, TRUE))       fail ();

  if (file_is_not_type (f, FV_NotPresent)) fail ();
  if (file_is_not_type (d, FV_NotPresent)) fail ();
}

#define TEST_CODE 13

File *trig_dir;
gint32 trig_id;
int last_event_type;
int alive;
int tcount;

gboolean
test_trigger (gint32      code,
	      const char  *code_name,
	      File        *target_dir,
	      File        *target_file,
	      Path        *target_seg_name,
	      FileHandle  *target_handle,
	      const char  *trigger_argument,
	      gint32      trigger_id,
	      gint32      trigger_flags,
	      gint32      trigger_parent_id,
	      gint32      trigger_event_type)
{
  tcount += 1;

  g_assert (code == TEST_CODE);
  g_assert (strcmp (code_name, "test trigger") == 0);
  g_assert (target_dir == trig_dir);
  if (target_seg_name)
    g_assert (target_seg_name == path_root ());
  g_assert (! trigger_parent_id);
  g_assert (strcmp (trigger_argument, "test argument") == 0);
  if (target_file)
    g_assert (strcmp (path_basename (file_dest_path (target_file)), "test trigger target") == 0);

  g_assert (trigger_event_type > last_event_type);
  last_event_type = trigger_event_type;

  return TRUE;
}

void
touch_segment (FileSegment* seg)
{
  FileHandle *fh;

  if (! (fh = segment_open (seg, HV_Replace))) fail ();

  if (! handle_printf (fh, "this file has been touched")) fail ();

  if (! handle_close (fh)) fail ();
}

void
test_triggers ()
{
  Path *p;
  File *e, *f;
  FileSegment *es1, *es2, *fs1, *fs2;
  gboolean once = TRUE;

  trig_dir = empty_file ();

  if (! file_mkdir (trig_dir)) fail ();

  if (! trigger_type_register (TEST_CODE, "test trigger", test_trigger)) fail ();

  if ((trig_id = trigger_add (trig_dir, TEST_CODE, path_root (), TE_TriggerCreateDelete, TF_Recursive, "test argument")) < 0) fail ();

  /* now trigger some events */

  p = path_append (file_access_path (trig_dir), "test trigger target");

  e = file_initialize (repo, p);
  f = empty_file ();

 again:

  es1 = file_segment (e, path_root ());
  fs1 = file_segment (f, path_root ());

  es2 = file_segment (e, path_absolute ("test"));
  fs2 = file_segment (f, path_absolute ("test"));

  /* index */
  if (! file_index_create (e, path_absolute ("test"), IT_Hash)) fail ();
  if (! file_index_create (f, path_absolute ("test"), IT_Hash)) fail ();

  if (! file_erase (e, TRUE)) fail ();
  if (! file_erase (f, TRUE)) fail ();

  /* segment */
  touch_segment (es1);
  touch_segment (es2);
  touch_segment (fs1);
  touch_segment (fs2);

  if (! segment_erase (es1, TRUE)) fail ();
  if (! segment_erase (es2, TRUE)) fail ();
  if (! segment_erase (fs1, TRUE)) fail ();
  if (! segment_erase (fs2, TRUE)) fail ();

  /* dir */
  if (! file_mkdir (e)) fail ();
  if (! file_mkdir (f)) fail ();

  if (! file_erase (e, TRUE)) fail ();
  if (! file_erase (f, TRUE)) fail ();

  /* symlink */
  if (! file_symlink (e, "link")) fail ();
  if (! file_symlink (f, "link")) fail ();

  if (! file_erase (e, TRUE)) fail ();
  if (! file_erase (f, TRUE)) fail ();

  /* done */

  if (once)
    {
      if (! trigger_delete (repo, trig_dir, trig_id)) fail ();

      once = FALSE;

      goto again;
    }

  g_assert (alive == 0);
  g_assert (tcount == 8);
  g_assert (last_event_type == TE_SymlinkDelete);
}

#define TEST2_CODE 15

gint test2_count;

gboolean
test2_trigger (gint32       code,
	       const char  *code_name,
	       File        *target_dir,
	       File        *target_file,
	       Path        *target_seg_name,
	       FileHandle  *target_handle,
	       const char  *trigger_argument,
	       gint32       trigger_id,
	       gint32       trigger_flags,
	       gint32       trigger_parent_id,
	       gint32       trigger_event_type)
{
  test2_count += 1;
  return TRUE;
}

void
test_trigger_propagataion ()
{
  File* f1, *f2, *f3;
  File* d1, *d2, *d3;
  gint32 trig;

  if (! trigger_type_register (TEST2_CODE, "test2 trigger", test2_trigger)) fail ();

  if (! file_mkdir_p (repo, path_absolute2 ("ttp1", "ttp2"))) fail ();

  d1 = file_initialize (repo, path_absolute ("ttp1"));
  d2 = file_initialize (repo, path_absolute2 ("ttp1", "ttp2"));

  if ((trig = trigger_add (d1, TEST2_CODE, path_root (), TE_SegmentCreate, TF_Recursive, "test2 argument")) < 0) fail ();

  d3 = file_initialize (repo, path_absolute3 ("ttp1", "ttp2", "ttp3"));

  if (! file_mkdir (d3)) fail ();

  f1 = file_initialize (repo, path_absolute2 ("ttp1", "f"));
  f2 = file_initialize (repo, path_absolute3 ("ttp1", "ttp2", "f"));
  f3 = file_initialize (repo, path_absolute4 ("ttp1", "ttp2", "ttp3", "f"));

  touch_segment (file_default_segment (f1));
  touch_segment (file_default_segment (f2));
  touch_segment (file_default_segment (f3));

  g_assert (test2_count == 3);

  if (! file_erase (f1, TRUE)) fail ();
  if (! file_erase (f2, TRUE)) fail ();
  if (! file_erase (f3, TRUE)) fail ();

  if (! trigger_delete (repo, d1, trig)) fail ();

  touch_segment (file_default_segment (f1));
  touch_segment (file_default_segment (f2));
  touch_segment (file_default_segment (f3));

  g_assert (test2_count == 3);

  if (! file_erase (d1, TRUE)) fail ();
}

void
test_index_unlink (void)
{
  File* f = empty_file ();
  File* d = empty_file ();

  sn = path_absolute ("Attribute");

  if (! file_mkdir (d))             fail ();
  if (! file_index_create (f, sn, IT_Hash))  fail ();
  if (! file_index_link (f, d, FALSE))     fail ();
  if (! file_erase (f, TRUE))       fail ();

  if (! file_index_create (f, sn, IT_Hash))  fail ();
  if (! file_index_link (f, d, FALSE))     fail ();
  if (! file_erase (d, TRUE))       fail ();

  if (! file_erase (f, TRUE))       fail ();
}

const guint8*
digest_file_create (FileHandle* fh, const MessageDigest* md, guint len)
{
  void* ctx = g_malloc (md->ctx_size);
  guint8* res = g_malloc (md->cksum_len);
  int i;

  g_assert (len <= FILL_SIZE);

  for (i = 0; i < len; i += 1)
    fill_buffer[i] = i & 0xff;

  md->init (ctx);
  md->update (ctx, fill_buffer, len);
  md->final (res, ctx);

  if (! handle_write (fh, fill_buffer, len))
    fail ();

  return res;
}

#define NF 5

void
test_digest_indexing (const MessageDigest* md)
{
  File* dir = empty_file ();
  File* index;
  File* s;
  int i;

  if (! file_mkdir (dir)) fail ();

  index = file_initialize (repo, path_append (file_dest_path (dir), "index"));

  if (! file_index_create (index, path_absolute (md->name), IT_Hash)) fail ();

  if (! file_index_link (index, dir, FALSE)) fail ();

  s = file_initialize (repo, path_append (file_dest_path (dir), "seq"));

  if (! file_sequence_create (s)) fail ();

  if (! auto_digest_property (dir, path_root (), md, TRUE)) fail ();

  for (i = 1; i < NF; i += 1)
    {
      FileHandle *fh = file_open (s, HV_Replace);
      const guint8 *digest = digest_file_create (fh, md, i * 17);
      const guint8 *rdigest;
      Path *qpath;
      File *test;

      if (! file_index_lookup (index, digest, md->cksum_len, & qpath)) fail ();

      g_assert (qpath == NULL);

      if (! handle_close (fh)) fail ();

      if (! (rdigest = handle_digest (fh, md))) fail ();

      g_assert (memcmp (rdigest, digest, md->cksum_len) == 0);

      if (! file_index_lookup (index, digest, md->cksum_len, & qpath)) fail ();

      g_assert (qpath != NULL);

      test = file_initialize (repo, qpath);

      fh = file_open (test, HV_Read);

      if (! (rdigest = handle_digest (fh, md))) fail ();

      g_assert (memcmp (rdigest, digest, md->cksum_len) == 0);
    }
}

void
test_digests ()
{
  guint8 md5[16];
  const guint8* md52;

  File* f = fill_file (683, md5);
  FileSegment *fs;

  if (! (md52 = file_digest (f, edsio_message_digest_md5 ()))) fail ();

  g_assert (memcmp (md52, md5, 16) == 0);

  if (! (md52 = file_digest (f, edsio_message_digest_md5 ()))) fail ();

  g_assert (memcmp (md52, md5, 16) == 0);

  fs = file_segment (f, path_absolute ("MD5"));

  g_assert (segment_type (fs) == SV_Regular);

  if (! segment_erase (file_default_segment (f), TRUE)) fail ();

  g_assert (segment_type (fs) == SV_NotPresent);
}

static guint8 digest_assertion_buffer[FILL_SIZE];

gboolean
test_digest_assertion_page_in (FileSegment *seg,
			       SegmentView *view,
			       guint8      *pgbuf,
			       gssize       page_no,
			       gsize        in_len)
{
  memcpy (pgbuf, digest_assertion_buffer + page_no * SEGMENT_VIEW_PAGE_SIZE, in_len);
  return TRUE;
}

void
test_digest_assertions (guint size)
{
  SegmentView* view = segment_view_register ("TDA", test_digest_assertion_page_in);
  EdsioMD5Ctx ctx;
  guint8 md50[16];
  const guint8 *md5v;
  File* f = empty_file ();
  FileSegment *s = file_default_segment (f);
  const MessageDigest* md = edsio_message_digest_md5 ();
  int i;
  FileHandle* fh;

  for (i = 0; i < size; i += 1)
    digest_assertion_buffer[i] = lrand48() & 0xff;

  edsio_md5_init (&ctx);
  edsio_md5_update (&ctx, digest_assertion_buffer, size);
  edsio_md5_final (md50, &ctx);

  /* create */
  if (! segment_view_create (s, size, view)) fail ();

  /* assert */
  if (! segment_digest_assert (s, md, md50)) fail ();

  /* verify return of asserted value */
  if (! (md5v = segment_digest (s, md))) fail ();
  g_assert (memcmp (md50, md5v, 16) == 0);

  /* read the view, make sure it validates it */
  if (! (fh = segment_open (s, HV_Read))) fail ();
  if (! handle_close (fh)) fail ();

  /* again verify return of asserted value, should now be positive */
  if (! (md5v = segment_digest (s, md))) fail ();
  g_assert (memcmp (md50, md5v, 16) == 0);

  /* break it */
  memset (digest_assertion_buffer, 0, size);

  /* verify failure condition */
  if (! segment_erase (s, FALSE)) fail ();
  if (! segment_view_create (s, size+1, view)) fail ();
  if (! segment_digest_assert (s, md, md50)) fail ();

  /* verify return of asserted value */
  if (! (md5v = segment_digest (s, md))) fail ();
  g_assert (memcmp (md50, md5v, 16) == 0);

  /* read the view, make sure validation fails */
  if (! (fh = segment_open (s, HV_Read))) fail ();
  if (handle_close (fh)) fail ();
}

int
main (int argc, char** argv)
{
  gboolean nodel = FALSE;

  for (; argc > 1; argc -= 1, argv += 1)
    {
      if (strcmp (argv[1], "-n") == 0)
	nodel = TRUE;
      else
	g_print ("unrecognized argument: %s\n", argv[1]);
    }

  system ("rm -rf /tmp/repotest");

  repository_system_init ();

  repo = repository_create ("db:/tmp/repotest");

  if (repo == NULL)
    fail ();

  g_print ("testing path ops...\n");
  test_path_ops ();

  g_print ("testing file/segment types...\n");
  test_file_types ();

  g_print ("testing sequences...\n");
  test_sequences ();

  g_print ("testing triggers...\n");
  test_triggers ();

  g_print ("testing trigger propagataion...\n");
  test_trigger_propagataion ();

  g_print ("testing regular files...\n");

  test_file_size (0);
  test_file_size (17);

  test_file_size (FS_PAGE_SIZE - 17);
  test_file_size (FS_PAGE_SIZE);
  test_file_size (FS_PAGE_SIZE + 17);

  test_file_size (FS_PAGE_SIZE * 4 - 17);
  test_file_size (FS_PAGE_SIZE * 4);
  test_file_size (FS_PAGE_SIZE * 4 + 17);

  g_print ("testing views...\n");

  test_view_size (0);
  test_view_size (17);

  test_view_size (SEGMENT_VIEW_PAGE_SIZE - 17);
  test_view_size (SEGMENT_VIEW_PAGE_SIZE);
  test_view_size (SEGMENT_VIEW_PAGE_SIZE + 17);

  test_view_size (SEGMENT_VIEW_PAGE_SIZE * 4 - 17);
  test_view_size (SEGMENT_VIEW_PAGE_SIZE * 4);
  test_view_size (SEGMENT_VIEW_PAGE_SIZE * 4 + 17);

  g_print ("testing secondary indexing...\n");
  test_indexing ();

  g_print ("testing index unlink...\n");
  test_index_unlink ();

  g_print ("testing digests...\n");
  test_digests ();

  g_print ("testing digest assertions...\n");
  test_digest_assertions (0);
  test_digest_assertions (17);

  test_digest_assertions (SEGMENT_VIEW_PAGE_SIZE - 17);
  test_digest_assertions (SEGMENT_VIEW_PAGE_SIZE);
  test_digest_assertions (SEGMENT_VIEW_PAGE_SIZE + 17);

  test_digest_assertions (SEGMENT_VIEW_PAGE_SIZE * 4 - 17);
  test_digest_assertions (SEGMENT_VIEW_PAGE_SIZE * 4);
  test_digest_assertions (SEGMENT_VIEW_PAGE_SIZE * 4 + 17);

  g_print ("testing auto MD5 digest indexing...\n");
  test_digest_indexing (edsio_message_digest_md5 ());

  g_print ("testing auto SHA-1 digest indexing...\n");
  test_digest_indexing (edsio_message_digest_sha ());

  if (! repository_commit (repo))
    fail ();

  if (! repository_close (repo))
    fail ();

  if (! repository_system_close ())
    fail ();

  if (! nodel)
    system ("rm -rf /tmp/repotest");

  return 0;
}
