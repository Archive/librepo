/* -*-Mode: C;-*-
 * $Id: mpool.h 1.18 Sun, 18 Apr 1999 05:53:06 -0700 jmacd $
 *
 * Copyright (C) 1997, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#ifndef _MPOOL_H_
#define _MPOOL_H_

typedef struct _PageManager PageManager;

typedef gboolean PageInFunc  (RepoFileHandle *handle,
			      void       *pgaddr,
			      gssize      page_no,
			      gsize       in_len);
typedef gboolean PageOutFunc (RepoFileHandle *handle,
			      void       *pgaddr,
			      gssize      page_no,
			      gsize       out_len);

gssize   mem_pool_map_page     (FileHandle *fh, guint pgno, const guint8** mem);
gboolean mem_pool_unmap_page   (FileHandle *fh, guint pgno, const guint8** mem);

gboolean     mem_pool_init           (void);
PageManager* mem_pool_pager_new      (gsize        page_size,
				      PageInFunc  *page_in,
				      PageOutFunc *page_out);

PageManager* mem_pool_temp_pager_new (void);
void         mem_pool_pager_free     (PageManager*);
gsize        mem_pool_page_size      (PageManager*);
void         mem_pool_release        (PageManager *pager,
				      gpointer     unique);

DB_ENV*   db_env        (void);
DB_ENV*   db_env_txn    (const char* dir);
gboolean  db_env_close  (DB_ENV*);

#endif /* _MPOOL_H_ */
