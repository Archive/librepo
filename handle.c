/* -*-Mode: C;-*-
 * handle.c,v 1.11 1998/10/03 00:17:50 jmacd Exp
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include <sys/types.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include "repo.h"
#include "repopriv.h"

/**********************************************************************/
/*                            FILE SYSTEM HANDLE                      */
/**********************************************************************/

typedef struct _FsCommonHandle FsCommonHandle;

struct _FsCommonHandle {
  RepoFileHandle     handle;
  gint               fd;
};

typedef struct _FsHandle FsHandle;

struct _FsHandle {
  RepoFileHandle     handle;
  gint               fd;
  FsFileHandleClose *close;
  Path              *path;
  const char        *native_name;
  void              *data;
};

static gboolean     fs_handle_close (FileHandle* fh);
static void         fs_handle_abort (FileHandle* fh);
static const gchar* fs_handle_name  (FileHandle *fh);
static void         fs_handle_free  (FileHandle* fh);

static gboolean fs_file_page_in (RepoFileHandle *handle,
				 void           *pgaddr,
				 gssize          page_no,
				 gsize           in_len);

static gboolean fs_file_page_out (RepoFileHandle *handle,
				  void           *pgaddr,
				  gssize          page_no,
				  gsize           out_len);

static HandleFuncTable fs_table = {
  fs_handle_close,
  fs_handle_abort,
  fs_handle_name,
  fs_handle_free,
  mem_pool_map_page,
  mem_pool_unmap_page,
  NULL,
  NULL,
  NULL
};

static PageManager*
fs_pager (void)
{
  static PageManager* pager = NULL;

  if (! pager)
    {
      if (! (pager = mem_pool_pager_new (FS_PAGE_SIZE, fs_file_page_in, fs_file_page_out)))
	return NULL;
    }

  return pager;
}

static gint
fs_handle_open (const char* name, gint flags)
{
  gint open_flags;
  gint fd;

  if (flags & HV_Read)
    open_flags = O_RDONLY;
  else
    open_flags = O_WRONLY|O_CREAT|O_EXCL;

  fd = open (name, open_flags, 0666);

  if (fd < 0)
    {
      if (flags & HV_Read)
	repo_generate_stringerrno_event (EC_RepoOpenReadFailed, name);
      else
	repo_generate_stringerrno_event (EC_RepoOpenReplaceFailed, name);
    }

  return fd;
}

FileHandle*
fs_file_handle_new (const char        *native_name,
		    Path              *path,
		    guint              length,
		    void              *data,
		    FsFileHandleClose *close,
		    gint               flags)
{
  FsHandle* fs;
  int fd;

  if ((fd = fs_handle_open (native_name, flags)) < 0)
    return NULL;

  fs = g_new0 (FsHandle, 1);

  fs->handle.handle.table = & fs_table;
  fs->handle.handle.fh_open_flags = flags;
  fs->handle.handle.fh_open_mode = flags & (HV_Read | HV_Replace);
  fs->handle.handle.fh_has_len = TRUE;

  fs->handle.pager = fs_pager ();

  fs->fd = fd;
  fs->data = data;
  fs->close = close;
  fs->path = path;
  fs->native_name = g_strdup (native_name);

  file_position_from_abs (FS_PAGE_SIZE, 0,      & fs->handle.handle.fh_cur_pos);
  file_position_from_abs (FS_PAGE_SIZE, length, & fs->handle.handle.fh_file_len);

  return & fs->handle.handle;
}

static gboolean
fs_handle_close (FileHandle* fh)
{
  FsHandle* fs = (FsHandle*) fh;

  if (close (fs->fd) < 0)
    {
      repo_generate_handleerrno_event (EC_RepoCloseFailed, fh);
      return FALSE;
    }

  if (fs->close)
    return fs->close (fh, fs->path, fs->data);

  return TRUE;
}

void
fs_handle_abort (FileHandle* fh)
{
  FsHandle* fs = (FsHandle*) fh;

  if (close (fs->fd) < 0)
    repo_generate_handleerrno_event (EC_RepoCloseFailed, fh);

  unlink (fs->native_name);
}

const gchar*
fs_handle_name  (FileHandle *fh)
{
  FsHandle* fs = (FsHandle*) fh;

  return g_strdup (fs->native_name);
}

void
fs_handle_free (FileHandle* fh)
{
  g_free (fh);
}

gboolean
fs_file_page_out (RepoFileHandle *fh,
		  void           *pgaddr,
		  gssize          page_no,
		  gsize           out_len)
{
  FsCommonHandle* fs = (FsCommonHandle*) fh;
  ssize_t goal = FS_PAGE_SIZE * page_no;

  if (lseek (fs->fd, goal, SEEK_SET) != goal)
    {
      repo_generate_handleerrno_event (EC_RepoLseekFailed, FH(fh));
      return FALSE;
    }

  if (write (fs->fd, pgaddr, out_len) != out_len)
    {
      repo_generate_handleerrno_event (EC_RepoWriteFailed, FH(fh));
      return FALSE;
    }

  return TRUE;
}

gboolean
fs_file_page_in (RepoFileHandle *fh,
		 void           *pgaddr,
		 gssize          page_no,
		 gsize           in_len)
{
  FsCommonHandle* fs = (FsCommonHandle*) fh;
  ssize_t val, goal = FS_PAGE_SIZE * page_no;

  if (lseek (fs->fd, goal, SEEK_SET) != goal)
    {
      repo_generate_handleerrno_event (EC_RepoLseekFailed, FH(fh));
      return FALSE;
    }

  if ((val = read (fs->fd, pgaddr, in_len)) != in_len)
    {
      repo_generate_handleerrno_event (EC_RepoReadFailed, FH(fh));
      return FALSE;
    }

  return TRUE;
}

/**********************************************************************/
/*                             DATABASE HANDLE                        */
/**********************************************************************/

typedef struct _DbHandle DbHandle;

struct _DbHandle {
  RepoFileHandle     handle;
  gint               fd;

  DbFileHandleClose *close;
  FileHandleName    *name;
  Path              *fs_base;
  KeyIterator       *ki;
  void              *data;

  Path              *fs_path;
  guint8 first_page [DB_FIRST_PAGE_SIZE];
};

static gboolean     db_handle_close (FileHandle* fh);
static void         db_handle_abort (FileHandle* fh);
static const gchar* db_handle_name  (FileHandle *fh);
static void         db_handle_free  (FileHandle* fh);

static gboolean db_file_page_in (RepoFileHandle *handle,
				 void           *pgaddr,
				 gssize          page_no,
				 gsize           in_len);

static gboolean db_file_page_out (RepoFileHandle *handle,
				  void           *pgaddr,
				  gssize          page_no,
				  gsize           out_len);

const char sorta_base64_table[64] = {
  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
  'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a',
  'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
  'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2',
  '3', '4', '5', '6', '7', '8', '9', '+', /* this char differs, no slashes! */ '-'
};

static HandleFuncTable db_table = {
  db_handle_close,
  db_handle_abort,
  db_handle_name,
  db_handle_free,
  mem_pool_map_page,
  mem_pool_unmap_page,
  NULL,
  NULL,
  NULL
};

static PageManager*
db_pager (void)
{
  static PageManager* pager = NULL;

  if (! pager)
    {
      if (! (pager = mem_pool_pager_new (FS_PAGE_SIZE, db_file_page_in, db_file_page_out)))
	return NULL;
    }

  return pager;
}

FileHandle*
db_file_handle_new (Path              *fs_base,
		    KeyIterator       *ki,
		    void              *data,
		    DbFileHandleClose *close,
		    FileHandleName    *name,
		    gint               flags)
{
  DbHandle* dfh = g_new0 (DbHandle, 1);

  dfh->handle.handle.table = & db_table;
  dfh->handle.handle.fh_open_flags = flags;
  dfh->handle.handle.fh_open_mode = flags & (HV_Read | HV_Replace);
  dfh->handle.handle.fh_has_len = TRUE;

  dfh->handle.pager = db_pager ();

  dfh->close   = close;
  dfh->name    = name;
  dfh->ki      = ki;
  dfh->fs_base = fs_base;
  dfh->data    = data;
  dfh->fd      = -1;

  file_position_from_abs (FS_PAGE_SIZE, 0, & dfh->handle.handle.fh_cur_pos);
  file_position_from_abs (FS_PAGE_SIZE, 0, & dfh->handle.handle.fh_file_len);

  return & dfh->handle.handle;
}

gboolean
db_file_page_in (RepoFileHandle *fh,
		 void           *pgaddr,
		 gssize          page_no,
		 gsize           in_len)
{
  /* this handle is only used for writing.  the reader is handled
   * directly between mem and fs handles. */
  return TRUE;
}

const guint8*
key_iterate (KeyIterator* ki, int *key_len)
{
  int count_rem = ki->count, i;
  int depth = ki->depth;
  int mask;
  int k_minus_1 = ki->k - 1;
  static guint8 key[64];

  mask = (1<<k_minus_1)-1;

  g_assert (ki->k <= 8 && depth > 0 && depth < 64);

  for (i = 0; i < depth; i += 1)
    {
      guint8 b;

      if (i == 0)
	b = count_rem & mask;
      else
	b = (count_rem & mask) | (1 << k_minus_1);

      key[depth-i-1] = b;

      count_rem >>= k_minus_1;
    }

  (* key_len) = depth;

  ki->count += 1;

  if (ki->count == (1<<(k_minus_1*depth)))
    {
      ki->depth += 1;
      ki->count = 0;
    }

  return key;
}

Path*
db_generate_data_path (Path* fsbase, KeyIterator *ki)
{
  Path* p;

  for (;;)
    {
      int key_len, i;
      const guint8* key;
      File* f;

      p = fsbase;

      key = key_iterate (ki, & key_len);

      for (i = 0; i < key_len; i += 1)
	p = path_append_format (p, "%c", sorta_base64_table[key[i]]);

      if (! file_mkdir_p (_fs_repo, path_dirname (p)))
	return NULL;

      f = file_initialize (_fs_repo, p);

      g_assert (f);

      if (file_is_type_noerr (f, FV_NotPresent))
	break;
    }

  return p;
}

gboolean
db_file_page_out (RepoFileHandle *fh,
		  void           *pgaddr,
		  gssize          page_no,
		  gsize           out_len)
{
  DbHandle* dfh = (DbHandle*) fh;

  if (page_no == 0 && out_len <= DB_FIRST_PAGE_SIZE)
    {
      /* assumption is that we're closing... @@@ add an assertion in edsio some time */
      memcpy (dfh->first_page, pgaddr, out_len);
      return TRUE;
    }

  if (dfh->fd < 0)
    {
      if (! (dfh->fs_path = db_generate_data_path (dfh->fs_base, dfh->ki)))
	return NULL;

      if ((dfh->fd = fs_handle_open (path_to_string (_fs_repo, dfh->fs_path), HV_Replace)) < 0)
	return NULL;
    }

  return fs_file_page_out (fh, pgaddr, page_no, out_len);
}

gboolean
db_handle_close (FileHandle* fh)
{
  DbHandle* dfh = (DbHandle*) fh;

  return dfh->close (fh,
		     dfh->data,
		     file_position_to_abs (& fh->fh_file_len),
		     dfh->fs_path ? NULL : dfh->first_page,
		     dfh->fs_path);
}

void
db_handle_abort (FileHandle* fh)
{
  DbHandle* dfh = (DbHandle*) fh;

  if (dfh->fs_path)
    unlink (path_to_string (_fs_repo, dfh->fs_path));
}

const gchar*
db_handle_name (FileHandle *fh)
{
  DbHandle* dfh = (DbHandle*) fh;

  return dfh->name (dfh->data);
}

void
db_handle_free (FileHandle* fh)
{
  g_free (fh);
}

/* Temp handles
 */

typedef struct _TempHandle TempHandle;

struct _TempHandle {
  RepoFileHandle       handle;
  TempFileHandleClose *close;
  FileHandleName      *name;
  void                *data;
};

static gboolean     temp_handle_close (FileHandle* fh);
static void         temp_handle_abort (FileHandle* fh);
static const gchar* temp_handle_name  (FileHandle *fh);
static void         temp_handle_free  (FileHandle* fh);

static HandleFuncTable temp_table = {
  temp_handle_close,
  temp_handle_abort,
  temp_handle_name,
  temp_handle_free,
  mem_pool_map_page,
  mem_pool_unmap_page,
  NULL,
  NULL,
  NULL
};

FileHandle*
temp_file_handle_new (PageManager *pager, guint len, void* data, TempFileHandleClose *close, FileHandleName *name, gint flags)
{
  TempHandle* tfh = g_new0 (TempHandle, 1);

  tfh->handle.handle.table = & temp_table;
  tfh->handle.handle.fh_open_flags = flags;
  tfh->handle.handle.fh_open_mode = flags & (HV_Read | HV_Replace);
  tfh->handle.handle.fh_has_len = TRUE;

  if (! pager)
    pager = mem_pool_temp_pager_new ();

  tfh->handle.pager = pager;

  tfh->close   = close;
  tfh->name    = name;
  tfh->data    = data;

  file_position_from_abs (TEMP_PAGE_SIZE, 0, & tfh->handle.handle.fh_cur_pos);
  file_position_from_abs (TEMP_PAGE_SIZE, len, & tfh->handle.handle.fh_file_len);

  return & tfh->handle.handle;
}

gboolean
temp_handle_close (FileHandle* fh)
{
  TempHandle *tfh = (TempHandle*) fh;

  if (tfh->close)
    return tfh->close (fh, tfh->data, file_position_to_abs (& fh->fh_file_len), tfh->handle.pager);
  else
    return TRUE;
}

void
temp_handle_abort (FileHandle* fh)
{
  TempHandle *tfh = (TempHandle*) fh;

  if (fh->fh_open_mode == HV_Replace)
    mem_pool_pager_free (tfh->handle.pager);
}

const gchar*
temp_handle_name (FileHandle *fh)
{
  TempHandle* tfh = (TempHandle*) fh;

  return tfh->name (tfh->data);
}

void
temp_handle_free (FileHandle* fh)
{
  g_free (fh);
}

/* memory handle
 */

typedef struct _MemHandle MemHandle;

struct _MemHandle {
  RepoFileHandle     handle;
  const guint8      *buf;
  const char        *name;
  guint              len;
};

static gboolean     mem_handle_close (FileHandle* fh);
static void         mem_handle_abort (FileHandle* fh);
static const gchar* mem_handle_name  (FileHandle *fh);
static void         mem_handle_free  (FileHandle* fh);

static gboolean mem_file_page_in (RepoFileHandle *handle,
				 void           *pgaddr,
				 gssize          page_no,
				 gsize           in_len);

static gboolean mem_file_page_out (RepoFileHandle *handle,
				  void           *pgaddr,
				  gssize          page_no,
				  gsize           out_len);

static HandleFuncTable mem_table = {
  mem_handle_close,
  mem_handle_abort,
  mem_handle_name,
  mem_handle_free,
  mem_pool_map_page,
  mem_pool_unmap_page,
  NULL,
  NULL,
  NULL
};

static PageManager*
mem_pager (void)
{
  static PageManager* pager = NULL;

  if (! pager)
    {
      if (! (pager = mem_pool_pager_new (MEM_PAGE_SIZE, mem_file_page_in, mem_file_page_out)))
	return NULL;
    }

  return pager;
}

FileHandle*
mem_file_handle_new (const guint8* buf, const char* name, guint len, gint flags)
{
  MemHandle* mem;

  mem = g_new0 (MemHandle, 1);

  mem->handle.handle.table = & mem_table;
  mem->handle.handle.fh_open_flags = flags;
  mem->handle.handle.fh_open_mode = flags & (HV_Read | HV_Replace);
  mem->handle.handle.fh_has_len = TRUE;

  mem->handle.pager = mem_pager ();

  mem->buf = g_memdup (buf, len);
  mem->len = len;
  mem->name = name;

  file_position_from_abs (MEM_PAGE_SIZE, 0, & mem->handle.handle.fh_cur_pos);
  file_position_from_abs (MEM_PAGE_SIZE, len, & mem->handle.handle.fh_file_len);

  return & mem->handle.handle;
}

gboolean
mem_handle_close (FileHandle* fh)
{
  return TRUE;
}

void
mem_handle_abort (FileHandle* fh)
{
}

const gchar*
mem_handle_name (FileHandle *fh)
{
  MemHandle *mfh = (MemHandle*) fh;

  return g_strdup_printf ("short:%s", mfh->name);
}

void
mem_handle_free (FileHandle* fh)
{
  MemHandle *mfh = (MemHandle*) fh;

  g_free ((void*) mfh->buf);
  g_free (mfh);
}

gboolean
mem_file_page_in (RepoFileHandle *handle,
		  void           *pgaddr,
		  gssize          page_no,
		  gsize           in_len)
{
  MemHandle *mfh = (MemHandle*) handle;

  guint off = page_no * MEM_PAGE_SIZE;

  g_assert (off + in_len <= mfh->len);

  memcpy (pgaddr, mfh->buf + off, in_len);

  return TRUE;
}

gboolean
mem_file_page_out (RepoFileHandle *handle,
		   void           *pgaddr,
		   gssize          page_no,
		   gsize           out_len)
{
  abort ();
}
