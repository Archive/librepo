/* -*-Mode: C;-*-
 * $Id: fsrepo.c 1.76 Sun, 02 May 1999 04:53:40 -0700 jmacd $
 *
 * Copyright (C) 1997, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include <sys/types.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <pwd.h>
#include <grp.h>
#include "config.h"

#if HAVE_DIRENT_H
# include <dirent.h>
# define NAMLEN(dirent) strlen((dirent)->d_name)
#else
# define dirent direct
# define NAMLEN(dirent) (dirent)->d_namlen
# if HAVE_SYS_NDIR_H
#  include <sys/ndir.h>
# endif
# if HAVE_SYS_DIR_H
#  include <sys/dir.h>
# endif
# if HAVE_NDIR_H
#  include <ndir.h>
# endif
#endif

#include "repo.h"
#include "repopriv.h"

typedef struct _FSRepository     FSRepository;
typedef struct _FSDriverFile       FSDriverFile;
typedef struct _FSRegFile        FSRegFile;
typedef struct _FSOperation      FSOperation;
typedef enum   _FSOperationType  FSOperationType;
typedef struct _FSDirectoryHandle FSDirectoryHandle;

struct _FSRepository {
  Repository repo;

  GSList* commit_ops;
  GSList* rollback_ops;

  GHashTable *uid_to_name_table;
  GHashTable *gid_to_name_table;
};

Repository* _fs_repo;

struct _FSDriverFile {
  DriverFile dfile;

  Path *current_path;

  /* Access checks */
  guint access_result : 3;
  guint access_tested : 3;

  /* Struct stat */
  SerialGenericUnixFileProperties props;

  gboolean on_disk;

  const char* link;
};

#define FS_ACCESS_READ 1
#define FS_ACCESS_WRITE 2
#define FS_ACCESS_EXECUTE 4

enum _FSOperationType {
  FSOP_Unlink = 1,
  FSOP_Rename = 2,
  FSOP_Symlink = 3,
  FSOP_Rmdir = 4,
  FSOP_Mkdir = 5
};

struct _FSOperation {
  FSOperationType type;
  const void* arg1;
  const void* arg2;
};

struct _FSDirectoryHandle {
  DirectoryHandle dh;
  DIR* thisdir;
  const char* prefix;
  int prefix_len;
  Path* dpath;
};

static gboolean fs_repo_close_replace (FileHandle *fh, Path* current, FileSegment* seg);

static Path* fs_repo_canonicalize (Repository* repo, const char* url);
static Path* fs_repo_canonicalize_long (Repository* repo, Path*relative, const char* url);

static gboolean fs_repo_commit (Repository *repo);
static gboolean fs_repo_rollback (Repository *repo);
static gboolean fs_repo_close (Repository *repo);
static Repository* fs_repo_create (const char* url, RepositoryDriver* driver);

static gboolean fs_repo_exists (const char* url, RepositoryDriver* driver);
static Repository* fs_repo_initialize (const char* url, RepositoryDriver* driver);
static DriverFile* fs_repo_lstat (Repository* repo, DriverFile* parent, Path* path);

static guint fs_repo_segment_length (FileSegment* seg);

static gboolean fs_repo_make_dir (DriverFile* file);
static gboolean fs_repo_make_symlink (DriverFile* file, const char* link);

static const char* fs_repo_path_to_string (Repository* repo, Path* path);
static gboolean fs_repo_will_handle (const char* url, RepositoryDriver* driver);

static gboolean     fs_repo_segment_unlink (FileSegment* seg);
static FileSegment* fs_repo_segment_stat   (DriverFile* file, Path* name);
static FileHandle*  fs_repo_open_replace   (FileSegment* seg, gint flags);
static FileHandle*  fs_repo_open_read      (FileSegment* seg, gint flags);

static gboolean    fs_repo_properties_restore (DriverFile* file, const guint8* data, guint len);
static gboolean    fs_repo_unmodified_since   (DriverFile* file, const guint8* data, guint len);

static DirectoryHandle* fs_repo_opendir  (DriverFile* file, const char* prefix);
static gboolean         fs_repo_readdir  (DirectoryHandle* dh, Path** res);
static gboolean         fs_repo_closedir (DirectoryHandle* dh);

Path*
repository_system_user_home (void)
{
  const char* home = g_get_home_dir ();
  Path* p = path_canonicalize (_fs_repo, home);

  if (! p)
    p = path_root ();

  return p;
}

static void
fs_commit_op (Repository* repo, FSOperationType type, const void* arg1, const void* arg2)
{
  FSOperation* op = g_new0 (FSOperation, 1);
  FSRepository* fsrepo = (FSRepository*) repo;

  op->type = type;
  op->arg1 = arg1;
  op->arg2 = arg2;

  fsrepo->commit_ops = g_slist_prepend (fsrepo->commit_ops, op);
}

static void
fs_rollback_op (Repository* repo, FSOperationType type, const void* arg1, const void* arg2)
{
  FSOperation* op = g_new0 (FSOperation, 1);
  FSRepository* fsrepo = (FSRepository*) repo;

  op->type = type;
  op->arg1 = arg1;
  op->arg2 = arg2;

  fsrepo->rollback_ops = g_slist_prepend (fsrepo->rollback_ops, op);
}

/* Permissions
 */

static gboolean
fs_access (DriverFile* file, gint arg, gint mask)
{
  FSDriverFile* fsdf = (FSDriverFile*) file;

  if (! (fsdf->access_tested & mask))
    {
      if (access (path_to_string (file->repo, file->driver_path), arg) >= 0)
	fsdf->access_result |= mask;

      fsdf->access_tested |= mask;
    }

  return (fsdf->access_result & mask) != 0;
}

static gboolean
fs_repo_mayread_driver_file (DriverFile* file)
{
  return fs_access (file, R_OK, FS_ACCESS_READ);
}

static gboolean
fs_repo_maymodify_driver_file (DriverFile* file)
{
  return fs_access (file, W_OK, FS_ACCESS_WRITE);
}

/* System-specific stamps and properties.
 */

static gboolean
fs_repo_file_properties (DriverFile* dfile, guint8** arg, guint32* arg_len)
{
  GByteArray* sink_result;
  FSDriverFile* data = (FSDriverFile*) dfile;
  SerialSink* sink;

  sink = edsio_simple_sink (NULL, SBF_None, FALSE, NULL, & sink_result);

  g_assert (dfile->type & (FV_Regular | FV_SymbolicLink | FV_Directory));

  if (! serialize_genericunixfileproperties_obj (sink, & data->props))
    return FALSE;

  if (! sink->sink_close (sink))
    return FALSE;

  sink->sink_free (sink);

  (* arg) = sink_result->data;
  (* arg_len) = sink_result->len;

  g_byte_array_free (sink_result, FALSE);

  return TRUE;
}

gboolean
fs_repo_properties_restore (DriverFile* file, const guint8* prop_data, guint prop_len)
{
  abort ();
}

gboolean
fs_repo_unmodified_since (DriverFile* file, const guint8* prop_data, guint prop_len)
{
  FSDriverFile* data = (FSDriverFile*) file;
  SerialGenericUnixFileProperties *props = NULL;
  gboolean ret = FALSE;
  SerialSource *source;

  source = edsio_simple_source (prop_data, prop_len, SBF_None);

  if (! unserialize_genericunixfileproperties (source, &props))
    goto nope;

  if (props->dev != data->props.dev)
    goto nope;

  if (props->rdev != data->props.rdev)
    goto nope;

  if (props->ino != data->props.ino)
    goto nope;

  if (props->mode != data->props.mode)
    goto nope;

  /*if (props->nlink != data->props.nlink)
    goto nope;*/

  if (props->uid != data->props.uid)
    goto nope;

  if (props->gid != data->props.gid)
    goto nope;

  if (props->length != data->props.length)
    goto nope;

  if (props->atime.seconds != data->props.atime.seconds)
    goto nope;

  if (props->atime.nanos != data->props.atime.nanos)
    goto nope;

  if (props->mtime.seconds != data->props.mtime.seconds)
    goto nope;

  if (props->mtime.nanos != data->props.mtime.nanos)
    goto nope;

  if (props->ctime.seconds != data->props.ctime.seconds)
    goto nope;

  if (props->ctime.nanos != data->props.ctime.nanos)
    goto nope;

  ret = TRUE;

 nope:

  /* if (! source->source_close (source)) ...; */

  if (source)
    source->source_free (source);

  if (props)
    g_free (props);

  return ret;
}

/* ...
 */

static gint
fs_getpid (void)
{
  static gint pid = 0;

  if (!pid)
    pid = getpid ();

  return pid;
}

static const char*
fs_repo_cwd(void)
{
  static char buf[MAXPATHLEN];
  const char* rval;

  if(buf[0] == 0)
    {
      rval =
#if defined(sun) && !defined(__SVR4)
	getwd(buf);
#else
        getcwd(buf, MAXPATHLEN);
#endif

      if (rval == NULL)
	return NULL;
    }

  return buf;
}

/*
 */

gboolean
fs_repo_will_handle (const char* url, RepositoryDriver* driver)
{
  return strncmp (url, "file:", strlen ("file:")) == 0;
}

Repository*
fs_repo_create (const char* url, RepositoryDriver* driver)
{
  return fs_repo_initialize (url, driver);
}

gboolean
fs_repo_exists (const char* url, RepositoryDriver* driver)
{
  return TRUE;
}

Repository*
fs_repo_initialize (const char* url, RepositoryDriver* driver)
{
  static Repository *repo = NULL;

  FSRepository *fsrepo;
  const char *path;

  if (repo)
    {
      g_assert (repo == _fs_repo);
      return repo;
    }

  repo = repository_new (driver);

  fsrepo = (FSRepository*) repo;

  path = url + strlen ("file:");

  repo->relative_path = path_root();
  repo->supports_segments = FALSE;

  fsrepo->uid_to_name_table = g_hash_table_new (g_int_hash, g_int_equal);
  fsrepo->gid_to_name_table = g_hash_table_new (g_int_hash, g_int_equal);

  if (path[0] != '/')
    repo->relative_path = path_canonicalize (repo, fs_repo_cwd ());

  if (path[0] != 0)
    repo->relative_path = path_canonicalize (repo, path);

  return repo;
}

Path*
fs_repo_canonicalize (Repository* repo, const char* url)
{
  return path_canonicalize_internal (repo, repo->relative_path, '/', FALSE, url);
}

Path*
fs_repo_canonicalize_long (Repository* repo, Path* relative, const char* url0)
{
  return path_canonicalize_internal (repo, relative, '/', FALSE, url0);
}

const char*
fs_repo_path_to_string (Repository* repo, Path* path)
{
  return path_to_string_simple (path, '/');
}

static const char*
fs_repo_driver_readlink (DriverFile* df)
{
  FSDriverFile* fsdf = (FSDriverFile*) df;
  static char buf[MAXPATHLEN];
  gint ret;

  if (fsdf->link)
    return fsdf->link;

  if ((ret = readlink (path_to_string (df->repo, df->driver_path), buf, MAXPATHLEN)) < 0)
    {
      repo_generate_patherrno_event (EC_RepoReadLinkFailed, df->driver_path);
      return FALSE;
    }

  /* readlink() doesn't zero terminate */
  buf[ret] = 0;

  fsdf->link = g_strdup (buf);

  return fsdf->link;
}

static void
fill_properties (Repository* repo, struct stat *sbuf, SerialGenericUnixFileProperties* props)
{
  FSRepository *fsrepo = (FSRepository*) repo;

  props->dev = sbuf->st_dev;
  props->rdev = sbuf->st_rdev;
  props->ino = sbuf->st_ino;
  props->mode = sbuf->st_mode;
  props->nlink = sbuf->st_nlink;
  props->uid = sbuf->st_uid;
  props->gid = sbuf->st_gid;
  props->length = sbuf->st_size;

#if STRUCT_STAT_TIMESPEC
  props->atime.seconds = sbuf->st_atimespec.tv_sec;
  props->atime.nanos = sbuf->st_atimespec.tv_nsec;
  props->mtime.seconds = sbuf->st_mtimespec.tv_sec;
  props->mtime.nanos = sbuf->st_mtimespec.tv_nsec;
  props->ctime.seconds = sbuf->st_ctimespec.tv_sec;
  props->ctime.nanos = sbuf->st_ctimespec.tv_nsec;
#else

#if STRUCT_STAT_TIME
  props->atime.seconds = sbuf->st_atime;
  props->mtime.seconds = sbuf->st_mtime;
  props->ctime.seconds = sbuf->st_ctime;
#else
#error "No times in struct stat?"
#endif

#if STRUCT_STAT_TIMENSEC
  props->atime.nanos = sbuf->st_atimensec;
  props->mtime.nanos = sbuf->st_mtimensec;
  props->ctime.nanos = sbuf->st_ctimensec;
#else
  props->atime.nanos = 0;
  props->mtime.nanos = 0;
  props->ctime.nanos = 0;
#endif
#endif

  if (! (props->uid_name = g_hash_table_lookup (fsrepo->uid_to_name_table, &props->uid)))
    {
      struct passwd *pwd;

      if (! (pwd = getpwuid (props->uid)))
	props->uid_name = "unknown";
      else
	props->uid_name = g_strdup (pwd->pw_name);

      g_hash_table_insert (fsrepo->uid_to_name_table, & props->uid, (void*) props->uid_name);
    }

  if (! (props->gid_name = g_hash_table_lookup (fsrepo->gid_to_name_table, & props->gid)))
    {
      struct group *grp;

      if (! (grp = getgrgid (props->gid)))
	props->gid_name = "unknown";
      else
	props->gid_name = g_strdup (grp->gr_name);

      g_hash_table_insert (fsrepo->gid_to_name_table, & props->gid, (void*) props->gid_name);
    }
}

const char*
fs_repo_mode_to_typestring (mode_t mode)
{
  mode &= S_IFMT;

  switch (mode)
    {
      /* from /usr/include/sys/stat.h on FreeBSD 3.1 */
    case S_IFIFO /*0010000*/:
      return "named pipe (fifo)";
    case S_IFCHR /*0020000*/:
      return "character special";
    case S_IFDIR /*0040000*/:
      return "directory";
    case S_IFBLK /*0060000*/:
      return "block special";
    case S_IFREG /*0100000*/:
      return "regular";
    case S_IFLNK /*0120000*/:
      return "symbolic link";
    case S_IFSOCK /*0140000*/:
      return "socket";
    default:
      {
	static char buf[128];
	sprintf (buf, "unknown system specific file type: 0%o", mode);
	return buf;
      }
    }
}

DriverFile*
fs_repo_lstat (Repository* repo, DriverFile* parent, Path* path)
{
  const char *native = path_to_string (repo, path);
  FSDriverFile *df = g_new0 (FSDriverFile, 1);
  struct stat sbuf;
  gint res;

  res = lstat (native, &sbuf);

  df->dfile.repo = repo;
  df->dfile.driver_path = path;

  df->dfile.segment_stat = fs_repo_segment_stat;
  df->dfile.segment_length = fs_repo_segment_length;
  df->dfile.segment_unlink = fs_repo_segment_unlink;
  df->dfile.driver_readlink = fs_repo_driver_readlink;

  if (res < 0 && errno != ENOENT)
    {
      repo_generate_patherrno_event (EC_RepoLstatFailed, path);
      return NULL;
    }

  df->current_path = NULL;

  if (res < 0)
    {
      df->dfile.type = FV_NotPresent;
      df->on_disk = FALSE;
    }
  else
    {
      df->on_disk = TRUE;

      fill_properties (repo, &sbuf, &df->props);

      if (S_ISLNK (sbuf.st_mode))
	df->dfile.type = FV_SymbolicLink;
      else if (S_ISREG (sbuf.st_mode))
	{
	  df->dfile.type = FV_Regular;

	  if (FS_PAGE_SIZE % sbuf.st_blksize)
	    g_warning ("encountered small file system page_size, raise FS_PAGE_SIZE");
	}
      else if (S_ISDIR (sbuf.st_mode))
	{
	  df->dfile.type = FV_Directory;
	}
      else
	{
	  repo_generate_pathstring_event (EC_RepoUnsupportedFileType, path, fs_repo_mode_to_typestring (sbuf.st_mode));
	  return NULL;
	}
    }

  return (DriverFile*) df;
}

gboolean
fs_repo_make_dir (DriverFile* file)
{
  if (mkdir (path_to_string (file->repo, file->driver_path), 0777))
    {
      repo_generate_patherrno_event (EC_RepoMkdirFailed, file->driver_path);
      return FALSE;
    }

  g_assert (file->type = FV_NotPresent);

  fs_rollback_op (file->repo, FSOP_Rmdir, file->driver_path, NULL);

  return TRUE;
}

gboolean
fs_repo_make_symlink (DriverFile* file, const char* link)
{
  FSDriverFile* fsdf = (FSDriverFile*) file;

  fs_commit_op (file->repo, FSOP_Symlink, file->driver_path, link);

  g_assert (file->type = FV_NotPresent);

  fsdf->link = g_strdup (link);

  return TRUE;
}

/* This function is expected to file_initialize_link on every file in
 * the directory. */
DirectoryHandle*
fs_repo_opendir (DriverFile* file, const char* prefix)
{
  FSDirectoryHandle* dh = g_new0 (FSDirectoryHandle, 1);

  dh->dh.repo = file->repo;
  dh->prefix = prefix;
  dh->prefix_len = strlen (prefix);
  dh->dpath = file->driver_path;
  dh->dh.close = fs_repo_closedir;
  dh->dh.next = fs_repo_readdir;

  if (! (dh->thisdir = opendir (path_to_string (file->repo, file->driver_path))))
    {
      repo_generate_patherrno_event (EC_RepoOpendirFailed, file->driver_path);
      g_free (dh);
      return NULL;
    }

  return & dh->dh;
}

gboolean
fs_repo_readdir (DirectoryHandle* ndh, Path** res)
{
  FSDirectoryHandle* dh = (FSDirectoryHandle*) ndh;
  struct dirent* entry;

  (*res) = NULL;

  while ((entry = readdir (dh->thisdir)) != NULL)
    {
      const char* name = entry->d_name;

      /* Skip ".", "..", and our temp files */
      if (strcmp (name, ".") == 0 ||
	  strcmp (name, "..") == 0 ||
	  strncmp (name, ".__fsr", strlen (".__fsr")) == 0)
	continue;

      if (strncmp (dh->prefix, name, dh->prefix_len) != 0)
	continue;

      (*res) = path_append (dh->dpath, name);
      break;
    }

  return TRUE;
}

gboolean
fs_repo_closedir (DirectoryHandle* ndh)
{
  FSDirectoryHandle* dh = (FSDirectoryHandle*) ndh;

  if (closedir (dh->thisdir) < 0)
    {
      repo_generate_patherrno_event (EC_RepoClosedirFailed, dh->dpath);
      return FALSE;
    }

  return TRUE;
}

static gboolean
fs_repo_delete_dir (DriverFile* file)
{
  fs_commit_op (file->repo, FSOP_Rmdir, file->driver_path, NULL);

  return TRUE;
}

static gboolean
fs_repo_delete_symlink (DriverFile* file)
{
  fs_commit_op (file->repo, FSOP_Unlink, file->driver_path, NULL);

  return TRUE;
}

/* Default Segment Functions
 */

gboolean
fs_repo_segment_unlink (FileSegment* seg)
{
  if (segment_is_not_type (seg, SV_View | SV_Regular))
    return FALSE;

  fs_commit_op (seg->repo, FSOP_Unlink, seg->seg_file->driver_path, NULL);

  return TRUE;
}

guint
fs_repo_segment_length (FileSegment* seg)
{
  FSDriverFile* df = (FSDriverFile*) seg->seg_file;

  return df->props.length;
}

FileSegment*
fs_repo_segment_stat (DriverFile* file, Path* name)
{
  FileSegment* seg = g_new0 (FileSegment, 1);

  seg->repo = file->repo;
  seg->seg_file = file;
  seg->seg_name = name;
  seg->seg_is_default = (name == path_root ());

  if (seg->seg_is_default)
    {
      switch (seg->seg_file->type)
	{
	case FV_NotPresent:
	  seg->seg_type = SV_NotPresent;
	  break;

	case FV_Regular:
	  seg->seg_type = SV_Regular;
	  break;

	case FV_IrregularCase:
	case FV_Invalid:

	  repo_generate_segment_event (EC_RepoWrongDbFileType, seg);
	  seg->seg_type = SV_Invalid;
	  break;
	}
    }
  else
    {
      repo_generate_pathpath_event (EC_RepoSegmentsNotSupported, file->driver_path, name);
      seg->seg_type = SV_Invalid;
    }

  return seg;
}

FileHandle*
fs_repo_open_read (FileSegment *seg, gint flags)
{
  FSDriverFile *fsdf = (FSDriverFile*) seg->seg_file;
  Path* path = fsdf->dfile.driver_path;

  if (fsdf->current_path)
    path = fsdf->current_path;

  return fs_file_handle_new (path_to_string (seg->repo, path), path, fsdf->props.length, seg, NULL, flags);
}

FileHandle*
fs_repo_open_replace (FileSegment* seg, gint flags)
{
  FSDriverFile* fsdf = (FSDriverFile*) seg->seg_file;
  Path *replace_path;

  if (fsdf->on_disk)
    {
      gchar buf[32];

      sprintf (buf, ".__fsr%d%d", fs_getpid (), seg->repo->temp_count++);

      replace_path = path_append (path_dirname (seg->seg_file->driver_path), buf);
    }
  else
    {
      replace_path = seg->seg_file->driver_path;
    }

  return fs_file_handle_new (path_to_string (seg->repo, replace_path), replace_path, 0, seg, (FsFileHandleClose*) fs_repo_close_replace, flags);
}

/* Default Handle Close Functions
 */

gboolean
fs_repo_close_replace (FileHandle *fh, Path* current, FileSegment* seg)
{
  FSDriverFile* dfile = (FSDriverFile*) seg->seg_file;

  const char *native = path_to_string (seg->repo, current);

  struct stat sbuf;

  if (! driver_set_segment_state (seg, NULL, SV_NotPresent))
    return FALSE;

  /* There's a race between close and lstat, wheee. */

  if (lstat (native, &sbuf) < 0)
    {
      repo_generate_patherrno_event (EC_RepoLstatFailed, current);
      return FALSE;
    }

  fill_properties (seg->repo, &sbuf, &dfile->props);

  g_assert (handle_length (fh) == dfile->props.length);

  if (current != seg->seg_file->driver_path)
    fs_commit_op (seg->repo, FSOP_Rename, current, seg->seg_file->driver_path);

  fs_rollback_op (seg->repo, FSOP_Unlink, current, NULL);

  dfile->current_path = current;
  dfile->on_disk = TRUE;

  if (! driver_set_segment_state (seg, NULL, SV_Regular))
    return FALSE;

  return TRUE;
}

/* Commit, Close
 */

gboolean
fs_repo_close (Repository *repo)
{
  FSRepository *fsrepo = (FSRepository*) repo;

  if (fsrepo->commit_ops)
    {
      repo_generate_void_event (EC_RepoUncommittedClose);
      /* @@@ Rollback. */
    }

  return TRUE;
}

static gboolean
process_ops (Repository* repo, GSList* ops)
{
  for (; ops; ops = ops->next)
    {
      FSOperation *op = ops->data;

      switch (op->type)
	{
	case FSOP_Unlink:

	  if (unlink (path_to_string (repo, (Path*) op->arg1)) < 0)
	    {
	      repo_generate_patherrno_event (EC_RepoUnlinkFailed, (Path*) op->arg1);
	      return FALSE;
	    }

	  break;
	case FSOP_Rmdir:

	  if (rmdir (path_to_string (repo, (Path*) op->arg1)) < 0)
	    {
	      repo_generate_patherrno_event (EC_RepoRmdirFailed, (Path*) op->arg1);
	      return FALSE;
	    }

	  break;
	case FSOP_Symlink:

	  if (symlink (path_to_string (repo, (Path*) op->arg2), (const char*) op->arg1) < 0)
	    {
	      repo_generate_patherrno_event (EC_RepoSymlinkFailed, (Path*) op->arg1);
	      return FALSE;
	    }

	  break;

	case FSOP_Rename:

	  if (rename (path_to_string (repo, (Path*) op->arg1),
		      path_to_string (repo, (Path*) op->arg2)) < 0)
	    {
	      repo_generate_pathpatherrno_event (EC_RepoRenameFailed, (Path*) op->arg1, (Path*) op->arg2);
	      return FALSE;
	    }

	  break;

	case FSOP_Mkdir:

	  break;
	}
    }

  return TRUE;
}

gboolean
fs_repo_rollback (Repository *repo)
{
  FSRepository *fsrepo = (FSRepository*) repo;

  fsrepo->rollback_ops = g_slist_reverse (fsrepo->rollback_ops);

  if (! process_ops (repo, fsrepo->rollback_ops))
    return FALSE;

  g_slist_free (fsrepo->commit_ops);
  g_slist_free (fsrepo->rollback_ops);

  fsrepo->commit_ops = NULL;
  fsrepo->rollback_ops = NULL;

  return TRUE;
}

gboolean
fs_repo_commit (Repository *repo)
{
  FSRepository *fsrepo = (FSRepository*) repo;

  fsrepo->commit_ops = g_slist_reverse (fsrepo->commit_ops);

  if (! process_ops (repo, fsrepo->commit_ops))
    return FALSE;

  g_slist_free (fsrepo->commit_ops);
  g_slist_free (fsrepo->rollback_ops);

  fsrepo->commit_ops = NULL;
  fsrepo->rollback_ops = NULL;

  return TRUE;
}

gboolean
fs_repo_init ()
{
  static gboolean already = FALSE;

  if (! already)
    {
      SimpleDriver fs_driver;

      memset (&fs_driver, 0, sizeof (fs_driver));

      fs_driver.open_replace           = fs_repo_open_replace;
      fs_driver.open_read              = fs_repo_open_read;
      fs_driver.canonicalize = fs_repo_canonicalize;
      fs_driver.canonicalize_long = fs_repo_canonicalize_long;
      fs_driver.will_handle = fs_repo_will_handle;
      fs_driver.path_to_string = fs_repo_path_to_string;
      fs_driver.initialize = fs_repo_initialize;
      fs_driver.create = fs_repo_create;
      fs_driver.exists = fs_repo_exists;
      fs_driver.lstat = fs_repo_lstat;
      fs_driver.make_dir = fs_repo_make_dir;
      fs_driver.make_symlink = fs_repo_make_symlink;
      fs_driver.opendir = fs_repo_opendir;
      fs_driver.delete_symlink = fs_repo_delete_symlink;
      fs_driver.delete_dir = fs_repo_delete_dir;
      fs_driver.commit = fs_repo_commit;
      fs_driver.rollback = fs_repo_rollback;
      fs_driver.close = fs_repo_close;
      fs_driver.properties_restore = fs_repo_properties_restore;
      fs_driver.unmodified_since = fs_repo_unmodified_since;

      fs_driver.file_properties   = fs_repo_file_properties;
      fs_driver.file_maymodify    = fs_repo_maymodify_driver_file;
      fs_driver.file_mayread      = fs_repo_mayread_driver_file;

      driver_new (sizeof (FSRepository), &fs_driver, "Unix File System Repository");

      already = TRUE;
    }

  return TRUE;
}
