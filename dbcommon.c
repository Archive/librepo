/* -*-Mode: C;-*-
 * PRCS - The Project Revision Control System
 * Copyright (C) 1997, 1998  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: dbcommon.c 1.14 Mon, 12 Apr 1999 05:57:56 -0700 jmacd $
 */

#include "repo.h"
#include "repopriv.h"

gboolean
db_object_del (Repository *repo,
	       DB         *dbp,
	       DB_TXN     *txn,
	       const char* key)
{
  DBT dkey;

  clear_dbt (&dkey);

  init_str_key (&dkey, key);

  if ( (* dbp->del) (dbp, txn, &dkey, 0) )
    {
      repo_generate_stringerrno_event (EC_RepoDbError, "del");
      return FALSE;
    }

  return TRUE;
}

gboolean
db_object_rename (Repository *repo,
		  DB         *dbp,
		  DB_TXN     *txn,
		  const char *key1,
		  const char *key2)
{
  DBT dkey;
  DBT data;

  clear_dbts (&dkey, &data);

  init_str_key (&dkey, key1);

  if ( (* dbp->get) (dbp, txn, &dkey, &data, 0) )
    goto get;

  init_str_key (&dkey, key2);

  if ( (* dbp->put) (dbp, txn, &dkey, &data, 0) )
    goto put;

  init_str_key (&dkey, key1);

  if ( (* dbp->del) (dbp, txn, &dkey, 0) )
    goto del;

  return TRUE;

del:
  repo_generate_stringerrno_event (EC_RepoDbError, "del");
  return FALSE;

put:
  repo_generate_stringerrno_event (EC_RepoDbError, "put");
  return FALSE;

get:
  repo_generate_stringerrno_event (EC_RepoDbError, "get");
  return FALSE;
}

const char*
db_get_name (DB *dbp, DB_TXN* txn)
{
  DBT key, data;

  clear_dbts (&key, &data);

  init_str_key (&key, "*database-name*");

  if (dbp->get (dbp, txn, &key, &data, 0))
    return "*error*";

  return g_strdup ((char*)data.data);
}
